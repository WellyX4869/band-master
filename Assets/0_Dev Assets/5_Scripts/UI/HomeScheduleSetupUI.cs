using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class HomeScheduleSetupUI : MonoBehaviour
{
    public TextMeshProUGUI homeScheduleDateText;
    public TextMeshProUGUI homeScheduleTimeText;
    public TextMeshProUGUI homeScheduleTitleText;
    public TextMeshProUGUI homeScheduleLocationText;
    public Image goToStageButtonImage;
    [HideInInspector] public BandSchedule bandSchedule;
}
