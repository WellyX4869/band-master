using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameMenuUI : MonoBehaviour
{
    public static GameMenuUI instance;

    [Header("Game Panel")]
    [SerializeField] List<GameObject> activeMenus = new List<GameObject>();
    [SerializeField] List<GameObject> inactiveMenus = new List<GameObject>();
    [SerializeField] List<GameObject> contentMenus = new List<GameObject>();
    [SerializeField] Transform interval;
    int activeMenuIndex = 0;

    [Header("Player's Info")]
    [SerializeField] TextMeshProUGUI groupNameText;
    [SerializeField] TextMeshProUGUI playerNameText;
    bool IsMenuLocked = false;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else Destroy(gameObject);

        #region  Settings Game Menu 
        for (int i = 0; i < activeMenus.Count; i++)
        {
            if (i == activeMenuIndex)
            {
                activeMenus[i].SetActive(true);
                contentMenus[i].SetActive(true);
            }
            else
            {
                activeMenus[i].SetActive(false);
                contentMenus[i].SetActive(false);
            }
        }

        for (int i = 0; i < inactiveMenus.Count; i++)
        {
            if (i == activeMenuIndex)
            {
                inactiveMenus[i].SetActive(false);
                interval.SetSiblingIndex(i + inactiveMenus.Count - 1);
            }
            else
            {
                inactiveMenus[i].SetActive(true);
            }
        }
        #endregion
    }

    // Start is called before the first frame update
    void Start()
    {
        #region Settings Player Info
        groupNameText.text = PlayerSaveData.instance.groupName;
        playerNameText.text = "Hello, " + PlayerSaveData.instance.playerName + "!";
        #endregion
    }

    public void LockMenu()
    {
        IsMenuLocked = true;
    }

    public void UnlockMenu()
    {
        IsMenuLocked = false;
    }

    public void ChangeMenu(Transform menu)
    {
        if (!PlayerSaveData.instance.tutorialDone) return;

        if (IsMenuLocked) { return; }
        // Inbox => 7-5 = 2
        int siblingIndex = menu.transform.GetSiblingIndex();

        int indexChoosed = inactiveMenus.Count - siblingIndex;
        indexChoosed = indexChoosed > 0 ? indexChoosed - 1 : 0;
        
        if (indexChoosed != activeMenuIndex)
        {
            if(interval.GetSiblingIndex() < siblingIndex && siblingIndex < inactiveMenus.Count)
            {
                indexChoosed += 1;
            }

            interval.SetSiblingIndex(siblingIndex);
            activeMenus[activeMenuIndex].SetActive(false);
            inactiveMenus[indexChoosed].SetActive(false);
            contentMenus[activeMenuIndex].SetActive(false);

            activeMenus[indexChoosed].SetActive(true);
            inactiveMenus[activeMenuIndex].SetActive(true);
            contentMenus[indexChoosed].SetActive(true);

            activeMenuIndex = indexChoosed;
        }
    }

    public void ChangeMenu(int choosedIndex)
    {
        if (IsMenuLocked) { return; }
        int siblingIndex = choosedIndex;
        int indexChoosed = inactiveMenus.Count - siblingIndex;
        indexChoosed = indexChoosed > 0 ? indexChoosed - 1 : 0;

        if (indexChoosed != activeMenuIndex)
        {
            if (interval.GetSiblingIndex() < siblingIndex && siblingIndex < inactiveMenus.Count)
            {
                indexChoosed += 1;
            }

            interval.SetSiblingIndex(siblingIndex);
            activeMenus[activeMenuIndex].SetActive(false);
            inactiveMenus[indexChoosed].SetActive(false);
            contentMenus[activeMenuIndex].SetActive(false);

            activeMenus[indexChoosed].SetActive(true);
            inactiveMenus[activeMenuIndex].SetActive(true);
            contentMenus[indexChoosed].SetActive(true);

            activeMenuIndex = indexChoosed;
        }
    }
}
