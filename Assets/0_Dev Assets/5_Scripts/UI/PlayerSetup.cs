using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;

public class PlayerSetup : MonoBehaviour
{
    [Header("FOR DEBUGGING PURPOSES")]
    [SerializeField] int debugOpenedPanels = 0;

    [Header("Common Setup")]
    [SerializeField] GameObject[] UIPanels;
    int indexOfOpenedPanels = 0;
    PlayerSaveData playerData;

    [Header("Ask Player's Name UI")]
    [SerializeField] TMP_InputField playerNameField;
    [SerializeField] Vector2Int playerNameRange;
    [SerializeField] TextMeshProUGUI buttonOKPlayerNameText;
    [SerializeField] Button buttonOKPlayerName;
    [SerializeField] Image buttonOKImagePlayerName;
    [SerializeField] Sprite defaultButtonOKSprite;
    [SerializeField] Sprite activeButtonOKSprite;
    bool isButtonOKPlayerNameActive = false;

    [Header("Select Trainees")]
    [SerializeField] GameObject[] traineesCards;
    [SerializeField] Button buttonNextStep;
    [SerializeField] Image buttonNextStepImage;
    [SerializeField] Sprite defaultButtonNextStepSprite;
    [SerializeField] Sprite activeButtonNextStepSprite;
    List<int> choosedTrainees = new List<int>();

    [Header("Ask Group's Name UI")]
    [SerializeField] TMP_InputField groupNameField;
    [SerializeField] Vector2Int groupNameRange;
    [SerializeField] TextMeshProUGUI buttonOKGroupNameText;
    [SerializeField] Button buttonOKGroupName;
    [SerializeField] Image buttonOKImageGroupName;
    [SerializeField] Sprite defaultButtonOKGroupNameSprite;
    [SerializeField] Sprite activeButtonOKGroupNameSprite;
    bool isButtonOKGroupNameActive = false;

    [Header("Choose Group Logo")]
    [SerializeField] GameObject[] logoCards;
    [SerializeField] TextMeshProUGUI buttonOKGroupLogoText;
    [SerializeField] Button buttonOKGroupLogo;
    [SerializeField] Image buttonOKGroupLogoImage;
    [SerializeField] Color defaultButtonOKGroupLogoTextColor;
    [SerializeField] Sprite defaultButtonOKGroupLogoSprite;
    [SerializeField] Sprite activeButtonOKGroupLogoSprite;
    [SerializeField] GameObject confirmationGroupLogo;
    int choosedLogo = -1;

    [Header("Congratulations & Loading Screen")]
    [SerializeField] float delayedLoadingToGameScene = 1f;

    private void Start()
    {
        playerData = PlayerSaveData.instance;
        foreach (GameObject panel in UIPanels) 
        {
            panel.SetActive(false);
        }

        indexOfOpenedPanels = debugOpenedPanels;
        ShowPanel(indexOfOpenedPanels);
        buttonOKPlayerName.interactable = isButtonOKPlayerNameActive;
        buttonOKGroupName.interactable = isButtonOKGroupNameActive;
    }

    #region SAVE_PLAYER_DATA
    /// <summary>
    /// Ask Player Name - OK Button
    /// </summary>
    public void ConfirmPlayerName()
    {
        playerData.playerName = playerNameField.text.ToString();
        // Save Data of player name?

        // Continue to Ask Which Group Panel
        ShowPanel(indexOfOpenedPanels+1);
    }

    /// <summary>
    /// Ask Which Group - Choose Group Button
    /// </summary>
    public void ChooseBoyGroup()
    {
        //Debug.Log("Choose " + Band.group.boy.ToString() + " Group");
        //playerData.bandGroup = Band.group.boy;

        // Save group band choice progress?
        ChooseGroup();
    }

    public void ChooseGirlGroup()
    {
        //Debug.Log("Choose " + Band.group.girl.ToString() + " Group");
        //playerData.bandGroup = Band.group.girl;

        // Save group band choice progress?
        ChooseGroup();
    }

    void ChooseGroup()
    {
        TraineesDisplayer.instance.SetupTraineesGroupUI();
        ShowPanel(indexOfOpenedPanels+1);
    }

    /// <summary>
    /// Select Trainees - Next Step Button
    /// </summary>
    public void ChooseTheseTrainees()
    {
        // Add Chosen Trainees To Player Data Debut Trainees
        var traineesDisplayer = TraineesDisplayer.instance;
        foreach(int index in choosedTrainees)
        {
            playerData.debutTraineesIndex.Add(index);
        }

        ShowPanel(indexOfOpenedPanels + 1);
    }

    /// <summary>
    /// Ask Group Name - OK Button
    /// </summary>
    public void ConfirmGroupName()
    {
        playerData.groupName = groupNameField.text.ToString();
        // Save Data of group name?

        // Choose Group Logo
        LogosDisplayer.instance.SetupLogos();
        confirmationGroupLogo.SetActive(false);
        ShowPanel(indexOfOpenedPanels + 1);
    }

    ///<summary>
    /// Choose Group Logo - OK Button
    ///</summary>
    public void OKChooseGroup()
    {
        confirmationGroupLogo.SetActive(true);
    }

    /// <summary>
    /// Confirm Group Logo - No Button
    /// </summary>
    public void NotConfirmGroupLogo()
    {
        confirmationGroupLogo.SetActive(false);
    }

    /// <summary>
    /// Confirm Group Logo - Yes Button
    /// </summary>
    public void ConfirmGroupLogo()
    {
        CongratulationsLoadingScreen();
    }

    public void CongratulationsLoadingScreen()
    {
        ShowPanel(indexOfOpenedPanels + 1);
        SaveSystem.instance.SaveSetupData();
        SaveSystem.instance.isPlayerAlreadySetup = true;

        GoToGameScene();
    }

    private void GoToGameScene()
    {
        if (indexOfOpenedPanels == 0)
        {
            ChangeToGameScene();
        }
        else
        {
            Action changeToGameScene = ChangeToGameScene;
            Invoke(changeToGameScene.Method.Name, delayedLoadingToGameScene);
        }
    }

    private void ChangeToGameScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    #endregion

    #region SHOW & CLOSE UI
    void ShowPanel(int index)
    {
        if(index > 1) 
        {
            ClosePanel(index - 1); 
        }
        
        if(index == 3)
        {
            ClosePanel(0);
        }

        UIPanels[index].SetActive(true);
        indexOfOpenedPanels = index;
    }

    void ClosePanel(int index)
    {
        UIPanels[index].SetActive(false);
    }

    public void Continue() 
    {
        if (indexOfOpenedPanels == 0 && SaveSystem.instance.isPlayerAlreadySetup)
        {
            GoToGameScene();
        }
        else
        {
            ShowPanel(indexOfOpenedPanels + 1);
        }
    }
    #endregion

    #region ASK_PLAYER_NAME
    public void PlayerNameChanged()
    {
        bool lastIsButtonOKActive = isButtonOKPlayerNameActive;
        
        if (playerNameField.text.Length < playerNameRange.x || playerNameField.text.Length > playerNameRange.y)
        {
            isButtonOKPlayerNameActive = false;
        }
        else
        {
            isButtonOKPlayerNameActive = true;
        }

        if (lastIsButtonOKActive != isButtonOKPlayerNameActive)
        {
            buttonOKImagePlayerName.sprite = isButtonOKPlayerNameActive ? activeButtonOKSprite : defaultButtonOKSprite;
            buttonOKPlayerNameText.color = isButtonOKPlayerNameActive ? Color.black : Color.white;
            buttonOKPlayerName.interactable = isButtonOKPlayerNameActive;
        }
    }
    #endregion

    #region SELECT_TRAINEES
    public void ChooseThisTrainee(GameObject trainee)
    {
        // Set choosed/unchoosed trainee
        var choosedBackgroundImage = trainee.transform.GetChild(0).gameObject;
        bool isChoosed = choosedBackgroundImage.activeSelf;

        // Get the index of the trainees 
        int indexChoosed = trainee.transform.GetSiblingIndex();

        // First check if the trainees is already chosen
        if (isChoosed)
        {
            bool alreadyChoosen = false;
            int indexChoosedTrainees = 0;
            for (int i = 0; i < choosedTrainees.Count; i++)
            {
                if (choosedTrainees[i] == indexChoosed)
                {
                    alreadyChoosen = true;
                    indexChoosedTrainees = i;
                    break;
                }
            }

            if (alreadyChoosen)
            {
                choosedTrainees.RemoveAt(indexChoosedTrainees);
                choosedBackgroundImage.SetActive(!isChoosed);
                if (choosedTrainees.Count < 4)
                {
                    buttonNextStepImage.sprite = defaultButtonNextStepSprite;
                    buttonNextStep.interactable = false;
                }
            }
        }
        else
        {
            if(choosedTrainees.Count < 4)
            {
                choosedTrainees.Add(indexChoosed);
                choosedBackgroundImage.SetActive(!isChoosed);
                if (choosedTrainees.Count >= 4)
                {
                    buttonNextStepImage.sprite = activeButtonNextStepSprite;
                    buttonNextStep.interactable = true;
                }
            }
        }

        DebugChooseThisTrainee();
    }

    void DebugChooseThisTrainee()
    {
        string choosedTraineesString = "Choosed Trainees: ";
        for(int i = 0; i < choosedTrainees.Count; i++)
        {
            if(i > 0) { choosedTraineesString += ", "; }
            choosedTraineesString += choosedTrainees[i].ToString();
        }
        Debug.Log(choosedTraineesString);
    }
    #endregion

    #region ASK_GROUP_NAME
    public void GroupNameChanged()
    {
        bool lastIsButtonOKActive = isButtonOKGroupNameActive;

        if (groupNameField.text.Length < groupNameRange.x || groupNameField.text.Length > groupNameRange.y)
        {
            isButtonOKGroupNameActive = false;
        }
        else
        {
            isButtonOKGroupNameActive = true;
        }

        if (lastIsButtonOKActive != isButtonOKGroupNameActive)
        {
            buttonOKImageGroupName.sprite = isButtonOKGroupNameActive ? activeButtonOKGroupNameSprite : defaultButtonOKGroupNameSprite;
            buttonOKGroupNameText.color = isButtonOKGroupNameActive ? Color.black : Color.white;
            buttonOKGroupName.interactable = isButtonOKGroupNameActive;
        }
    }
    #endregion

    #region CHOOSE_GROUP_LOGO
    public void ChooseThisLogo(GameObject logo)
    {
        // Set choosed logo active
        var choosedBackgroundImage = logo.transform.GetChild(0).gameObject;
        int newChoosedLogo = logo.transform.GetSiblingIndex();

        if(choosedLogo == newChoosedLogo)
        {
            choosedBackgroundImage.SetActive(false);
            choosedLogo = -1;
            buttonOKGroupLogo.interactable = false;
            buttonOKGroupLogoImage.sprite = defaultButtonOKGroupLogoSprite;
            buttonOKGroupLogoText.color = defaultButtonOKGroupLogoTextColor;
        }
        else
        {
            if (choosedLogo >= 0)
            {
                var previousChoosedLogo = logoCards[choosedLogo].transform.GetChild(0).gameObject;
                previousChoosedLogo.SetActive(false);
            }

            choosedBackgroundImage.SetActive(true);
            choosedLogo = newChoosedLogo;
            buttonOKGroupLogo.interactable = true;
            buttonOKGroupLogoImage.sprite = activeButtonOKGroupLogoSprite;
            buttonOKGroupLogoText.color = Color.white;
        }
    }
    #endregion
}
