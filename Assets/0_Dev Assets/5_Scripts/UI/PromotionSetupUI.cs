using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class PromotionSetupUI : MonoBehaviour
{
    public TextMeshProUGUI tvPromotionText;
    public TextMeshProUGUI tvPopularityText;
    public TextMeshProUGUI bookingCostPerWeek;
    public Button bookNowButton;
    [HideInInspector] public Promotion promotion;
}
