using UnityEngine;

public class QuitGameUI : MonoBehaviour
{
    [SerializeField] GameObject quitGamePanel;

    // Start is called before the first frame update
    void Start()
    {
        quitGamePanel.SetActive(false);        
    }

    public void ShowQuitGamePanel()
    {
        quitGamePanel.SetActive(true);
    }

    public void CancelQuitGame()
    {
        // We can make animation out of this to close the quit game panel
        quitGamePanel.SetActive(false);
    }

    public void QuitGame() 
    {
        PlayerGameData.instance.SavePlayerGameData();
        Application.Quit();
    }
}
