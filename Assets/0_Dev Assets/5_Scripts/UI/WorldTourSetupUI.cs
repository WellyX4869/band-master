using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public enum WorldTourLocation
{
    Indonesia,
    USA,
    Europe,
    London,
    South_Korea,
    Japan
}

public class WorldTourSetupUI : MonoBehaviour
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI capacityText;
    public TextMeshProUGUI bookingCostPerDayText;
    public Button bookNowButton;
    public Image coloredCard;
    [HideInInspector] public WorldTour worldTour;
}
