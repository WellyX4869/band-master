using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MailSetupUI : MonoBehaviour
{
    public MailType mailType;
    public GameObject isNewStatus;
    [HideInInspector] public bool hasBeenRead;
    [HideInInspector] public bool isNew;
}
