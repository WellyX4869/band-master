using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class MemberSetupUI : MonoBehaviour
{
    public Image memberSmallImage;
    public TextMeshProUGUI memberName;
    public Image memberSmallImageHovered;
    public TextMeshProUGUI memberNameHovered;
    [HideInInspector] public Trainee memberTrainee;
}
