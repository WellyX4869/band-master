using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TraineeSetupUI : MonoBehaviour
{
    public TextMeshProUGUI traineeNameText;
    public TextMeshProUGUI traineeAgeText;
    public TextMeshProUGUI traineePositionText;

    public Image traineePhoto;
    public GameObject choosedIcon;
}
