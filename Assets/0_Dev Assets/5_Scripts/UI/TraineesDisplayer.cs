using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraineesDisplayer : MonoBehaviour
{
    public static TraineesDisplayer instance;

    [Header("Selection of Trainees")]
    public List<Trainee> debutTrainees = new List<Trainee>();
    [SerializeField] List<TraineeSetupUI> traineesUI = new List<TraineeSetupUI>();
    [SerializeField] Sprite blankTraineeSprite;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public void SetupTraineesGroupUI()
    {
        // Set each scriptable trainee object to Trainee UI
        for (int i = 0; i < debutTrainees.Count; i++)
        {
            // Setup Trainees Informations
            traineesUI[i].traineeNameText.text = debutTrainees[i].traineeName == "" ? "Name":debutTrainees[i].traineeName;
            traineesUI[i].traineeAgeText.text = debutTrainees[i].age == 0? "21" : debutTrainees[i].age.ToString();

            traineesUI[i].traineePositionText.text = "";
            traineesUI[i].traineePositionText.text += debutTrainees[i].position.ToString();

            // Setup Trainees Picture
            traineesUI[i].traineePhoto.sprite = blankTraineeSprite;
            if (debutTrainees[i].debutPhoto != null)
            {
                traineesUI[i].traineePhoto.sprite = debutTrainees[i].debutPhoto;
            }
        }
    }

    public bool CheckChosenTraineesMeetPositionRequirements(List<int> choosedTrainees, int traineesCount)
    {
        // If less or more than required amount of position requirements, then automatically return false
        if(choosedTrainees.Count != traineesCount) { return false; }

        List<bool> positionChecks = new List<bool>();
        for(int i = 0; i<traineesCount; i++)
        {
            positionChecks.Add(false);
        }

        foreach(int indexChoosed in choosedTrainees)
        {
            int positionIndex = (int)debutTrainees[indexChoosed].position;
            positionChecks[positionIndex] = true;
        }

        // Has That Many Required Position
        int haveRequiredPositionCount = 0;
        for(int i = 0; i<positionChecks.Count; i++)
        {
            if (positionChecks[i]) { haveRequiredPositionCount++; }
        }

        return haveRequiredPositionCount == traineesCount ? true : false;
    }
}
