using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;
using DG.Tweening;

public class PlayerSetupHigh : MonoBehaviour
{
    [Header("FOR DEBUGGING PURPOSES")]
    [SerializeField] int debugOpenedPanels = 0;

    [Header("Common Setup")]
    [SerializeField] GameObject[] UIPanels;
    [SerializeField] float fadeInDuration;
    [SerializeField] float fadeOutDuration;
    int indexOfOpenedPanels = 0;
    PlayerSaveData playerData;

    [Header("Start Screen")]
    [SerializeField] TextMeshProUGUI clickOrTouchToContinueText;
    [SerializeField] float textAnimChangeDimDuration;

    [Header("Ask Player's Name UI")]
    [SerializeField] TMP_InputField playerNameField;
    [SerializeField] Vector2Int playerNameRange;
    [SerializeField] Button buttonNextPlayerName;
    [SerializeField] Image buttonNextImagePlayerName;
    [SerializeField] Sprite defaultButtonNextSprite;
    [SerializeField] Sprite activeButtonNextSprite;
    bool isButtonNextPlayerNameActive = false;

    [Header("Select Trainees")]
    [SerializeField] Button buttonNextStep;
    [SerializeField] Image buttonNextStepImage;
    [SerializeField] Sprite defaultButtonNextStepSprite;
    [SerializeField] Sprite activeButtonNextStepSprite;
    List<int> choosedTrainees = new List<int>();
    [SerializeField] int requiredTraineesCount;
    [SerializeField] GameObject confirmationTraineeSelection;
    [SerializeField] Scrollbar selectTraineesScrollBar;
    [SerializeField] float scrollBarSnapDuration;

    [Header("Ask Group's Name UI")]
    [SerializeField] TMP_InputField groupNameField;
    [SerializeField] Vector2Int groupNameRange;
    [SerializeField] Button buttonOKGroupName;
    [SerializeField] Image buttonOKImageGroupName;
    [SerializeField] Sprite defaultButtonOKGroupNameSprite;
    [SerializeField] Sprite activeButtonOKGroupNameSprite;
    [SerializeField] GameObject confirmationGroupName;
    bool isButtonOKGroupNameActive = false;

    [Header("Choose Group Logo")]
    [SerializeField] Button buttonNextGroupLogo;
    [SerializeField] Image buttonNextGroupLogoImage;
    [SerializeField] Sprite defaultButtonNextGroupLogoSprite;
    [SerializeField] Sprite activeButtonNextGroupLogoSprite;
    [SerializeField] Sprite defaultLogoSprite;
    [SerializeField] Image choosedLogoImage;
    [SerializeField] GameObject selectYourGroupLogo;
    [SerializeField] TextMeshProUGUI logoGroupNameText;
    [SerializeField] GameObject confirmationGroupLogo;
    int choosedLogo = -1;

    private void Start()
    {
        playerData = PlayerSaveData.instance;
        foreach (GameObject panel in UIPanels)
        {
            panel.SetActive(false);
        }

        indexOfOpenedPanels = debugOpenedPanels;
        ShowPanel(indexOfOpenedPanels);
        StartScreenAnimation();
        buttonNextPlayerName.interactable = isButtonNextPlayerNameActive;
        buttonOKGroupName.interactable = isButtonOKGroupNameActive;
    }

    #region SAVE_PLAYER_DATA
    /// <summary>
    /// Ask Player Name - OK Button
    /// </summary>
    public void ConfirmPlayerName()
    {
        playerData.playerName = playerNameField.text.ToString();
        // Save Data of player name?

        // Continue to Selection Trainees Panel
        if (UIPanels[0].activeSelf) { UIPanels[0].SetActive(false); }
 
        TraineesDisplayer.instance.SetupTraineesGroupUI();
        ShowPanelWithFadeAnimation(indexOfOpenedPanels + 1);
    }

    /// <summary>
    /// Select Trainees - Next Step Button
    /// </summary>
    public void ChooseTheseTrainees()
    {
        // Add Chosen Trainees To Player Data Debut Trainees
        var traineesDisplayer = TraineesDisplayer.instance;
        foreach (int index in choosedTrainees)
        {
            playerData.debutTraineesIndex.Add(index);
            if(traineesDisplayer.debutTrainees[index].position == TraineePosition.Vocalist)
            {
                playerData.vocalistSex = traineesDisplayer.debutTrainees[index].sex;
            }
        }

        ShowPanelWithFadeAnimation(indexOfOpenedPanels + 1);
    }

    /// <summary>
    /// Ask Group Name - OK Button
    /// </summary>
    public void ConfirmGroupName()
    {
        playerData.groupName = groupNameField.text.ToString();
        // Save Data of group name?

        /// Group Logo is excluded for now
        // Choose Group Logo
        //logoGroupNameText.text = PlayerSaveData.instance.groupName;
        //LogosDisplayer.instance.SetupLogos();
        //confirmationGroupLogo.SetActive(false);
        GameManager.instance.LoadingToGameScene();
    }

    /// <summary>
    /// Confirm Group Logo - Yes Button
    /// </summary>
    public void ConfirmGroupLogo()
    {
        GameManager.instance.LoadingToGameScene();
    }
    #endregion

    #region SHOW & CLOSE UI
    void ShowPanel(int index)
    {
        if (index > 1)
        {
            ClosePanel(index - 1);
        }

        if (index == 3)
        {
            ClosePanel(0);
        }

        UIPanels[index].SetActive(true);
        indexOfOpenedPanels = index;
    }

    void ClosePanel(int index)
    {
        UIPanels[index].SetActive(false);
    }

    void ShowPanelWithFadeAnimation(int index)
    {
        if (index > 1)
        {
            ClosePanelWithFadeAnimation(index - 1);
        }

        if (index == 2)
        {
            ClosePanel(0);
        }

        StartCoroutine(FadeIn(UIPanels[index]));
        indexOfOpenedPanels = index;
    }

    void ClosePanelWithFadeAnimation(int index)
    {
        StartCoroutine(FadeOut(UIPanels[index]));
    }

    IEnumerator FadeIn(GameObject panel)
    {
        var panelImages = panel.GetComponentsInChildren<Image>();
        var panelTexts = panel.GetComponentsInChildren<TextMeshProUGUI>();
        foreach (Image panelImage in panelImages)
        {
            Color defaultColor = new Color(panelImage.color.r, panelImage.color.g, panelImage.color.b, panelImage.color.a);
            panelImage.color = new Color(0, 0, 0, 0);
            panelImage.DOColor(defaultColor, fadeInDuration);
        }
        foreach (TextMeshProUGUI panelText in panelTexts)
        {
            Color defaultColor = new Color(panelText.color.r, panelText.color.g, panelText.color.b, panelText.color.a);
            panelText.color = new Color(0, 0, 0, 0);
            panelText.DOColor(defaultColor, fadeInDuration);
        }
        panel.SetActive(true);
        yield return new WaitForSeconds(fadeInDuration);
    }

    IEnumerator FadeOut(GameObject panel)
    {
        var panelImages = panel.GetComponentsInChildren<Image>();
        var panelTexts = panel.GetComponentsInChildren<TextMeshProUGUI>();
        List<Color> defaultPanelImagesColors = new List<Color>();
        List<Color> defaultPanelTextsColors = new List<Color>();
        foreach (Image panelImage in panelImages)
        {
            defaultPanelImagesColors.Add(panelImage.color);
            panelImage.DOColor(new Color(0, 0, 0, 0), fadeOutDuration);
        }
        foreach(TextMeshProUGUI panelText in panelTexts)
        {
            defaultPanelTextsColors.Add(panelText.color);
            panelText.DOColor(new Color(0, 0, 0, 0), fadeOutDuration);
        }

        yield return new WaitForSeconds(fadeOutDuration);
        panel.SetActive(false); int counterIndex = 0;
        foreach (Image panelImage in panelImages)
        {
            panelImage.DOColor(defaultPanelImagesColors[counterIndex], 0);
            counterIndex++;
        }
        counterIndex = 0;
        foreach (TextMeshProUGUI panelText in panelTexts)
        {
            panelText.DOColor(defaultPanelTextsColors[counterIndex], 0);
            counterIndex++;
        }
    }

    #endregion

    #region START_SCREEN
    private void StartScreenAnimation()
    {
        clickOrTouchToContinueText.DOColor(new Color(1, 1, 1, 1f), textAnimChangeDimDuration).SetLoops(-1, LoopType.Yoyo);
    }

    private void StopStartScreenAnimation()
    {
        clickOrTouchToContinueText.DOKill();
    }

    public void Continue()
    {
        if (indexOfOpenedPanels == 0 && PlayerSaveData.instance.IsPlayerAlreadySetup)
        {
            GameManager.instance.LoadingToGameScene();
        }
        else
        {
            StopStartScreenAnimation();
            ShowPanelWithFadeAnimation(indexOfOpenedPanels + 1);
        }
    }
    #endregion

    #region ASK_PLAYER_NAME
    public void PlayerNameChanged()
    {
        bool lastIsButtonNextActive = isButtonNextPlayerNameActive;

        if (playerNameField.text.Length < playerNameRange.x || playerNameField.text.Length > playerNameRange.y)
        {
            isButtonNextPlayerNameActive = false;
        }
        else
        {
            isButtonNextPlayerNameActive = true;
        }

        if (lastIsButtonNextActive != isButtonNextPlayerNameActive)
        {
            buttonNextImagePlayerName.sprite = isButtonNextPlayerNameActive ? activeButtonNextSprite : defaultButtonNextSprite;
            buttonNextPlayerName.interactable = isButtonNextPlayerNameActive;
        }
    }
    #endregion

    #region SELECT_TRAINEES
    public void ChooseThisTrainee(TraineeSetupUI trainee)
    {
        // Set choosed/unchoosed trainee
        var choosedIcon = trainee.choosedIcon;
        bool isChoosed = choosedIcon.activeSelf;

        // Get the index of the trainees 
        int indexChoosed = trainee.transform.GetSiblingIndex();

        // First check if the trainees is already chosen
        if (isChoosed)
        {
            bool alreadyChoosen = false;
            int indexChoosedTrainees = 0;
            for (int i = 0; i < choosedTrainees.Count; i++)
            {
                if (choosedTrainees[i] == indexChoosed)
                {
                    alreadyChoosen = true;
                    indexChoosedTrainees = i;
                    break;
                }
            }

            if (alreadyChoosen)
            {
                choosedTrainees.RemoveAt(indexChoosedTrainees);
                choosedIcon.SetActive(!isChoosed);

                if (choosedTrainees.Count < requiredTraineesCount)
                {
                    buttonNextStepImage.sprite = defaultButtonNextStepSprite;
                    buttonNextStep.interactable = false;
                }
            }
        }
        else
        {
            if (choosedTrainees.Count < requiredTraineesCount)
            {
                choosedTrainees.Add(indexChoosed);
                choosedIcon.SetActive(!isChoosed);

                // Check position requirements
                bool isMeetingPositionRequirements = TraineesDisplayer.instance.CheckChosenTraineesMeetPositionRequirements(choosedTrainees, requiredTraineesCount);

                if (isMeetingPositionRequirements)
                {
                    buttonNextStepImage.sprite = activeButtonNextStepSprite;
                    buttonNextStep.interactable = true;
                    SelectionTraineesScrollBarSnapToEnd();
                }
            }
        }

        DebugChooseThisTrainee();
    }

    void SelectionTraineesScrollBarSnapToEnd()
    {
        // Tween a float called scrollbar value to 0 in snap duration in second
        DOTween.To(() => selectTraineesScrollBar.value, x => selectTraineesScrollBar.value = x, 0, scrollBarSnapDuration);
    }

    void DebugChooseThisTrainee()
    {
        string choosedTraineesString = "Choosed Trainees: ";
        for (int i = 0; i < choosedTrainees.Count; i++)
        {
            if (i > 0) { choosedTraineesString += ", "; }
            choosedTraineesString += choosedTrainees[i].ToString();
        }
        Debug.Log(choosedTraineesString);
    }

    public void ShowConfirmTraineeSelection()
    {
        StartCoroutine(FadeIn(confirmationTraineeSelection));
    }

    public void ConfirmTraineesSelection()
    {
        StartCoroutine(FadeOut(confirmationTraineeSelection));
        ChooseTheseTrainees();
    }

    public void CancelTraineesSelection()
    {
        StartCoroutine(FadeOut(confirmationTraineeSelection));
    }
    #endregion

    #region ASK_GROUP_NAME
    public void GroupNameChanged()
    {
        bool lastIsButtonOKActive = isButtonOKGroupNameActive;

        if (groupNameField.text.Length < groupNameRange.x || groupNameField.text.Length > groupNameRange.y)
        {
            isButtonOKGroupNameActive = false;
        }
        else
        {
            isButtonOKGroupNameActive = true;
        }

        if (lastIsButtonOKActive != isButtonOKGroupNameActive)
        {
            buttonOKImageGroupName.sprite = isButtonOKGroupNameActive ? activeButtonOKGroupNameSprite : defaultButtonOKGroupNameSprite;
            buttonOKGroupName.interactable = isButtonOKGroupNameActive;
        }
    }

    public void ShowConfirmGroupName()
    {
        StartCoroutine(FadeIn(confirmationGroupName));
    }

    public void CancelGroupNameConfirmation()
    {
        StartCoroutine(FadeOut(confirmationGroupName));
    }
    #endregion

    #region CHOOSE_GROUP_LOGO
    public void ChooseThisLogo(GameObject logo)
    {
        // Set choosed logo active
        int newChoosedLogo = logo.transform.GetSiblingIndex();

        if (choosedLogo == newChoosedLogo)
        {
            choosedLogo = -1;
            selectYourGroupLogo.SetActive(true);
            choosedLogoImage.sprite = defaultLogoSprite;
            buttonNextGroupLogo.interactable = false;
            buttonNextGroupLogoImage.sprite = defaultButtonNextGroupLogoSprite;
        }
        else
        {
            choosedLogo = newChoosedLogo;
            selectYourGroupLogo.SetActive(false);
            choosedLogoImage.sprite = logo.GetComponent<Image>().sprite;
            buttonNextGroupLogo.interactable = true;
            buttonNextGroupLogoImage.sprite = activeButtonNextGroupLogoSprite;
            
        }
    }

    ///<summary>
    /// Show Confirmation Group Logo - Next Button
    ///</summary>
    public void ShowConfirmationGroupLogo()
    {
        StartCoroutine(FadeIn(confirmationGroupLogo));
    }

    public void CancelConfirmationGroupLogo()
    {
        StartCoroutine(FadeOut(confirmationGroupLogo));
    }
    #endregion
}
