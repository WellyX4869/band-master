using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogosDisplayer : MonoBehaviour
{
    public static LogosDisplayer instance;

    [Header("Choose Logos")]
    public List<Logo> logos = new List<Logo>();
    [SerializeField] List<LogoSetupUI> logosUI = new List<LogoSetupUI>();

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public void SetupLogos()
    {
        for(int i = 0; i<logos.Count; i++)
        {
            logosUI[i].logoName = logos[i].logoName;
            //logosUI[i].transform.name = logos[i].name;
            if (logos[i].logo != null)
                logosUI[i].logoImage = logos[i].logo;
        }
    }
}
