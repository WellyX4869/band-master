using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScheduleSetupUI : MonoBehaviour
{
    [HideInInspector] public bool IsPicked = false;
    public Button checkBoxButton;
    public Image checkBoxImage;
    public TextMeshProUGUI dateText;
    public TextMeshProUGUI timeText;
    [HideInInspector] public BandSchedule bandSchedule;
}
