using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class StatusSymbolUI : MonoBehaviour
{
    public TextMeshProUGUI productName;
    public TextMeshProUGUI productPrice;
    public Image productFilledImage;
    public Image productFrontImage;
    public Button seeDetails;
    public TextMeshProUGUI seeDetailsText;
    [HideInInspector] public StatusSymbol statusSymbolItem;
    public GameObject soldOut;
}
