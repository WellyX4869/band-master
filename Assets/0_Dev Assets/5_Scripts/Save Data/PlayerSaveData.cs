using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PlayerSaveData : MonoBehaviour
{
    public static PlayerSaveData instance;

    [Serializable]
    public class PlayerSetupData
    {
        public string playerName;
        public Sex vocalistSex;
        public string groupName;
        public List<int> debutTraineesIndex = new List<int>();
        public bool tutorialDone;
        public DateTime establishedDate;
        public bool IsPlayerAlreadySetup;
    }

    /// <summary>
    /// Player's Setup Data
    /// </summary>
    public string playerName;
    public Sex vocalistSex;
    public string groupName;
    public List<int> debutTraineesIndex = new List<int>();
    public bool tutorialDone = false;
    public DateTime establishedDate;
    public bool IsPlayerAlreadySetup;
    string filePath = "/playersetup";

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }

        if (LoadPlayerSetupData() == true)
        {
            IsPlayerAlreadySetup = true;
        }
    }

    public void SavePlayerSetupData()
    {
        PlayerSetupData saveItem = new PlayerSetupData();

        saveItem.playerName     = playerName;
        saveItem.groupName      = groupName;
        saveItem.debutTraineesIndex  = debutTraineesIndex;
        saveItem.vocalistSex    = vocalistSex;
        saveItem.tutorialDone   = tutorialDone;
        saveItem.establishedDate = establishedDate != null ? establishedDate : DateTime.Now;
        saveItem.IsPlayerAlreadySetup = IsPlayerAlreadySetup;

        string path = Application.persistentDataPath + filePath;
        string saveJson = JsonUtility.ToJson(saveItem, true);
        File.WriteAllText(path, saveJson);
    }

    public bool LoadPlayerSetupData()
    {
        try
        {
            string path = Application.persistentDataPath + filePath;
            
            string loadJson = File.ReadAllText(path);
            PlayerSetupData loadItem = JsonUtility.FromJson<PlayerSetupData>(loadJson);

            playerName      = loadItem.playerName;
            groupName       = loadItem.groupName;
            debutTraineesIndex   = loadItem.debutTraineesIndex;
            vocalistSex     = loadItem.vocalistSex;
            tutorialDone    = loadItem.tutorialDone;
            establishedDate = loadItem.establishedDate;
            IsPlayerAlreadySetup = loadItem.IsPlayerAlreadySetup;
            return true;
        }
        catch
        {
            return false;
        }
    }

    public void DeletePlayerSetupData()
    {
        string path = Application.persistentDataPath + filePath;
        IsPlayerAlreadySetup = false;
        SavePlayerSetupData();
        File.Delete(path);
        Destroy(this.gameObject);
    }
}
