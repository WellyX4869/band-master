using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    public static SaveSystem instance;
    public bool isPlayerAlreadySetup = false;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        if (PlayerSaveData.instance.LoadPlayerSetupData())
        {
            // Go To Game Menu UI
            isPlayerAlreadySetup = true;
        }
        else
        {
            isPlayerAlreadySetup = false;
        }
    }

    public bool LoadData()
    {
        return true;
    }

    public void SaveSetupData()
    {
        PlayerSaveData.instance.SavePlayerSetupData();
        DebuggerPlayerData();
    }

    void DebuggerPlayerData()
    {
        PlayerSaveData playerData = PlayerSaveData.instance;
        string log = "";
        log += "Player=========\n";
        log += "Name            : " + playerData.name + "\n";
        log += "Vocal Sex       : " + playerData.vocalistSex + "\n";
        log += "Group Name      : " + playerData.groupName + "\n";
        log += "Tutorial Done   : " + playerData.tutorialDone + "\n";
        log += "Debug Trainees  : " + playerData.debutTraineesIndex + "\n";
    }
}
