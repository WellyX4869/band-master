using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[CreateAssetMenu(menuName = "Status Symbol")]
public class StatusSymbol : ScriptableObject
{
    public int productIndex;
    public string productName;
    public int productPrice;
    public string productDescription;
    public Sprite productFrontPhoto;
    public Sprite productDetailPhoto;
    public Sprite productCheckoutPhoto;
    public Vector3 checkoutPhotoPosition;
}
