using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Promotion")]
public class Promotion : ScriptableObject
{
    public string tvName;
    public int tvPopularity;
    public int capacity;
    public int bookingCostPerDay;
    public int staminaCostPerDay;
    public int moneyCostPerDay;
    public int promotionDuration;

    [Header("Promotion Schedule - Each element correlated")]
    public List<Day> dayPromotions;
    public List<int> timePromotions;
}

public class BandSchedule
{
    public DateTime timeSchedule;
    public string scheduleTitle;
    public Promotion promotion;
    public bool IsActive;

    public BandSchedule(DateTime timeSchedule, int hour, string scheduleTitle, Promotion promotion)
    {
        this.timeSchedule = new DateTime(timeSchedule.Year, timeSchedule.Month, timeSchedule.Day, hour, 0, 0);
        this.scheduleTitle = scheduleTitle;
        this.promotion = promotion;
        this.IsActive = false;
    }

    public BandSchedule(DateTime timeSchedule, string scheduleTitle, Promotion promotion, bool IsActive)
    {
        this.timeSchedule = timeSchedule;
        this.scheduleTitle = scheduleTitle;
        this.promotion = promotion;
        this.IsActive = IsActive;
    }
}