using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Logo")]
public class Logo : ScriptableObject
{
    public string logoName;
    public Sprite logo;
}
