using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "World Tour")]
public class WorldTour : ScriptableObject
{
    public WorldTourLocation location;
    public string stadiumName;
    public int capacity;
    public int bookingCostPerDay;
    public int staminaCost;
    public int totalCurrencyCost;
    public Sprite coloredSprite;
    public Sprite loadingScreen;
}
