using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TraineePosition
{
    Vocalist, Guitarist, Bassist, Drummer
}

public enum Sex { male, female };

[CreateAssetMenu(menuName = "Trainee")]
public class Trainee : ScriptableObject
{
    public string traineeName;
    public int age;
    public Sex sex;
    public Sprite debutPhoto;
    public Sprite smallPhoto;
    public Sprite bigPhoto;
    public Sprite gigPhoto;
    public Sprite[] highlightPhoto;
    public TraineePosition position;
    public List<string> traits;
}
