using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class TutorialHandler : MonoBehaviour
{
    public static TutorialHandler instance;

    public enum DurationType
    {
        Normal,
        Slow
    }

    [Header("Tutorial")]
    [SerializeField] GameObject tutorialPanel;
    [SerializeField] GameObject pleaseOpenYourMail;
    [HideInInspector] public bool StillOpenYourMail = true;
    [SerializeField] GameObject tutorial;

    [Header("Detail Tutorial")]
    [SerializeField] GameObject description;
    [SerializeField] GameObject buttons;
    [SerializeField] GameObject backButton;
    [SerializeField] GameObject nextButton;
    [SerializeField] Sprite defaultButtonSprite;
    [SerializeField] Sprite activeButtonSprite;
    [SerializeField] Sprite activeBackButtonSprite;
    [SerializeField] Sprite hoveredBackButtonSprite;
    [SerializeField] Sprite activeNextButtonSprite;
    [SerializeField] Sprite hoveredNextButtonSprite;
    [SerializeField] float hideDescriptionPosY;
    [SerializeField] Color defaultBlackColor;
    float defaultDescriptionPosY;

    [Header("End of Session Tutorial")]
    [SerializeField] bool debugEndOfSession = false;
    [SerializeField] GameObject tutorialEndOfSession;
    [SerializeField] GameObject logoEndOfSession;
    [SerializeField] GameObject descriptionEndOfSession;
    [SerializeField] float hideEndOfSessionDescriptionPosY;
    float defaultEndOfSessionDescriptionPosY;

    [System.Serializable]
    public class SerializableClass
    {
        public List<GameObject> isiTutorial;
        public bool haveDescription;
        public bool haveBackButton;
        public bool isBackButtonActive;
        public bool isNextButtonActive;
        public bool isTransition;
        public DurationType durationType;
    }

    [Header("Alur")]
    [SerializeField] float transisiAlurDuration;
    [SerializeField] float slowTransisiAlurDuration;
    [SerializeField] List<SerializableClass> alurTutorial = new List<SerializableClass>();
    [SerializeField] List<SerializableClass> endOfSessionTutorial = new List<SerializableClass>();
    int alurTutorialIndex = 0;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else Destroy(gameObject);
    }

    private void Start()
    {
        tutorial.SetActive(false);
        pleaseOpenYourMail.SetActive(false);
        tutorialPanel.SetActive(false);
        defaultDescriptionPosY = description.transform.position.y;

        descriptionEndOfSession.transform.DOMoveY(hideEndOfSessionDescriptionPosY, 0, true);
        descriptionEndOfSession.SetActive(false);
        logoEndOfSession.SetActive(false);
        tutorialEndOfSession.SetActive(false);
        defaultEndOfSessionDescriptionPosY = descriptionEndOfSession.transform.position.y;

        // Just for demo or debug, dont use this but directly call invoke
        if (!PlayerSaveData.instance.tutorialDone)
        {
            Invoke("PleaseOpenYourMailTutorial", 1f);
        }
    }

    public void HoveredBackButton(Image backButtonImage)
    {
        if(backButtonImage.GetComponent<Button>().interactable)
            backButtonImage.sprite = hoveredBackButtonSprite;
    }

    public void UnhoveredBackButton(Image backButtonImage)
    {
        if (backButtonImage.GetComponent<Button>().interactable)
            backButtonImage.sprite = activeBackButtonSprite;
    }

    public void HoveredNextButton(Image nextButtonImage)
    {
        if(nextButtonImage.GetComponent<Button>().interactable)
            nextButtonImage.sprite = hoveredNextButtonSprite;
    }

    public void UnhoveredNextButton(Image nextButtonImage)
    {
        if (nextButtonImage.GetComponent<Button>().interactable)
            nextButtonImage.sprite = activeNextButtonSprite;
    }

    void PleaseOpenYourMailTutorial()
    {
        StillOpenYourMail = true;
        CommonHandler.instance.FadeInAnimation(pleaseOpenYourMail, tutorialPanel);
    }

    public void CloseOpenYourMailTutorial()
    {
        CommonHandler.instance.FadeOutAnimation(pleaseOpenYourMail, tutorialPanel, 0);
        StillOpenYourMail = false;
    }

    public void StartTutorial()
    {
        CommonHandler.instance.FadeInAnimation(tutorial, tutorialPanel);
        foreach (GameObject isi in alurTutorial[alurTutorialIndex].isiTutorial)
        {
            isi.SetActive(true);
        }

        SetIsiAlur(alurTutorialIndex);
    }

    void SetIsiAlur(int index)
    {
        var isiAlur = alurTutorial[index];
        if (!isiAlur.haveDescription)
        {
            CommonHandler.instance.FadeOutAnimation(buttons, null, 0.2f);
            CommonHandler.instance.FadeOutAnimation(description, null, 0.2f);
            description.transform.DOMoveY(hideDescriptionPosY, 0, true);
            return;
        }
        else
        {
            if (!description.activeSelf)
            {
                description.SetActive(true);
                buttons.SetActive(true);

                description.transform.DOMoveY(defaultDescriptionPosY, 0.5f, true);
            }
        }

        if (isiAlur.haveBackButton)
        {
            backButton.SetActive(true);
            if (isiAlur.isBackButtonActive)
            {
                backButton.GetComponentInChildren<TextMeshProUGUI>().color = defaultBlackColor;
                backButton.GetComponent<Image>().sprite = activeBackButtonSprite;
                backButton.GetComponent<Button>().interactable = true;
            }
            else
            {
                backButton.GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                backButton.GetComponent<Image>().sprite = defaultButtonSprite;
                backButton.GetComponent<Button>().interactable = false;
            }
        }
        else
        {
            backButton.GetComponent<Image>().sprite = defaultButtonSprite;
            backButton.GetComponent<Button>().interactable = false;
            backButton.SetActive(false);
        }

        nextButton.SetActive(true);
        if (isiAlur.isNextButtonActive)
        {
            nextButton.GetComponent<Image>().sprite = activeNextButtonSprite;
            nextButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            nextButton.GetComponent<Image>().sprite = defaultButtonSprite;
            nextButton.GetComponent<Button>().interactable = false;
        }
    }

    #region NEXT
    public void SeeNextTutorial()
    {
        StartCoroutine(NextAnimation());
    }

    IEnumerator NextAnimation()
    {
        // IS OVER TUTORIAL INDEX COUNT
        if (alurTutorialIndex + 1 >= alurTutorial.Count || debugEndOfSession)
        {
            StartCoroutine(TutorialEndOfSession());
            yield return null;
        }
        else
        {
            // WHILE IS ALUR IN TRANSITION?
            while (alurTutorial[alurTutorialIndex + 1].isTransition)
            {
                StartCoroutine(DetailNextAnimation());

                if(alurTutorial[alurTutorialIndex].durationType == DurationType.Slow)
                    yield return new WaitForSeconds(slowTransisiAlurDuration);
                else yield return new WaitForSeconds(transisiAlurDuration);

                if (alurTutorialIndex >= alurTutorial.Count - 1)
                {
                    break;
                }
            }

            if (alurTutorialIndex >= alurTutorial.Count - 1)
            {
                yield return null;
            }
            else
            {
                StartCoroutine(DetailNextAnimation());
                yield return new WaitForSeconds(transisiAlurDuration);
            }
        }
    }

    IEnumerator DetailNextAnimation()
    {
        List<GameObject> nextAlurObjects = new List<GameObject>();
        List<bool> nextAlurBools = new List<bool>();
        foreach (GameObject isi in alurTutorial[alurTutorialIndex + 1].isiTutorial)
        {
            nextAlurObjects.Add(isi);
            nextAlurBools.Add(false);
        }

        // Fade out unmacthing Isi Alur Tutorial
        foreach (GameObject isi in alurTutorial[alurTutorialIndex].isiTutorial)
        {
            bool isMatch = false;
            for (int i = 0; i < nextAlurObjects.Count; i++)
            {
                if (nextAlurBools[i] == true) continue;
                if (GameObject.ReferenceEquals(isi, nextAlurObjects[i]))
                {
                    isMatch = true;
                    nextAlurBools[i] = true;
                    break;
                }
            }

            if (isMatch)
            {
                continue;
            }
            else
            {
                bool isTutorialDescription = false;
                try
                {
                    if (isi.GetComponent<TextMeshProUGUI>() != null)
                    {
                        isTutorialDescription = true;
                    }
                }
                catch { }

                if (isTutorialDescription)
                {
                    isi.SetActive(false);
                }
                else
                {
                    CommonHandler.instance.FadeOutAnimation(isi, null, transisiAlurDuration * 0.2f);
                }
            }
        }
        SetIsiAlur(alurTutorialIndex + 1);

        // Fade in transition alur
        for (int i = 0; i < nextAlurObjects.Count; i++)
        {
            if (nextAlurBools[i]) { continue; }

            GameObject isi = nextAlurObjects[i]; bool isTutorialDescription = false;
            try
            {
                if (isi.GetComponent<TextMeshProUGUI>() != null)
                {
                    isTutorialDescription = true;
                }
            }
            catch {}

            if (isTutorialDescription)
            {
                isi.GetComponent<TextMeshProUGUI>().color = defaultBlackColor;
                isi.SetActive(true);
            }
            else
            {
                CommonHandler.instance.FadeInAnimation(isi, null, transisiAlurDuration * 0.4f);
            }
        }
        alurTutorialIndex++;
        yield return null;
    }
    #endregion

    #region BACK
    public void SeePreviousTutorial()
    {
        StartCoroutine(PrevAnimation());
    }

    IEnumerator PrevAnimation()
    {
        // IS BELOW 0
        if(alurTutorialIndex - 1 < 0)
        {
            yield return null;
        }
        else
        {
            // WHILE IS ALUR IN TRANSITION?
            int lastTutorialIndex = alurTutorialIndex;
            while (alurTutorial[alurTutorialIndex - 1].isTransition)
            {
                alurTutorialIndex--;
                if (alurTutorialIndex <= 0)
                {
                    break;
                }
            }

            DetailPrevAnimation(lastTutorialIndex, alurTutorialIndex - 1);
            yield return new WaitForSeconds(transisiAlurDuration);
        }
    }

    void DetailPrevAnimation(int closedIndex, int openedIndex)
    {
        List<GameObject> prevAlurObjects = new List<GameObject>();
        List<bool> prevAlurBools = new List<bool>();
     
        // Fade in alur Tutorial of opened index on the behind the scene
        foreach (GameObject isi in alurTutorial[openedIndex].isiTutorial)
        {
            prevAlurObjects.Add(isi);
            prevAlurBools.Add(false);
        }

        // Fade out alur Tutorial of Closed index 
        foreach (GameObject isi in alurTutorial[closedIndex].isiTutorial)
        {
            bool isMatched = false;
            for (int i = 0; i < prevAlurObjects.Count; i++)
            {
                if (GameObject.ReferenceEquals(prevAlurObjects[i], isi))
                {
                    prevAlurBools[i] = true;
                    isMatched = true;
                    break;
                }
            }

            if (!isMatched)
            {
                try
                {
                    var text = isi.GetComponent<TextMeshProUGUI>();
                    if(text != null)
                        CommonHandler.instance.FadeOutAnimation(isi, null, transisiAlurDuration / 4);
                    else
                        CommonHandler.instance.FadeOutAnimation(isi, null, transisiAlurDuration / 2);
                }
                catch
                {
                    CommonHandler.instance.FadeOutAnimation(isi, null, transisiAlurDuration / 2);
                }
            }
        }

        for(int i = 0; i<prevAlurBools.Count; i++)
        {
            if(!prevAlurBools[i])
                CommonHandler.instance.FadeInAnimation(prevAlurObjects[i], null, transisiAlurDuration/2);
        }
        SetIsiAlur(alurTutorialIndex-1);
        alurTutorialIndex--;
    }
    #endregion

    #region TUTORIAL END OF SESSION
    public void BackBeforeEndOfSession()
    {
        CommonHandler.instance.FadeInAnimation(tutorial, null, 0);
        StartCoroutine(EndOfSessionFadeOutAnimation(transisiAlurDuration * 0.5f));
    }

    public void NextAfterEndOfSession()
    {
        PlayerSaveData.instance.tutorialDone = true;
        PlayerSaveData.instance.SavePlayerSetupData();
        StartCoroutine(EndOfSessionFadeOutAnimation(transisiAlurDuration * 0.5f, true));
    }

    IEnumerator EndOfSessionFadeOutAnimation(float duration, bool isEnd = false)
    {
        var parent = isEnd == true ? tutorialPanel : null;
        CommonHandler.instance.FadeOutAnimation(tutorialEndOfSession, parent, duration);
        yield return new WaitForSeconds(duration);
        descriptionEndOfSession.transform.DOMoveY(hideEndOfSessionDescriptionPosY, 0, true);
        descriptionEndOfSession.SetActive(false);
        logoEndOfSession.SetActive(false);
        tutorialEndOfSession.SetActive(false);
    }

    IEnumerator TutorialEndOfSession()
    {
        CommonHandler.instance.FadeOutAnimation(tutorial, null, transisiAlurDuration * 0.5f);
        CommonHandler.instance.FadeInAnimation(tutorialEndOfSession, null, transisiAlurDuration * 0.5f);
        yield return new WaitForSeconds(transisiAlurDuration);
        CommonHandler.instance.FadeInAnimation(logoEndOfSession, null, transisiAlurDuration);
        yield return new WaitForSeconds(transisiAlurDuration);
        descriptionEndOfSession.SetActive(true);
        descriptionEndOfSession.transform.DOMoveY(defaultEndOfSessionDescriptionPosY, 0.5f, true);
        yield return null;
        InboxHandler.instance.ShowDebutSponsorshipMail();
    }
    #endregion
}
