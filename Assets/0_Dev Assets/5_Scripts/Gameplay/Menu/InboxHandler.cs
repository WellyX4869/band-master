using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using TMPro;

public enum MailType 
{
    Tutorial,
    DebutSponsorship,
    Regular,
    NewGame
}

public class InboxHandler : MonoBehaviour
{
    public static InboxHandler instance;

    [Header("Mail Settings")]
    [SerializeField] List<MailSetupUI> mailsSetupUI = new List<MailSetupUI>();
    [SerializeField] Sprite unreadedBackgroundMail;
    [SerializeField] Sprite readedBackgroundMail;

    [SerializeField] GameObject fanPage;
    [SerializeField] GameObject isiFanPage;

    [Header("Detail Mail")]
    [SerializeField] GameObject detailInbox;
    [SerializeField] GameObject mail;
    [SerializeField] GameObject tutorialContentMail;
    [SerializeField] GameObject debutSponsorshipContentMail;
    [SerializeField] GameObject regularContentMail;
    [SerializeField] GameObject newGameMail;
    [SerializeField] GameObject rewardObtained;
    [SerializeField] TextMeshProUGUI debutSponsorshipFundText;
    [SerializeField] TextMeshProUGUI successfullyObtainedText;
    GameObject openedMailContent;
    MailSetupUI openedMailSetupUI;
    [SerializeField] List<TextMeshProUGUI> replacedWithAppropriateLowerGroupName = new List<TextMeshProUGUI>();
    [SerializeField] List<TextMeshProUGUI> replacedWithAppropriateGroupName = new List<TextMeshProUGUI>();

    [Header("Animations")]
    [SerializeField] float delayMailAnimation;
    [SerializeField] float mailYDestination = 1000f;
    [SerializeField] float delayFanPageAnimation;
    [SerializeField] float fanPageShowX;
    [SerializeField] float fanPageHideX;

    float defaultMailYPosition;
    float defaultContentMailYPosition;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {
        mail.SetActive(false);
        fanPage.SetActive(false);
        detailInbox.SetActive(false);

        SetupMailSetupUI(true);
        SetupAppropriateGroupName();
        SetupHasDoneTutorial();

        defaultMailYPosition = mail.transform.GetChild(1).position.y;
        defaultContentMailYPosition = debutSponsorshipContentMail.transform.position.y;
    }

    void SetupAppropriateGroupName()
    {
        foreach (TextMeshProUGUI tmpro in replacedWithAppropriateGroupName)
        {
            tmpro.text = tmpro.text.Replace("[group name]", PlayerSaveData.instance.groupName);
        }
        
        foreach (TextMeshProUGUI tmpro in replacedWithAppropriateLowerGroupName)
        {
            string trimmed = (tmpro.text.Replace("[group name]", PlayerSaveData.instance.groupName)).ToLower();
            trimmed = trimmed.Replace(" ", "");
            tmpro.text = trimmed;
        }
    }

    void SetupHasDoneTutorial()
    {
        if (PlayerSaveData.instance.tutorialDone)
        {
            ShowDebutSponsorshipMail();
            if (PlayerGameData.instance.hasReceivedFunds)
            {
                mailsSetupUI[mailsSetupUI.Count - 2].isNew = false;
            }
            mailsSetupUI[mailsSetupUI.Count - 1].isNew = false;
            SetupMailSetupUI();
        }
    }

    #region MAIL ANIMATION
    public void OpenMail(MailSetupUI mailSetupUI)
    {
        openedMailContent = null;
        detailInbox.SetActive(true);
        mailSetupUI.isNew = false;
        openedMailSetupUI = mailSetupUI;
        SetupMailSetupUI();

        switch (mailSetupUI.mailType)
        {
            case MailType.Tutorial:
                openedMailContent = tutorialContentMail;
                break;
            case MailType.DebutSponsorship:
                openedMailContent = debutSponsorshipContentMail;
                break;
            case MailType.Regular:
                openedMailContent = regularContentMail;
                break;
            case MailType.NewGame:
                openedMailContent = newGameMail;
                break;
            default:
                openedMailContent = regularContentMail;
                break;
        }
        StartCoroutine(OpenMailAnimation());
    }

    IEnumerator OpenMailAnimation()
    {
        CommonHandler.instance.FadeInAnimation(mail.transform.GetChild(0).gameObject, mail);
        tutorialContentMail.SetActive(false);
        debutSponsorshipContentMail.SetActive(false);
        regularContentMail.SetActive(false);
        newGameMail.SetActive(false);
        openedMailContent.SetActive(true);
        openedMailContent.transform.parent.GetComponent<ScrollRect>().content = openedMailContent.GetComponent<RectTransform>();

        var openedMail = mail.transform.GetChild(1).transform;
        openedMail.DOMoveY(mailYDestination, delayMailAnimation, true);
        yield return new WaitForSeconds(delayMailAnimation);
    }

    public void CloseMail(bool fadeRightAway = false)
    {
        if (fadeRightAway)
            StartCoroutine(CloseMailFadeRightAwayAnimation());
        else
            StartCoroutine(CloseMailAnimation());
    }

    IEnumerator CloseMailFadeRightAwayAnimation()
    {
        var openedMail = mail.transform.GetChild(1).transform;
        CommonHandler.instance.FadeOutAnimation(rewardObtained);
        openedMail.DOMoveY(defaultMailYPosition, 0, true);
        openedMailContent.transform.DOMoveY(defaultContentMailYPosition, 0, true);
        CommonHandler.instance.FadeOutAnimation(mail, detailInbox);
        yield return new WaitForSeconds(CommonHandler.instance.fadeOutDuration);
        openedMailContent = null;
    }

    IEnumerator CloseMailAnimation()
    {
        var openedMail = mail.transform.GetChild(1).transform;
        openedMail.DOMoveY(defaultMailYPosition, delayMailAnimation / 2, true);
        yield return new WaitForSeconds(delayMailAnimation / 3);
        openedMailContent.transform.DOMoveY(defaultContentMailYPosition, 0, true);
        CommonHandler.instance.FadeOutAnimation(mail, detailInbox);
        yield return new WaitForSeconds(CommonHandler.instance.fadeOutDuration);
        openedMailContent = null;
    }
    #endregion

    #region FAN PAGE
    public void OpenFanPage()
    {
        detailInbox.SetActive(true);
        // Use Hot Tween to open from right to display

        fanPage.SetActive(true);
        fanPage.GetComponent<Image>().DOFade(0, 0);
        fanPage.GetComponent<Image>().DOFade(0.4f, delayFanPageAnimation);

        isiFanPage.transform.DOLocalMoveX(fanPageShowX, delayFanPageAnimation);
    }

    public void CloseFanPage()
    {
        // Use Hot Tween to open from display to hide
        isiFanPage.transform.DOLocalMoveX(fanPageHideX, delayFanPageAnimation);
        fanPage.GetComponent<Image>().DOFade(0, delayFanPageAnimation);

        Invoke(GetFunctionName(CloseFanPageAnimation), delayFanPageAnimation);
    }

    private void CloseFanPageAnimation()
    {
        fanPage.SetActive(false);
        detailInbox.SetActive(false);
    }
    #endregion

    #region DETAIL_MAIL
    public void MailDone()
    {
        if(openedMailSetupUI.mailType == MailType.Tutorial)
        {
            PlayerSaveData.instance.tutorialDone = true;
            PlayerSaveData.instance.SavePlayerSetupData();
        }
        CloseMail();
        ShowDebutSponsorshipMail();
    }

    public void OpenLinkTutorial()
    {
        CloseMail();
        TutorialHandler.instance.StartTutorial();
    }

    public void ShowDebutSponsorshipMail()
    {
        foreach(MailSetupUI mail in mailsSetupUI)
        {
            if(mail.mailType == MailType.DebutSponsorship)
            {
                mail.gameObject.SetActive(true);
                debutSponsorshipFundText.text = "Debut sponsorship funds : " + CommonHandler.instance.IntToString(PlayerGameData.instance.debutSponsorshipFund);
                successfullyObtainedText.text = "Money: + " + CommonHandler.instance.IntToString(PlayerGameData.instance.debutSponsorshipFund);
            }
        }
    }

    public void ShowNewGameMail()
    {
        foreach (MailSetupUI mail in mailsSetupUI)
        {
            if (mail.mailType == MailType.NewGame)
            {
                mail.isNew = PlayerGameData.instance.isNewGameMailStillNewAfterLoading;
                mail.gameObject.SetActive(true);
            }
        }
        SetupMailSetupUI();
    }

    public void OpenNewGame()
    {
        CloseMail();
        PlayerGameData.instance.NewGame();
    }

    public void MarkAsRead()
    {
        openedMailSetupUI.hasBeenRead = true;
        SetupMailSetupUI();
        CloseMail(true);
    }

    public void Receive()
    {
        if (PlayerGameData.instance.hasReceivedFunds) { return; }
        CommonHandler.instance.FadeInAnimation(rewardObtained);
        PlayerGameData.instance.hasReceivedFunds = true;
        PlayerGameData.instance.AddCurrency(PlayerGameData.instance.debutSponsorshipFund);
        PlayerGameData.instance.SavePlayerGameData();
    }

    public void RewardObtainedOK()
    {
        CloseMail(true);
    }

    void SetupMailSetupUI(bool firstTimeSetup = false)
    {
        if (firstTimeSetup)
        {
            foreach (MailSetupUI mail in mailsSetupUI)
            {
                mail.hasBeenRead = false;
                mail.isNew = true;
            }

            if (!PlayerSaveData.instance.tutorialDone)
            {
                foreach (MailSetupUI mail in mailsSetupUI)
                {
                    if (mail.mailType == MailType.Tutorial)
                    {
                        mail.gameObject.SetActive(true);
                    }
                    else
                    {
                        mail.gameObject.SetActive(false);
                    }
                }
            }
        }

        foreach (MailSetupUI mail in mailsSetupUI)
        {
            if (mail.hasBeenRead)
            {
                mail.GetComponent<Image>().sprite = readedBackgroundMail;
            }
            else
            {
                mail.GetComponent<Image>().sprite = unreadedBackgroundMail;
            }

            if (mail.isNew)
            {
                mail.isNewStatus.SetActive(true);
            }
            else
            {
                mail.isNewStatus.SetActive(false);
            }
        }
    }
    #endregion

    private static string GetFunctionName(Action method)
    {
        return method.Method.Name;
    }
}
