using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.Globalization;

public enum Day
{
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
}

public class CalendarHandler : MonoBehaviour
{
    public static CalendarHandler instance;

    [SerializeField] GameObject calendar;
    [SerializeField] GameObject scrollPickerParent;
    [SerializeField] Color pickedColor;

    [Header("Mini Calendar")]
    [SerializeField] TextMeshProUGUI dayOnMiniCalendar;
    [SerializeField] TextMeshProUGUI dateOnMiniCalendar;
    [SerializeField] TextMeshProUGUI timeOnMiniCalendar;
    DateTime gameDate;

    [Header("Day")]
    [SerializeField] GameObject[] days5x7Objects;
    [SerializeField] GameObject[] days6x7Objects;
    [SerializeField] Color specialDayColor;
    [SerializeField] Color lockedDayColor;
    [SerializeField] Color defaultDayColor;
    [SerializeField] GameObject days5x7;
    [SerializeField] GameObject days6x7;
    List<TextMeshProUGUI> days5x7Text = new List<TextMeshProUGUI>();
    List<TextMeshProUGUI> days6x7Text = new List<TextMeshProUGUI>();

    [Header("Month")]
    [SerializeField] TextMeshProUGUI monthText;
    [SerializeField] GameObject[] monthsObjects;
    [SerializeField] GameObject monthScrollPicker;
    List<TextMeshProUGUI> monthsText = new List<TextMeshProUGUI>();

    [Header("Year")]
    [SerializeField] TextMeshProUGUI yearText;
    [SerializeField] GameObject[] yearsObjects;
    [SerializeField] GameObject yearScrollPicker;
    List<TextMeshProUGUI> yearsText = new List<TextMeshProUGUI>();

    [Header("Initial Values")]
    int calendarMonth = 11;
    int calendarYear = 2021;
    [SerializeField] Vector2Int yearRange = new Vector2Int(2021, 2023);

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start()
    {
        // days
        foreach (GameObject dayObject in days5x7Objects) 
        {
            days5x7Text.Add(dayObject.GetComponentInChildren<TextMeshProUGUI>());
        }
        foreach(GameObject dayObject in days6x7Objects)
        {
            days6x7Text.Add(dayObject.GetComponentInChildren<TextMeshProUGUI>());
        }

        // months
        foreach(GameObject monthObject in monthsObjects)
        {
            monthsText.Add(monthObject.GetComponentInChildren<TextMeshProUGUI>());
        }
        // years
        foreach (GameObject yearObject in yearsObjects)
        {
            yearsText.Add(yearObject.GetComponentInChildren<TextMeshProUGUI>());
        }

        SetupDate();

        monthScrollPicker.SetActive(false);
        yearScrollPicker.SetActive(false);
        scrollPickerParent.SetActive(false);
        calendar.SetActive(false);
    }

    float timePlayed = 0f;
    int timeAdded = 0;
    int minuteCounter = 0;

    private void Update()
    {
        timePlayed += Time.deltaTime;
        timeAdded = Mathf.CeilToInt(timePlayed);
        if(timeAdded > minuteCounter)
        {
            gameDate = gameDate.AddMinutes(1);
            PlayerGameData.instance.ChangeGameDate(gameDate);
            minuteCounter++;
            string AMorPM = gameDate.ToString("tt") == "AM" ? "a.m." : "p.m.";
            timeOnMiniCalendar.text = gameDate.ToString("hh:mm") + " " + AMorPM;
        }
    }

    public void OpenCalendar()
    {
        // For now the calendar is locked
        //CommonHandler.instance.FadeInAnimation(calendar, null, 0.25f);
    }

    public void CloseCalendar()
    {
        CommonHandler.instance.FadeOutAnimation(calendar, null, 0.25f);
    }

    #region MONTH PICKER
    public void OpenMonthScrollPicker()
    {
        // Set the color of choosed Month to be red color
        monthsText[calendarMonth - 1].color = pickedColor;
        scrollPickerParent.SetActive(true);
        monthScrollPicker.SetActive(true);
    }

    public void ChangeMonth(Transform monthObject)
    {
        monthsText[calendarMonth - 1].color = Color.black;
        int month = monthObject.GetSiblingIndex() + 1;
        calendarMonth = month;
        monthsText[calendarMonth - 1].color = pickedColor;
        monthText.text = GetFullMonthName(calendarMonth);

        SetupDate(false);
    }

    // function to get the full month name
    string GetFullMonthName(int month)
    {
        return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
    }
    #endregion

    #region YEAR PICKER
    public void OpenYearScrollPicker()
    {
        // Set the color of choosed Month to be red color
        yearsText[calendarYear-yearRange.x].color = pickedColor;
        scrollPickerParent.SetActive(true);
        yearScrollPicker.SetActive(true);
    }

    public void ChangeYear(Transform yearObject)
    {
        yearsText[calendarYear - yearRange.x].color = Color.black;
        int year = yearObject.GetSiblingIndex();
        calendarYear = yearRange.x + year;
        yearsText[year].color = pickedColor;
        yearText.text = (calendarYear).ToString();

        SetupDate(false);
    }
    #endregion

    public void CloseScrollPicker()
    {
        monthScrollPicker.SetActive(false);
        yearScrollPicker.SetActive(false);
        scrollPickerParent.SetActive(false);
    }

    public void DebugCalendar()
    {
        // Get the first day of the month in that year
        var firstDayDate = new DateTime(calendarYear, calendarMonth, 1);
        string nameOfTheDay = firstDayDate.ToString("dddd");
        Day firstDayEnum = (Day)Enum.Parse(typeof(Day), nameOfTheDay);
        int firstDay = (int)firstDayEnum;
        int lastDay = DateTime.DaysInMonth(firstDayDate.Year, firstDayDate.Month);

        int lastMonth = firstDayDate.Month - 1 <= 0 ? 12 : firstDayDate.Month - 1;
        int lastYear = lastMonth == 12 ? firstDayDate.Year - 1 : firstDayDate.Year;

        Debug.Log("First Day: "+firstDayDate);
        Debug.Log("Last Day: " + lastDay);
        Debug.Log("First Day: " + firstDay);
    }

    void SetupDate(bool isDefault = true)
    {
        // Setup Mini Calendar
        if(isDefault)
            SetupMiniCalendar();

        // Get the first day of the month in that year
        calendarYear = isDefault ? PlayerGameData.instance.GetCurrentDate().Year : calendarYear;
        calendarMonth = isDefault ? PlayerGameData.instance.GetCurrentDate().Month : calendarMonth;
        var firstDayDate = new System.DateTime(calendarYear, calendarMonth, 1);
        string nameOfTheDay = firstDayDate.ToString("dddd");
        monthText.text = firstDayDate.ToString("MMMM");
        yearText.text = calendarYear.ToString();
        Day firstDayEnum = (Day)Enum.Parse(typeof(Day), nameOfTheDay);
        int firstDay = (int)firstDayEnum;
        firstDay = firstDay <= 0 ? 6 : firstDay - 1;
        int lastDay = DateTime.DaysInMonth(firstDayDate.Year, firstDayDate.Month);

        int lastMonth = firstDayDate.Month - 1 <= 0 ? 12 : firstDayDate.Month - 1;
        int lastYear = lastMonth == 12 ? firstDayDate.Year - 1 : firstDayDate.Year;

        int lastDayOnPreviousMonth = DateTime.DaysInMonth(lastYear, lastMonth);
        int date = 1;

        // Check if it needs 6x7 calendar grid or 5x7 calendar grid
        bool isGrid6x7 = false;
        if (lastDay == 30 && firstDay == 6)
        {
            isGrid6x7 = true;
        }
        
        if(lastDay == 31 && firstDay >= 5)
        {
            isGrid6x7 = true;
        }

        //string log = firstDayDate.ToString("dd/MM/yyyy");
        //log += "\n" + "Name of First Day: " + nameOfTheDay + " - "+firstDay.ToString();
        //Debug.Log(log);

        if (isGrid6x7)
        {
            days5x7.SetActive(false);
            days6x7.SetActive(true);
            bool nextMonth = false;
            for (int i = 0; i < days6x7Text.Count; i++)
            {
                if (i >= firstDay)
                {
                    days6x7Text[i].text = date.ToString();

                    if (nextMonth)
                        days6x7Text[i].color = lockedDayColor;
                    else
                        days6x7Text[i].color = defaultDayColor;

                    date++;
                    if (date > lastDay) { date = 1; nextMonth = true; }
                }
                else
                {
                    days6x7Text[i].text = (lastDayOnPreviousMonth - firstDay + i + 1).ToString();
                    days6x7Text[i].color = lockedDayColor;
                }
            }
        }
        else
        {
            days6x7.SetActive(false);
            days5x7.SetActive(true);
            bool nextMonth = false;
            for (int i = 0; i < days5x7Text.Count; i++)
            {
                if (i >= firstDay)
                {
                    days5x7Text[i].text = date.ToString();

                    if (nextMonth)
                        days5x7Text[i].color = lockedDayColor;
                    else
                        days5x7Text[i].color = defaultDayColor;

                    date++;
                    if (date > lastDay) { date = 1; nextMonth = true; }
                }
                else
                {
                    days5x7Text[i].text = (lastDayOnPreviousMonth - firstDay + i + 1).ToString();
                    days5x7Text[i].color = lockedDayColor;
                }
            }
        }
    }

    void SetupMiniCalendar()
    {
        gameDate = PlayerGameData.instance.GetCurrentDate();
        dayOnMiniCalendar.text = gameDate.ToString("dddd");
        dateOnMiniCalendar.text = gameDate.ToString("dd / MMM / yyyy");
        string AMorPM = gameDate.ToString("tt") == "AM" ? "a.m." : "p.m.";
        timeOnMiniCalendar.text = gameDate.ToString("hh:mm") + " " + AMorPM;
    }
    public void ChangeDate()
    {
        SetupDate();
    }
}
