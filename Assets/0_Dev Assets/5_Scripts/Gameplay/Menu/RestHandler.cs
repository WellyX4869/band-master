using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class RestHandler : MonoBehaviour
{
    [Header("Panels")]
    [SerializeField] GameObject restPanel;
    [SerializeField] GameObject shortBreakPanel;
    [SerializeField] GameObject longBreakPanel;
    [SerializeField] GameObject restingPanel;
    [SerializeField] TextMeshProUGUI restingText;

    [Header("Buttons")]
    [SerializeField] Sprite defaultDurationSprite;
    [SerializeField] Sprite activeDurationSprite;
    [SerializeField] Sprite defaultOKSprite;
    [SerializeField] Sprite activeOKSprite;

    [Header("Short Break")]
    [SerializeField] List<GameObject> shortBreakDurations;
    [SerializeField] Button okShortBreakButton;
    [SerializeField] List<int> shortDurations;
    [SerializeField] Image shortBreakOKImage;
    [SerializeField] TextMeshProUGUI everyCountOf1Day;
    [SerializeField] int staminaRefreshPer1ShortDuration = 0;

    [Header("Long Break")]
    [SerializeField] List<GameObject> longBreakDurations;
    [SerializeField] Button okLongBreakButton;
    [SerializeField] List<int> longDurations;
    [SerializeField] Image longBreakOKImage;
    [SerializeField] TextMeshProUGUI everyCountOf1Month;
    [SerializeField] int staminaRefreshPer1LongDuration = 0;

    int shortDuration = 0;
    int longDuration = 0;

    private void Start()
    {
        restPanel.SetActive(false);
        shortBreakPanel.SetActive(false);
        longBreakPanel.SetActive(false);
        restingPanel.SetActive(false);
        okShortBreakButton.interactable = false;
        okLongBreakButton.interactable = false;

        SetupStaminaRefresh();
    }

    void SetupStaminaRefresh()
    {
        if (staminaRefreshPer1ShortDuration == 0)
            everyCountOf1Day.text = "Every count of 1 day will refresh __ stamina.";
        else
            everyCountOf1Day.text = "Every count of 1 day will refresh "+ staminaRefreshPer1ShortDuration.ToString() +" stamina.";

        if (staminaRefreshPer1LongDuration == 0)
            everyCountOf1Month.text = "Every count of 1 month will refresh __ stamina.";
        else
            everyCountOf1Month.text = "Every count of 1 month will refresh " + staminaRefreshPer1LongDuration.ToString() + " stamina.";
    }

    public void OpenShortBreakPanel()
    {
        ResetShortBreakDuration();
        CommonHandler.instance.FadeInAnimation(shortBreakPanel, restPanel);
    }

    public void OpenLongBreakPanel()
    {
        ResetLongBreakDuration();
        CommonHandler.instance.FadeInAnimation(longBreakPanel, restPanel);
    }

    public void CloseRestBreakPanel()
    {
        if (shortBreakPanel.activeSelf)
        {
            CommonHandler.instance.FadeOutAnimation(shortBreakPanel, restPanel);
        }

        if (longBreakPanel.activeSelf)
        {
            CommonHandler.instance.FadeOutAnimation(longBreakPanel, restPanel);
        }
    }

    private void ResetShortBreakDuration()
    {
        shortDuration = 0;
        for (int i = 0; i < shortBreakDurations.Count; i++)
        {
            shortBreakDurations[i].GetComponent<Image>().sprite = activeDurationSprite;
        }
        okShortBreakButton.interactable = false;
    }

    private void ResetLongBreakDuration()
    {
        longDuration = 0;
        for (int i = 0; i < longBreakDurations.Count; i++)
        {
            longBreakDurations[i].GetComponent<Image>().sprite = activeDurationSprite;
        }
        okLongBreakButton.interactable = false;
    }

    public void ChooseRestShortBreakDuration(GameObject durationButton)
    {
        int choosedIndex = durationButton.transform.GetSiblingIndex();
        int days = shortDurations[choosedIndex];

        if (shortDuration == days)
        {
            shortDuration = 0;
            for (int i = 0; i < shortBreakDurations.Count; i++)
            {
                shortBreakDurations[i].GetComponent<Image>().sprite = defaultDurationSprite;
                shortBreakDurations[i].GetComponent<Image>().sprite = activeDurationSprite;
            }
            shortBreakOKImage.sprite = defaultOKSprite;
            okShortBreakButton.interactable = false;
        }
        else
        {
            shortDuration = days;
            durationButton.GetComponent<Image>().sprite = activeDurationSprite;

            for (int i = 0; i < shortBreakDurations.Count; i++)
            {
                if (i == choosedIndex) { continue; }
                shortBreakDurations[i].GetComponent<Image>().sprite = defaultDurationSprite;
            }

            shortBreakOKImage.sprite = activeOKSprite;
            okShortBreakButton.interactable = true;
        }
    }

    public void ChooseRestLongBreakDuration(GameObject durationButton)
    {
        int choosedIndex = durationButton.transform.GetSiblingIndex();
        int months = longDurations[choosedIndex];

        if (longDuration == months)
        {
            longDuration = 0;
            for (int i = 0; i < longBreakDurations.Count; i++)
            {
                longBreakDurations[i].GetComponent<Image>().sprite = defaultDurationSprite;
                longBreakDurations[i].GetComponent<Image>().sprite = activeDurationSprite;
            }
            longBreakOKImage.sprite = defaultOKSprite;
            okLongBreakButton.interactable = false;
        }
        else
        {
            longDuration = months;
            durationButton.GetComponent<Image>().sprite = activeDurationSprite;

            for (int i = 0; i < longBreakDurations.Count; i++)
            {
                if (i == choosedIndex) { continue; }
                longBreakDurations[i].GetComponent<Image>().sprite = defaultDurationSprite;
            }

            longBreakOKImage.sprite = activeOKSprite;
            okLongBreakButton.interactable = true;
        }
    }

    public void ConfirmShortBreakDuration()
    {
        // Implement rest short duration break
        Debug.Log(shortDuration + " days");
        ProcessShortBreakRest();
        Resting();
    }

    public void ConfirmLongBreakDuration()
    {
        // Implement rest long duration break
        Debug.Log(longDuration + " months");
        ProcessLongBreakRest();
        Resting();
    }

    private void Resting()
    {
        CommonHandler.instance.FadeInAnimation(restingPanel);
        ProcessResting(shortBreakPanel.activeSelf);
    }

    public void DoneResting()
    {
        CommonHandler.instance.FadeOutAnimation(restingPanel);
        if(shortBreakPanel.activeSelf)
            CommonHandler.instance.FadeOutAnimation(shortBreakPanel, restPanel);
        else
            CommonHandler.instance.FadeOutAnimation(longBreakPanel, restPanel);
        
        // Change tab to home
        GameMenuUI.instance.ChangeMenu(7);
    }

    void ProcessShortBreakRest()
    {
        // Change Resting Text
        string restText = "Your band members will take their ";
        restText += shortDuration.ToString() + " ";
        restText += (shortDuration > 1) ? "days off" : "day off";
        restingText.text = restText;
    }

    void ProcessLongBreakRest()
    { 
        // Change Resting Text
        string restText = "Your band members will take their ";
        restText += longDuration.ToString() + " ";
        restText += (longDuration > 1) ? "months off" : "month off";
        restingText.text = restText;
    }

    void ProcessResting(bool isShortBreak)
    {
        if (isShortBreak)
        {
            // Change Date on Calendar
            PlayerGameData.instance.AddCurrentDate(shortDuration);

            // Add Stamina
            PlayerGameData.instance.AddStamina(shortDuration * staminaRefreshPer1ShortDuration);
            PlayerGameData.instance.SavePlayerGameData();
        }
        else
        {
            // Change Calendar
            PlayerGameData.instance.AddCurrentDate(longDuration * 30);

            // Add Stamina
            PlayerGameData.instance.AddStamina(longDuration * staminaRefreshPer1LongDuration);
            PlayerGameData.instance.SavePlayerGameData();
        }
    }
}
