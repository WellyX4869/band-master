using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HomeHandler : MonoBehaviour
{
    public static HomeHandler instance;

    [Header("Band's Schedules")]
    [SerializeField] List<HomeScheduleSetupUI> homeScheduleSetupUI = new List<HomeScheduleSetupUI>();
    [SerializeField] GameObject noSchedules;
    [SerializeField] GameObject schedules;
    [SerializeField] Sprite goToStageActiveSprite;
    [SerializeField] Sprite goToStageInactiveSprite;
    [SerializeField] TextMeshProUGUI prevScheduleText;
    [SerializeField] TextMeshProUGUI nextScheduleText;
    int openedHomeScheduleIndex;

    [SerializeField] GameObject calendarTooltip;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {
        calendarTooltip.SetActive(false);
        SetupHomeSchedule();
    }

    public void OpenToolTipCalendar()
    {
        calendarTooltip.SetActive(true);
    }

    public void CloseToolTipCalendar()
    {
        calendarTooltip.SetActive(false);
    }

    public void SetupHomeSchedule(bool setup = true)
    {
        if (PlayerGameData.instance.onGoingBandSchedules.Count <= 0)
        {
            schedules.SetActive(false);
            noSchedules.SetActive(true);
        }
        else
        {
            if (setup)
            {
                openedHomeScheduleIndex = 0;
            }

            // Clear content first
            for(int i= 0; i<homeScheduleSetupUI.Count; i++)
            {
                homeScheduleSetupUI[i].homeScheduleDateText.text = "";
                homeScheduleSetupUI[i].homeScheduleTimeText.text = "";
                homeScheduleSetupUI[i].homeScheduleTitleText.text = "";
                homeScheduleSetupUI[i].homeScheduleLocationText.text = "";
                homeScheduleSetupUI[i].goToStageButtonImage.sprite = goToStageInactiveSprite;
                homeScheduleSetupUI[i].goToStageButtonImage.GetComponent<Button>().interactable = false;
                homeScheduleSetupUI[i].gameObject.SetActive(false);
            }

            // Set content first
            int indexCounter = 0;
            var bandSchedules = PlayerGameData.instance.onGoingBandSchedules;
            for(int i = openedHomeScheduleIndex; i<bandSchedules.Count; i++)
            {
                if (i >= openedHomeScheduleIndex + homeScheduleSetupUI.Count)
                {
                    break;
                }

                homeScheduleSetupUI[indexCounter].homeScheduleDateText.text = bandSchedules[i].timeSchedule.DayOfWeek.ToString() + ", " + bandSchedules[i].timeSchedule.Day + " " + bandSchedules[i].timeSchedule.ToString("MMM");
                homeScheduleSetupUI[indexCounter].homeScheduleTimeText.text = bandSchedules[i].timeSchedule.ToString("HH") + ":" + bandSchedules[i].timeSchedule.ToString("mm");
                homeScheduleSetupUI[indexCounter].homeScheduleTitleText.text = bandSchedules[i].scheduleTitle;
                homeScheduleSetupUI[indexCounter].homeScheduleLocationText.text = bandSchedules[i].promotion.tvName;

                if (bandSchedules[i].IsActive || i == 0)
                {
                    homeScheduleSetupUI[indexCounter].goToStageButtonImage.sprite = goToStageActiveSprite;
                    homeScheduleSetupUI[indexCounter].goToStageButtonImage.GetComponent<Button>().interactable = true;
                }
                else
                {
                    homeScheduleSetupUI[indexCounter].goToStageButtonImage.sprite = goToStageInactiveSprite;
                    homeScheduleSetupUI[indexCounter].goToStageButtonImage.GetComponent<Button>().interactable = false;
                }
                homeScheduleSetupUI[indexCounter].gameObject.SetActive(true);
                homeScheduleSetupUI[indexCounter].bandSchedule = bandSchedules[i];
                indexCounter++;
            }

            noSchedules.SetActive(false);
            schedules.SetActive(true);
        }
        SetupPrevNextScheduleText();
    }

    private void SetupPrevNextScheduleText()
    {
        // Next Schedule color text setup
        if (openedHomeScheduleIndex + homeScheduleSetupUI.Count < PlayerGameData.instance.onGoingBandSchedules.Count)
            nextScheduleText.color = CommonHandler.instance.activeColor;
        else nextScheduleText.color = CommonHandler.instance.inactiveColor;

        if (openedHomeScheduleIndex - homeScheduleSetupUI.Count >= 0)
            prevScheduleText.color = CommonHandler.instance.activeColor;
        else prevScheduleText.color = CommonHandler.instance.inactiveColor;
    }

    public void SeeNextSchedule()
    {
        if(PlayerGameData.instance.onGoingBandSchedules.Count <= 0) { return; }
        if(openedHomeScheduleIndex + homeScheduleSetupUI.Count >= PlayerGameData.instance.onGoingBandSchedules.Count)
        {
            return;
        }
        openedHomeScheduleIndex += homeScheduleSetupUI.Count;
        SetupHomeSchedule(false);
    }

    public void SeePreviousSchedule()
    {
        if (PlayerGameData.instance.onGoingBandSchedules.Count <= 0) { return; }
        if (openedHomeScheduleIndex - homeScheduleSetupUI.Count < 0)
        {
            return;
        }
        openedHomeScheduleIndex -= homeScheduleSetupUI.Count;
        SetupHomeSchedule(false);
    }

    public void GoToStage(HomeScheduleSetupUI homeScheduleSetupUI)
    {
        int siblingIndex = homeScheduleSetupUI.transform.GetSiblingIndex();
        StagesHandler.instance.LoadingPromoStage(homeScheduleSetupUI.bandSchedule);
        if (openedHomeScheduleIndex + siblingIndex < PlayerGameData.instance.onGoingBandSchedules.Count)
        {
            PlayerGameData.instance.onGoingBandSchedules.RemoveAt(openedHomeScheduleIndex + siblingIndex);
            SetupHomeSchedule();
        }
    }

    public void OpenMail()
    {
        if (TutorialHandler.instance.StillOpenYourMail)
        {
            TutorialHandler.instance.CloseOpenYourMailTutorial();
        }
        GameMenuUI.instance.ChangeMenu(5);
    }
}
