using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class FinanceHandler : MonoBehaviour
{
    public static FinanceHandler instance;
    [SerializeField] GameObject finance;

    [Header("SubMenu")]
    [SerializeField] ScrollRect financeScrollArea;
    [SerializeField] GameObject weekly;
    [SerializeField] GameObject monthly;
    bool isWeekly;

    [Header("Week Reports")]
    [SerializeField] TextMeshProUGUI weekDetailsText;
    [SerializeField] WeekGraph weekGraph;
    [SerializeField] TextMeshProUGUI previousWeekText;
    [SerializeField] TextMeshProUGUI nextWeekText;
    DateTime currentWeekDate;

    [Header("Month Reports")]
    [SerializeField] TextMeshProUGUI monthDetailsText;
    [SerializeField] MonthGraph monthGraph;
    [SerializeField] TextMeshProUGUI previousMonthText;
    [SerializeField] TextMeshProUGUI nextMonthText;
    DateTime currentMonthDate;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        isWeekly = true;
        financeScrollArea.content = weekly.GetComponent<RectTransform>();
        finance.SetActive(true);
        weekly.SetActive(true);
        monthly.SetActive(true);
        SetupGraph();
        weekly.SetActive(isWeekly);
        monthly.SetActive(!isWeekly);
        finance.SetActive(false);
    }

    public void OpenWeekly()
    {
        Debug.Log("Open Weekly");
        isWeekly = true;
        financeScrollArea.content = weekly.GetComponent<RectTransform>();
        monthly.SetActive(!isWeekly);
        weekly.SetActive(isWeekly);
    }

    public void OpenMonthly()
    {
        Debug.Log("Open Monthly");
        isWeekly = false;
        financeScrollArea.content = monthly.GetComponent<RectTransform>();
        weekly.SetActive(isWeekly);
        monthly.SetActive(!isWeekly);
    }

    public void SetupGraph()
    {
        currentWeekDate = PlayerGameData.instance.GetCurrentDate();
        DateTime firstDayOfWeek = PlayerGameData.instance.GetFirstDayOfThisWeek();
        DateTime lastDayOfWeek = PlayerGameData.instance.GetLastDayOfThisWeek();
        weekDetailsText.text = "[" + firstDayOfWeek.ToString("MMM - dd") + "] - [" + lastDayOfWeek.ToString("MMM - dd") + "] Details:";
        List<int> weekValueList = PlayerGameData.instance.GetThisWeekProfitsData(firstDayOfWeek);
        weekGraph.ShowWeekReport(weekValueList);
        SetupPreviousNextWeekReports(firstDayOfWeek);

        currentMonthDate = PlayerGameData.instance.GetCurrentDate();
        DateTime firstDayOfMonth = PlayerGameData.instance.GetFirstDayOfThisMonth();
        DateTime lastDayOfMonth = PlayerGameData.instance.GetLastDayOfThisMonth();
        monthDetailsText.text = "[" + firstDayOfMonth.ToString("MMM - dd") + "] - [" + lastDayOfMonth.ToString("MMM - dd") + "] Details:";
        List<int> monthValueList = PlayerGameData.instance.GetThisMonthProfitsData(firstDayOfMonth);
        monthGraph.ShowMonthReport(monthValueList);
        SetupPreviousNextMonthReports(firstDayOfMonth, lastDayOfMonth);
    }

    public void SetupWeekGraph()
    { 
        DateTime firstDayOfWeek = CommonHandler.GetFirstDateOfThisWeek(currentWeekDate);
        DateTime lastDayOfWeek = CommonHandler.GetLastDateOfThisWeek(currentWeekDate);
        weekDetailsText.text = "[" + firstDayOfWeek.ToString("MMM - dd") + "] - [" + lastDayOfWeek.ToString("MMM - dd") + "] Details:";
        List<int> weekValueList = PlayerGameData.instance.GetThisWeekProfitsData(firstDayOfWeek);
        weekGraph.UpdateReport(weekValueList);
        SetupPreviousNextWeekReports(firstDayOfWeek);
    }

    public void SetupMonthGraph()
    {
        DateTime firstDayOfMonth = CommonHandler.GetFirstDateOfThisMonth(currentMonthDate);
        DateTime lastDayOfMonth = CommonHandler.GetLastDateOfThisMonth(currentMonthDate);
        monthDetailsText.text = "[" + firstDayOfMonth.ToString("MMM - dd") + "] - [" + lastDayOfMonth.ToString("MMM - dd") + "] Details:";
        List<int> monthValueList = PlayerGameData.instance.GetThisMonthProfitsData(firstDayOfMonth);
        monthGraph.ShowMonthReport(monthValueList);
        SetupPreviousNextMonthReports(firstDayOfMonth, lastDayOfMonth);
    }

    private void DebugLogValueList(List<int> valueList)
    {
        string log = "\n";
        foreach (int value in valueList)
        {
            log += value + "; ";
        }
        log += "\n";
        Debug.Log(log);
    }

    public void UpdateGraph()
    {
        int weekCompareResult = CommonHandler.GetFirstDateOfThisWeek(currentWeekDate).Date.
                                CompareTo(CommonHandler.GetFirstDateOfThisWeek(PlayerGameData.instance.GetCurrentDate()).Date);
        if (weekCompareResult == 0) { weekGraph.UpdateWeekReportsValue(); }

        int monthCompareResult = CommonHandler.GetFirstDateOfThisMonth(currentMonthDate).Date.
                                 CompareTo(CommonHandler.GetFirstDateOfThisMonth(PlayerGameData.instance.GetCurrentDate()).Date);
        if (monthCompareResult == 0) { monthGraph.UpdateMonthReportsValue(); }
    }

    public void RefreshGraph()
    {
        currentWeekDate = PlayerGameData.instance.GetCurrentDate();
        SetupWeekGraph();
        DateTime lastDateOfLastCurrentMonthDate = CommonHandler.GetLastDateOfThisMonth(currentMonthDate);
        currentMonthDate = PlayerGameData.instance.GetCurrentDate();
        if (lastDateOfLastCurrentMonthDate.Date.CompareTo(PlayerGameData.instance.GetCurrentDate().Date) < 0)
        {
            SetupMonthGraph();
        }
    }

    #region WEEK REPORTS
    private void SetupPreviousNextWeekReports(DateTime firstDayOfWeek)
    {
        previousWeekText.color = CommonHandler.instance.inactiveColor;
        if (PlayerGameData.instance.IsPreviousDateHaveProfitData(firstDayOfWeek))
            previousWeekText.color = CommonHandler.instance.activeColor;

        nextWeekText.color = CommonHandler.instance.inactiveColor;
        DateTime lastDayOfWeek = CommonHandler.GetLastDateOfThisWeek(firstDayOfWeek);
        int comparisonDateResult = PlayerGameData.instance.GetCurrentDate().Date.CompareTo(lastDayOfWeek.Date);
        if (PlayerGameData.instance.IsNextDateHaveProfitData(lastDayOfWeek) || comparisonDateResult > 0)
            nextWeekText.color = CommonHandler.instance.activeColor;
    }

    public void SeePreviousWeekReports()
    {
        DateTime firstDayOfWeek = CommonHandler.GetFirstDateOfThisWeek(currentWeekDate);
        if (PlayerGameData.instance.IsPreviousDateHaveProfitData(firstDayOfWeek))
        {
            currentWeekDate = currentWeekDate.AddDays(-7);
            SetupWeekGraph();
            SetupPreviousNextWeekReports(firstDayOfWeek.AddDays(-7));
        }
    }

    public void SeeNextWeekReports()
    {
        DateTime lastDayOfWeek = CommonHandler.GetLastDateOfThisWeek(currentWeekDate);
        int comparisonDateResult = PlayerGameData.instance.GetCurrentDate().Date.CompareTo(lastDayOfWeek.Date);
        if (PlayerGameData.instance.IsNextDateHaveProfitData(lastDayOfWeek) || comparisonDateResult > 0)
        {
            currentWeekDate = currentWeekDate.AddDays(7);
            SetupWeekGraph();
            SetupPreviousNextWeekReports(lastDayOfWeek.AddDays(7));
        }
    }
    #endregion

    #region MONTH REPORTS
    private void SetupPreviousNextMonthReports(DateTime firstDayOfMonth, DateTime lastDayOfMonth)
    {
        previousMonthText.color = CommonHandler.instance.inactiveColor;
        if (PlayerGameData.instance.IsPreviousDateHaveProfitData(firstDayOfMonth))
            previousMonthText.color = CommonHandler.instance.activeColor;
        nextMonthText.color = CommonHandler.instance.inactiveColor;
        int comparisonDateResult = PlayerGameData.instance.GetCurrentDate().Date.CompareTo(lastDayOfMonth.Date);
        if (PlayerGameData.instance.IsNextDateHaveProfitData(lastDayOfMonth) || comparisonDateResult > 0)
            nextMonthText.color = CommonHandler.instance.activeColor;
    }

    public void SeePreviousMonthReports()
    {
        DateTime firstDayOfMonth = CommonHandler.GetFirstDateOfThisMonth(currentMonthDate);
        if (PlayerGameData.instance.IsPreviousDateHaveProfitData(firstDayOfMonth))
        {
            currentMonthDate = currentMonthDate.AddMonths(-1);
            SetupMonthGraph();
            SetupPreviousNextMonthReports(firstDayOfMonth.AddMonths(-1), firstDayOfMonth.AddDays(-1));
        }
    }

    public void SeeNextMonthReports()
    {
        DateTime lastDayOfMonth = CommonHandler.GetLastDateOfThisMonth(currentMonthDate);
        int comparisonDateResult = PlayerGameData.instance.GetCurrentDate().Date.CompareTo(lastDayOfMonth.Date);
        if (PlayerGameData.instance.IsNextDateHaveProfitData(lastDayOfMonth) || comparisonDateResult > 0)
        {
            currentMonthDate = currentMonthDate.AddMonths(1);
            SetupMonthGraph();
            SetupPreviousNextMonthReports(lastDayOfMonth.AddDays(1), CommonHandler.GetLastDateOfThisMonth(lastDayOfMonth.AddDays(1)));
        }
    }
    #endregion
}
