using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class HelpHandler : MonoBehaviour
{
    public static HelpHandler instance;

    [Header("Help Panel")]
    [SerializeField] GameObject helpPanel;
    [SerializeField] GameObject helpShade;
    [SerializeField] GameObject helpContent;
    [SerializeField] GameObject backButton;
    [SerializeField] GameObject nextButton;
    [SerializeField] Sprite nextButtonDefaultSprite;
    [SerializeField] Sprite nextButtonActiveSprite;
    int screenshotIndex = 0;

    [Header("Help Animations")]
    [SerializeField] float targetPosY;
    [SerializeField] float moveAnimationDuration;
    float defaultPosY;

    [Header("Help Panel Tabs")]
    [SerializeField] Transform intervalTab;
    [SerializeField] List<GameObject> activeTabs = new List<GameObject>();
    [SerializeField] List<GameObject> inactiveTabs = new List<GameObject>();
    [SerializeField] List<GameObject> screenshotTabs = new List<GameObject>();
    int activeTabIndex = 0;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        defaultPosY = helpContent.transform.position.y;
        ResetHelpTab();
        helpPanel.SetActive(false);
    }

    void ResetHelpTab()
    {
        #region  Settings Help Tab
        for (int i = 0; i < activeTabs.Count; i++)
        {
            if (i == activeTabIndex)
            {
                activeTabs[i].SetActive(true);
                screenshotTabs[i].SetActive(true);
            }
            else
            {
                activeTabs[i].SetActive(false);
                screenshotTabs[i].SetActive(false);
            }
        }

        for (int i = 0; i < inactiveTabs.Count; i++)
        {
            if (i == activeTabIndex)
            {
                inactiveTabs[i].SetActive(false);
                intervalTab.SetSiblingIndex(i);
            }
            else
            {
                inactiveTabs[i].SetActive(true);
            }
        }

        SettingScreenshotButton();
        #endregion
    }

    void SettingScreenshotButton(int previousScreenshotIndex = 0)
    {
        int screenshotsTabCount = screenshotTabs[activeTabIndex].transform.childCount;
        screenshotTabs[previousScreenshotIndex].SetActive(false);
        screenshotTabs[activeTabIndex].SetActive(true);
        for(int i = 0; i<screenshotsTabCount; i++)
        {
            if (i == 0)
                screenshotTabs[activeTabIndex].transform.GetChild(i).gameObject.SetActive(true);
            else
                screenshotTabs[activeTabIndex].transform.GetChild(i).gameObject.SetActive(false);
        }

        if(screenshotsTabCount <= 1) { nextButton.GetComponent<Button>().interactable = false; }
        else { nextButton.GetComponent<Button>().interactable = true; }

        screenshotIndex = 0;
        backButton.SetActive(false);
        if (screenshotsTabCount > 1)
        {
            nextButton.GetComponent<Image>().sprite = nextButtonActiveSprite;
        }
        else
        {
            nextButton.GetComponent<Image>().sprite = nextButtonDefaultSprite;
        }
    }

    public void OpenHelpPanel()
    {
        helpContent.SetActive(true);
        CommonHandler.instance.FadeInAnimation(helpShade, helpPanel);
        helpContent.transform.DOMoveY(targetPosY, moveAnimationDuration, true);
    }

    public void CloseHelpPanel()
    {
        StartCoroutine(CloseHelpPanelAnimation());
    }

    IEnumerator CloseHelpPanelAnimation()
    {
        helpContent.transform.DOMoveY(defaultPosY, moveAnimationDuration/1.2f, true);
        yield return new WaitForSeconds(moveAnimationDuration / 2f);
        CommonHandler.instance.FadeOutAnimation(helpShade, helpPanel);
        yield return new WaitForSeconds(CommonHandler.instance.fadeOutDuration);
        helpContent.SetActive(false);
    }

    public void ChangeTab(Transform tab) 
    {
        // Set Change Tab
        int siblingIndex = tab.GetSiblingIndex();
        int indexChoosed = siblingIndex > 0 ? siblingIndex - 1 : 0;

        if (indexChoosed != activeTabIndex)
        {
            if(intervalTab.GetSiblingIndex() > siblingIndex && siblingIndex > 0)
            {
                indexChoosed += 1;
            }

            intervalTab.SetSiblingIndex(siblingIndex);
            activeTabs[activeTabIndex].SetActive(false);
            inactiveTabs[indexChoosed].SetActive(false);
            screenshotTabs[activeTabIndex].SetActive(false);

            activeTabs[indexChoosed].SetActive(true);
            inactiveTabs[activeTabIndex].SetActive(true);
            screenshotTabs[indexChoosed].SetActive(true);

            activeTabIndex = indexChoosed;

            // Set Screenshots on active tab index
            SettingScreenshotButton();
        }
    }

    public void ChangeTab(int choosedTabIndex)
    {

    }

    public void SeeNextScreenshot()
    {
        int screenshotTabsCount = screenshotTabs[activeTabIndex].transform.childCount;
        if (screenshotIndex < screenshotTabsCount - 1)
        {
            screenshotIndex++;
            screenshotTabs[activeTabIndex].transform.GetChild(screenshotIndex-1).gameObject.SetActive(false);
            screenshotTabs[activeTabIndex].transform.GetChild(screenshotIndex).gameObject.SetActive(true);
            backButton.SetActive(true);

            if(screenshotIndex == screenshotTabsCount - 1) { nextButton.SetActive(false); }
        }
    }

    public void SeePreviousScreenshot()
    {
        int screenshotTabsCount = screenshotTabs[activeTabIndex].transform.childCount;
        if (screenshotIndex > 0)
        {
            screenshotIndex--;
            screenshotTabs[activeTabIndex].transform.GetChild(screenshotIndex + 1).gameObject.SetActive(false);
            screenshotTabs[activeTabIndex].transform.GetChild(screenshotIndex).gameObject.SetActive(true);

            if (screenshotIndex <= 0) { backButton.SetActive(false); }
            nextButton.SetActive(true);
        }
    }
}
