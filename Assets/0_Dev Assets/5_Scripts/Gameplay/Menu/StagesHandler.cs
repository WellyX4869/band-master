using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class StagesHandler : MonoBehaviour
{
    public static StagesHandler instance;

    [Header("Sub Menu")]
    [SerializeField] GameObject promotionStage;
    [SerializeField] GameObject worldwideTour;
    [SerializeField] GameObject stagesParent;
    [SerializeField] ScrollRect stagesScrollArea;
    bool isPromotion = true;
    bool goToStage = false;

    [Header("Promotion Stage")]
    [SerializeField] GameObject detailPromotion;
    [SerializeField] GameObject confirmationPromoteSong;
    [SerializeField] List<Promotion> promotions = new List<Promotion>();
    [SerializeField] List<PromotionSetupUI> promotionsSetupUI = new List<PromotionSetupUI>();
    [SerializeField] Sprite inactiveBookNowPromotionSprite;
    [SerializeField] Sprite activeBookNowPromotionSprite;

    [Header("Promotion Available Dates")]
    [SerializeField] GameObject choosePromotionDate;
    [SerializeField] List<ScheduleSetupUI> schedulesSetupUI = new List<ScheduleSetupUI>();
    [SerializeField] TextMeshProUGUI choosePromotionDateTV;
    [SerializeField] TextMeshProUGUI choosePromotionDateMonth;
    [SerializeField] TextMeshProUGUI choosePromotionDateYear;
    [SerializeField] GridLayoutGroup availableSchedulesGrid;
    [SerializeField] Color lockedScheduleColor;
    [SerializeField] Sprite checkBoxLockedSprite;
    [SerializeField] Color unlockedScheduleColor;
    [SerializeField] Sprite checkBoxUnlockedSprite;
    [SerializeField] Sprite checkBoxPickedSprite;
    [SerializeField] Image bookButtonImage;
    [SerializeField] Button bookButton;
    [SerializeField] Sprite lockedBookSprite;
    [SerializeField] Sprite unlockedBookSprite;
    [SerializeField] GameObject totalPromotionCost;
    [SerializeField] TextMeshProUGUI totalStaminaCostText;
    [SerializeField] TextMeshProUGUI totalMoneyCostText;
    [SerializeField] TextMeshProUGUI totalCurrentStaminaText;
    [SerializeField] TextMeshProUGUI totalCurrentMoneyText;
    Promotion choosedPromotion;
    DateTime availablePromotionDate;
    List<BandSchedule> choosedBandSchedules = new List<BandSchedule>();
    List<BandSchedule> bandSchedules = new List<BandSchedule>();

    [Header("Booking Confirmation & Complete")]
    [SerializeField] GameObject bookingConfirmation;
    [SerializeField] List<GameObject> bookingConfirmationChosenDates;
    [SerializeField] GameObject bookingComplete;

    [Header("Load Promotion Stage")]
    [SerializeField] GameObject loadPromotionStage;
    [SerializeField] TextMeshProUGUI loadingPromotionStageText;
    [SerializeField] Slider loadingPromotionStageSlider;
    BandSchedule loadedSchedule;

    [Header("Worldwide Tour")]
    [SerializeField] List<WorldTour> worldTours = new List<WorldTour>();
    [SerializeField] List<WorldTourSetupUI> worldToursSetupUI = new List<WorldTourSetupUI>();
    [SerializeField] Sprite inactiveBookNowWorldTourSprite;
    [SerializeField] Sprite activeBookNowWorldTourSprite;
    [SerializeField] GameObject detailWorldwideTour;
    [SerializeField] GameObject proceedATour;
    [SerializeField] TextMeshProUGUI costXXXMoneyText;
    [SerializeField] TextMeshProUGUI currentGroupBudgetText;
    [SerializeField] TextMeshProUGUI groupStaminaCostText;
    [SerializeField] GameObject loadingOtwStage;
    [SerializeField] Image loadingOtwStageBackground;
    [SerializeField] TextMeshProUGUI loadingOtwStageText;
    [SerializeField] Slider loadingOtwStageSlider;
    [SerializeField] float loadingStep = 6;
    [SerializeField] float timeDelayLoading = 0.5f;
    WorldTour currentWorldTour;

    [Header("Stages")]
    [SerializeField] GameObject detailStage;
    [SerializeField] GameObject pleaseWait;
    [SerializeField] GameObject theStage;
    [SerializeField] Image theStageLivePics;
    [SerializeField] GameObject stageReport;
    [SerializeField] GameObject proceedToStageHighlight;
    [SerializeField] GameObject stageHighlight;
    [SerializeField] Image[] highlightImages;
    [SerializeField] GameObject proceedToNextStage;
    [SerializeField] GameObject goToNextStage;
    [SerializeField] TextMeshProUGUI stageCounterText;
    [SerializeField] GameObject stageEnd;

    [Header("Stages Animation Settings")]
    [SerializeField] float timeDelayToStage = 1f;
    [SerializeField] float timeDelayToStageReport = 1f;
    [SerializeField] float timeDelayInStageReport = 1f;
    [SerializeField] float stageHighlightDelay = 0.5f;
    [SerializeField] int maxStages = 2;

    [Header("Stage Detail")]
    [SerializeField] TextMeshProUGUI stageTitleText;
    [SerializeField] TextMeshProUGUI stageAudienceText;
    [SerializeField] TextMeshProUGUI stageGroupStaminaText;
    int stageAudience;

    [Header("Stage Report")]
    [SerializeField] GameObject[] stageReportResults;
    [SerializeField] GameObject[] stageReportBalance;
    [SerializeField] Button restButton;
    [SerializeField] Button nextButton;
    [SerializeField] Sprite inactivatedNextButton;
    [SerializeField] Sprite activatedNextButton;
    [SerializeField] TextMeshProUGUI stageReportTotalAudience;
    [SerializeField] TextMeshProUGUI stageReportTicketSold;
    [SerializeField] TextMeshProUGUI stageReportPopularity;
    [SerializeField] TextMeshProUGUI stageReportAddedMoney;
    [SerializeField] TextMeshProUGUI stageReportCurrentMoney;
    [SerializeField] TextMeshProUGUI stageReportCurrentGroupStamina;

    [Header("Stage End")]
    [SerializeField] TextMeshProUGUI stageEndTitle;
    [SerializeField] string promotionStageEndTitle;
    [SerializeField] string worldTourStageEndTitle;

    // States
    int stages = 0;
    bool isHighlightShowed = false;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        isPromotion = true;
        promotionStage.SetActive(isPromotion);
        worldwideTour.SetActive(!isPromotion);

        // Promotion Stage
        confirmationPromoteSong.SetActive(false);
        choosePromotionDate.SetActive(false);
        detailPromotion.SetActive(false);
        SetupPromotion();

        // Worldwide Tour
        proceedATour.SetActive(false);
        loadingOtwStage.SetActive(false);
        detailWorldwideTour.SetActive(false);
        SetupWorldTour();

        // Stages
        pleaseWait.SetActive(false);
        theStage.SetActive(false);
        stageReport.SetActive(false);
        stageHighlight.SetActive(false);
        proceedToNextStage.SetActive(false);
        goToNextStage.SetActive(false);
        stageEnd.SetActive(false);
        detailStage.SetActive(false);

        stagesParent.SetActive(false);
    }

    public void SetupPromotion()
    {
        for (int i = 0; i < promotionsSetupUI.Count; i++)
        {
            promotionsSetupUI[i].tvPromotionText.text = promotions[i].tvName;
            promotionsSetupUI[i].tvPopularityText.text = CommonHandler.instance.IntToString(promotions[i].tvPopularity, true);
            promotionsSetupUI[i].bookingCostPerWeek.text = CommonHandler.instance.IntToString((promotions[i].dayPromotions.Count * promotions[i].bookingCostPerDay), true) + " per week";
            if (PlayerGameData.instance.totalSongsReleased >= 1)
            {
                promotionsSetupUI[i].bookNowButton.GetComponent<Image>().sprite = activeBookNowPromotionSprite;
                promotionsSetupUI[i].bookNowButton.interactable = true;
            }
            else
            {
                promotionsSetupUI[i].bookNowButton.GetComponent<Image>().sprite = inactiveBookNowPromotionSprite;
                promotionsSetupUI[i].bookNowButton.interactable = false;
            }
            promotionsSetupUI[i].promotion = promotions[i];
        }
    }

    public void SetupWorldTour()
    {
        for(int i = 0; i < worldToursSetupUI.Count; i++)
        {
            worldToursSetupUI[i].titleText.text = worldTours[i].location + " : " + worldTours[i].stadiumName;
            worldToursSetupUI[i].capacityText.text = CommonHandler.instance.IntToString(worldTours[i].capacity, true);
            worldToursSetupUI[i].bookingCostPerDayText.text = CommonHandler.instance.IntToString(worldTours[i].bookingCostPerDay, true) + " per day";
            worldToursSetupUI[i].coloredCard.sprite = worldTours[i].coloredSprite;
            if (PlayerGameData.instance.totalSongsReleased >= 5)
            {
                worldToursSetupUI[i].bookNowButton.GetComponent<Image>().sprite = activeBookNowWorldTourSprite;
                worldToursSetupUI[i].bookNowButton.interactable = true;
            }
            else
            {
                worldToursSetupUI[i].bookNowButton.GetComponent<Image>().sprite = inactiveBookNowWorldTourSprite;
                worldToursSetupUI[i].bookNowButton.interactable = false;
            }
            worldToursSetupUI[i].worldTour = worldTours[i];
        }
    }

    public void OpenPromotionStage()
    {
        isPromotion = true;
        stagesScrollArea.content = promotionStage.GetComponent<RectTransform>();
        worldwideTour.SetActive(!isPromotion);
        promotionStage.SetActive(isPromotion);
    }

    public void OpenWorldWideTour()
    {
        isPromotion = false;
        stagesScrollArea.content = worldwideTour.GetComponent<RectTransform>();
        promotionStage.SetActive(isPromotion);
        worldwideTour.SetActive(!isPromotion);
    }

    #region PROMOTION STAGE
    public void OpenAvailableDatesBasedOnPromotion(PromotionSetupUI promotionSetupUI)
    {
        if(PlayerGameData.instance.totalSongsReleased <= 0)
        {
            return;
        }

        Promotion promotion = promotionSetupUI.promotion;
        if(!PlayerGameData.instance.IsStaminaOverCost(promotion.staminaCostPerDay) || !PlayerGameData.instance.IsCurrencyOverCost(promotion.moneyCostPerDay))
        {
            return;
        }

        // Set the content inside
        choosedPromotion = promotion;
        availablePromotionDate = PlayerGameData.instance.GetCurrentDate();
        SetAvailableDates();
        stagesParent.SetActive(true);
        bookButtonImage.sprite = lockedBookSprite;
        bookButton.interactable = false;
        totalPromotionCost.SetActive(false);
        CommonHandler.instance.FadeInAnimation(choosePromotionDate, detailPromotion, 0.4f);
    }

    public void CloseAvailableDates()
    {
        choosedPromotion = null;
        choosePromotionDate.SetActive(false);
        detailPromotion.SetActive(false);
        stagesParent.SetActive(false);
    }

    void SetAvailableDates()
    {
        bandSchedules.Clear();
        choosePromotionDateTV.text = choosedPromotion.tvName;
        availablePromotionDate = new System.DateTime(availablePromotionDate.Year, availablePromotionDate.Month, 1, 0, 0, 0);
        choosePromotionDateMonth.text = availablePromotionDate.ToString("MMMM");
        choosePromotionDateYear.text = availablePromotionDate.Year.ToString();

        // Get all band schedules for that month
        int lastDay = DateTime.DaysInMonth(availablePromotionDate.Year, availablePromotionDate.Month);
        for (int day = 1; day <= lastDay; day++)
        {
            DateTime checkingDate = new System.DateTime(availablePromotionDate.Year, availablePromotionDate.Month, day, 0, 0, 0);
            string dayName = checkingDate.DayOfWeek.ToString();
            bool isSameDayName = false;
            int hour = -1;
            for (int j = 0; j < choosedPromotion.dayPromotions.Count; j++)
            {
                if (dayName == choosedPromotion.dayPromotions[j].ToString())
                {
                    isSameDayName = true;
                    hour = choosedPromotion.timePromotions[j];
                    break;
                }
            }

            // Add band schedules
            if (isSameDayName)
            {
                bandSchedules.Add(new BandSchedule(checkingDate, hour, "Song Promotion - Band Performance", choosedPromotion));
            }
        }

        // Set non-active unneccessary schedule setup ui
        for (int i = 0; i < schedulesSetupUI.Count; i++)
        {
            if (i >= bandSchedules.Count)
                schedulesSetupUI[i].gameObject.SetActive(false);
            else schedulesSetupUI[i].gameObject.SetActive(true);
        }
        if (bandSchedules.Count > 8)
        {
            availableSchedulesGrid.spacing = new Vector2(availableSchedulesGrid.spacing.x, -25);
        }
        else
        {
            availableSchedulesGrid.spacing = new Vector2(availableSchedulesGrid.spacing.x, -1);
        }

        // Setup Schedule Setup UI
        for (int i = 0; i < bandSchedules.Count; i++)
        {
            BandSchedule schedule = bandSchedules[i];
            schedulesSetupUI[i].dateText.text = schedule.timeSchedule.DayOfWeek.ToString() + ", " + schedule.timeSchedule.Day;
            schedulesSetupUI[i].timeText.text = "@" + schedule.timeSchedule.ToString("HH") + ":" + schedule.timeSchedule.ToString("mm");

            int compareDateResult = DateTime.Compare(schedule.timeSchedule, PlayerGameData.instance.GetCurrentDate());
            if (compareDateResult <= 0 || PlayerGameData.instance.CompareDateToOngoingSchedule(schedule.timeSchedule)) // Current Date is over time schedule so lock this schedule setup ui
            {
                schedulesSetupUI[i].checkBoxImage.sprite = checkBoxLockedSprite;
                schedulesSetupUI[i].dateText.color = lockedScheduleColor;
                schedulesSetupUI[i].timeText.color = lockedScheduleColor;
                schedulesSetupUI[i].checkBoxButton.interactable = false;
            }
            else
            {
                schedulesSetupUI[i].checkBoxImage.sprite = checkBoxUnlockedSprite;
                schedulesSetupUI[i].dateText.color = unlockedScheduleColor;
                schedulesSetupUI[i].timeText.color = unlockedScheduleColor;
                schedulesSetupUI[i].checkBoxButton.interactable = true;
            }
            schedulesSetupUI[i].bandSchedule = bandSchedules[i];

            for(int j = 0; j<choosedBandSchedules.Count; j++)
            {
                if(DateTime.Compare(schedulesSetupUI[i].bandSchedule.timeSchedule, choosedBandSchedules[j].timeSchedule) == 0 && schedulesSetupUI[i].bandSchedule.promotion.tvName == choosedBandSchedules[j].promotion.tvName)
                {
                    schedulesSetupUI[i].checkBoxImage.sprite = checkBoxPickedSprite;
                }
            }
        }
    }

    public void CheckThisSchedule(ScheduleSetupUI scheduleSetupUI)
    {
        int siblingIndex = scheduleSetupUI.transform.GetSiblingIndex();
        if (scheduleSetupUI.IsPicked)
        {
            int choosedIndex = -1;
            for (int i = 0; i < choosedBandSchedules.Count; i++)
            {
                if (DateTime.Compare(choosedBandSchedules[i].timeSchedule, scheduleSetupUI.bandSchedule.timeSchedule) == 0)
                {
                    choosedIndex = i;

                    break;
                }
            }

            scheduleSetupUI.checkBoxImage.sprite = checkBoxUnlockedSprite;
            if (choosedIndex != -1) { choosedBandSchedules.RemoveAt(choosedIndex); }
            scheduleSetupUI.IsPicked = false;
        }
        else
        {
            if (choosedBandSchedules.Count >= 3)
            {
                return;
            }

            scheduleSetupUI.checkBoxImage.sprite = checkBoxPickedSprite;
            scheduleSetupUI.IsPicked = true;
            choosedBandSchedules.Add(scheduleSetupUI.bandSchedule);
        }

        int choosedSchedulesCount = choosedBandSchedules.Count;
        if (choosedSchedulesCount > 0)
        {
            totalStaminaCostText.text = "-" + CommonHandler.instance.IntToString(choosedSchedulesCount * choosedPromotion.staminaCostPerDay, true);
            totalMoneyCostText.text = "-" + CommonHandler.instance.IntToString(choosedSchedulesCount * choosedPromotion.moneyCostPerDay);
            totalCurrentStaminaText.text = CommonHandler.instance.IntToString(PlayerGameData.instance.GetStamina(), true);
            totalCurrentMoneyText.text = CommonHandler.instance.IntToString(PlayerGameData.instance.GetCurrency());
         
            if (!totalPromotionCost.activeSelf)
                totalPromotionCost.SetActive(true);

            if (choosedBandSchedules.Count >= 1)
            {
                bookButtonImage.sprite = unlockedBookSprite;
                bookButton.interactable = true;
            }
            else
            {
                bookButtonImage.sprite = lockedBookSprite;
                bookButton.interactable = false;
            }
        }
        else
        {
            bookButtonImage.sprite = lockedBookSprite;
            bookButton.interactable = false;
            totalPromotionCost.SetActive(false);
        }
    }

    public void SeePreviousMonthAvailableDates()
    {
        int availMonth = availablePromotionDate.AddMonths(-1).Month;
        int gameMonth = PlayerGameData.instance.GetCurrentDate().AddMonths(-1).Month;
        int availYear = availablePromotionDate.AddMonths(-1).Year;
        int gameYear = PlayerGameData.instance.GetCurrentDate().AddMonths(-1).Year;
        if (availYear >= gameYear && availMonth > gameMonth) 
        {
            availablePromotionDate = availablePromotionDate.AddMonths(-1);
            SetAvailableDates();
        }
    }

    public void SeeNextMonthAvailableDates()
    {
        availablePromotionDate = availablePromotionDate.AddMonths(1);
        SetAvailableDates();
    }

    public void OpenBookingConfirmation()
    {
        // Sort based on DateTime
        choosedBandSchedules.Sort((x, y) => DateTime.Compare(x.timeSchedule, y.timeSchedule));
        // Set the content inside
        for(int i = 0; i<bookingConfirmationChosenDates.Count; i++)
        {
            TextMeshProUGUI date = bookingConfirmationChosenDates[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI time = bookingConfirmationChosenDates[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            date.text = ""; time.text = "";

            if(i < choosedBandSchedules.Count)
            {
                var schedule = choosedBandSchedules[i];
                date.text = "• " + schedule.timeSchedule.ToString("MMMMM") + ", " + schedule.timeSchedule.DayOfWeek.ToString() + "(" + schedule.timeSchedule.Day.ToString() + ")";
                time.text = "@" + schedule.timeSchedule.ToString("HH") + ":" + schedule.timeSchedule.ToString("mm");
            }
        }
        CommonHandler.instance.FadeInAnimation(bookingConfirmation, bookingConfirmation.transform.parent.gameObject, 0.4f);
    }

    public void CancelBookingConfirmation()
    {
        CommonHandler.instance.FadeOutAnimation(bookingConfirmation, bookingConfirmation.transform.parent.gameObject, 0.3f);
    }

    public void ConfirmBooking()
    {
        // Setup home for schedule
        PlayerGameData.instance.UseCurrencyandStamina(choosedPromotion.moneyCostPerDay * choosedBandSchedules.Count, choosedPromotion.staminaCostPerDay * choosedBandSchedules.Count);
        PlayerGameData.instance.AddNewPromotionSchedule(choosedBandSchedules);
        PlayerGameData.instance.SavePlayerGameData();
        HomeHandler.instance.SetupHomeSchedule();

        CommonHandler.instance.FadeOutAnimation(bookingConfirmation, null, 0.5f);
        CommonHandler.instance.FadeInAnimation(bookingComplete, null, 0.5f);
    }

    public void BookingCompleteDone()
    {
        choosedBandSchedules.Clear();
        choosedPromotion = null;
        
        bookingComplete.SetActive(false);
        bookingComplete.transform.parent.gameObject.SetActive(false);
        choosePromotionDate.SetActive(false);
        detailPromotion.SetActive(false);
        stagesParent.SetActive(false);

        GameMenuUI.instance.ChangeMenu(7);
    }

    public void LoadingPromoStage(BandSchedule bandSchedule)
    {
        goToStage = true;
        loadedSchedule = bandSchedule;
        Debug.Log("Loading this: "+loadedSchedule.timeSchedule);
        StartCoroutine(LoadingPromotionStageAnimation());
    }

    IEnumerator LoadingPromotionStageAnimation()
    {
        stagesParent.SetActive(true);
        float timeDelay = 1f;
        float loadingCounter = 1f;
        loadingPromotionStageText.text = "Loading";
        loadingPromotionStageSlider.value = (1f - loadingCounter);
        CommonHandler.instance.FadeInAnimation(loadPromotionStage, detailPromotion, 0.5f);
        yield return new WaitForSeconds(timeDelay);
        loadingCounter -= (1f / loadingStep);
        loadingPromotionStageText.text = "Loading.";
        loadingPromotionStageSlider.value = (1f - loadingCounter);
        yield return new WaitForSeconds(timeDelay);
        loadingCounter -= (1f / loadingStep);
        loadingPromotionStageText.text = "Loading..";
        loadingPromotionStageSlider.value = (1f - loadingCounter);
        yield return new WaitForSeconds(timeDelay);
        loadingCounter -= (1f / loadingStep);
        loadingPromotionStageText.text = "Loading...";
        loadingPromotionStageSlider.value = (1f - loadingCounter);

        yield return new WaitForSeconds(timeDelay);
        loadingCounter -= (1f / loadingStep);
        loadingPromotionStageText.text = "Loading.";
        loadingPromotionStageSlider.value = (1f - loadingCounter);
        yield return new WaitForSeconds(timeDelay);
        loadingCounter -= (1f / loadingStep);
        loadingPromotionStageText.text = "Loading..";
        loadingPromotionStageSlider.value = (1f - loadingCounter);
        yield return new WaitForSeconds(timeDelay);
        loadingCounter -= (1f / loadingStep);
        loadingPromotionStageText.text = "Loading...";
        loadingPromotionStageSlider.value = (1f - loadingCounter);

        yield return new WaitForSeconds(timeDelay);
        Stages();
    }

    /// <summary>
    /// If the button promote in Promotion Stage pressed
    /// </summary>
    public void OpenConfirmationPromoteSong()
    {
        stagesParent.SetActive(true);
        detailPromotion.SetActive(true);
        confirmationPromoteSong.SetActive(true);
    }

    /// <summary>
    /// Choice button on Confirmation Promote Song
    /// </summary>
    public void ChooseNoToPromoteSong()
    {
        confirmationPromoteSong.SetActive(false);
        detailPromotion.SetActive(false);
        stagesParent.SetActive(false);
    }

    public void ChooseYesToPromoteSong()
    {
        confirmationPromoteSong.SetActive(false);
        choosePromotionDate.SetActive(true);
    }
    #endregion

    #region WORLDWIDE TOUR
    public void OpenProceedATour(WorldTourSetupUI worldTourSetupUI)
    {
        if(PlayerGameData.instance.totalSongsReleased <= 4) { return; }
        if (!PlayerGameData.instance.IsTodayOkayToPromoteOrTour()) { return; }

        // Set the content inside
        WorldTour worldTour = worldTourSetupUI.worldTour;
        costXXXMoneyText.text = "It cost " + CommonHandler.instance.IntToString(worldTour.totalCurrencyCost) + " money, proceed a tour to this location?";
        currentGroupBudgetText.text = CommonHandler.instance.IntToString(PlayerGameData.instance.GetCurrency());
        groupStaminaCostText.text = "- " + worldTour.staminaCost;
        currentWorldTour = worldTour;

        stagesParent.SetActive(true);
        CommonHandler.instance.FadeInAnimation(proceedATour, detailWorldwideTour, 0.5f);
    }

    public void ChooseNoToProceedATour()
    {
        currentWorldTour = null;
        proceedATour.SetActive(false);
        detailWorldwideTour.SetActive(false);
        stagesParent.SetActive(false);
    }

    public void ChooseYesToProceedATour()
    {
        // Check if player's game data meet world tour requirements
        if(!PlayerGameData.instance.IsStaminaOverCost(currentWorldTour.staminaCost) || !PlayerGameData.instance.IsCurrencyOverCost(currentWorldTour.totalCurrencyCost))
        {
            return;
        }
        else
        {
            PlayerGameData.instance.UseCurrencyandStamina(currentWorldTour.totalCurrencyCost, currentWorldTour.staminaCost);
            PlayerGameData.instance.SavePlayerGameData();
            // Set appropriate background related to world tour location
            loadingOtwStageBackground.sprite = currentWorldTour.loadingScreen;
            loadingOtwStageSlider.value = 0f;
            StartCoroutine(LoadingOtwStageAnimation());
        }
    }

    IEnumerator LoadingOtwStageAnimation()
    {
        float loadingCounter = 1f;
        loadingOtwStageText.text = "Loading";
        loadingOtwStageSlider.value = (1f - loadingCounter);
        loadingOtwStage.SetActive(true);
        yield return new WaitForSeconds(timeDelayLoading);
        loadingCounter -= (1f / loadingStep);
        loadingOtwStageText.text = "Loading.";
        loadingOtwStageSlider.value = (1f - loadingCounter);
        yield return new WaitForSeconds(timeDelayLoading);
        loadingCounter -= (1f / loadingStep);
        loadingOtwStageText.text = "Loading..";
        loadingOtwStageSlider.value = (1f - loadingCounter);
        yield return new WaitForSeconds(timeDelayLoading);
        loadingCounter -= (1f / loadingStep);
        loadingOtwStageText.text = "Loading...";
        loadingOtwStageSlider.value = (1f - loadingCounter);

        yield return new WaitForSeconds(timeDelayLoading);
        loadingCounter -= (1f / loadingStep);
        loadingOtwStageText.text = "Loading.";
        loadingOtwStageSlider.value = (1f - loadingCounter);
        yield return new WaitForSeconds(timeDelayLoading);
        loadingCounter -= (1f / loadingStep);
        loadingOtwStageText.text = "Loading..";
        loadingOtwStageSlider.value = (1f - loadingCounter);
        yield return new WaitForSeconds(timeDelayLoading);
        loadingCounter -= (1f / loadingStep);
        loadingOtwStageText.text = "Loading...";
        loadingOtwStageSlider.value = (1f - loadingCounter);

        yield return new WaitForSeconds(timeDelayLoading);
        Stages();
    }
    #endregion

    #region STAGES
    public void Stages()
    {
        if (goToStage || isPromotion)
        {
            loadPromotionStage.SetActive(false);
            detailPromotion.SetActive(false);
            SetupStagePromotion();
        }
        else
        {
            proceedATour.SetActive(false);
            detailWorldwideTour.SetActive(false);
            SetupStageWorldTour();
        }
        PlayerGameData.instance.CheckPromotion();

        PlayerGameData.instance.JustDonePromotionOrTourToday();
        PlayerGameData.instance.SavePlayerGameData();
        StartCoroutine(StagesAnimation());
    }

    private void SetupStagePromotion()
    {
        Debug.Log(loadedSchedule.promotion.tvName);
        stageTitleText.text = "Promotion Stage - " + loadedSchedule.promotion.tvName;

        float totalAudience = ((PlayerGameData.instance.currentSongRate / 10f) * (0.7f * loadedSchedule.promotion.tvPopularity)) + (0.3f * loadedSchedule.promotion.capacity);
        int audience = Mathf.Clamp(Mathf.CeilToInt(totalAudience), 0, loadedSchedule.promotion.tvPopularity);
        audience = UnityEngine.Random.Range(audience - Mathf.CeilToInt(0.1f * (float)audience), audience + 1);

        stageAudience = audience;
        stageAudienceText.text = CommonHandler.instance.IntToString(audience, true);
        stageGroupStaminaText.text = loadedSchedule.promotion.staminaCostPerDay.ToString();
    }

    private void SetupStageWorldTour()
    {
        string location = currentWorldTour.location.ToString().Replace("_", " ");
        stageTitleText.text = "World Tour - " + location;

        float totalAudience = ((PlayerGameData.instance.currentSongRate / 10f) * (0.7f * currentWorldTour.capacity)) + ((0.3f * currentWorldTour.capacity) * PlayerGameData.instance.GetRatioOfPopularity());
        int audience = Mathf.Clamp(Mathf.CeilToInt(totalAudience), 0, currentWorldTour.capacity);
        audience = UnityEngine.Random.Range(audience - Mathf.CeilToInt(0.1f * (float)audience), audience+1);

        stageAudience = audience;
        stageAudienceText.text = CommonHandler.instance.IntToString(audience, true);
        stageGroupStaminaText.text = currentWorldTour.staminaCost.ToString();
    }

    IEnumerator StagesAnimation()
    {
        stages++;
        detailStage.SetActive(true);
       
        // Go To Stage
        yield return new WaitForSeconds(timeDelayToStage);
        goToNextStage.SetActive(false);
        stageReport.SetActive(false);
        stageHighlight.SetActive(false);
        stageEnd.SetActive(false);

        // The Stage
        CommonHandler.instance.FadeInAnimation(theStage, null, 0.5f);
        Coroutine stageLiveCoroutine = StartCoroutine(StageLiveAnimation());
        MusicPlayer.instance.PlayCurrentSong();
        if (!goToStage && !isPromotion) { CommonHandler.instance.FadeOutAnimation(loadingOtwStage, null, 0.5f); }

        // The Stage to Stage Report
        ResetStageReport();
        yield return new WaitForSeconds(timeDelayToStageReport);
        MusicPlayer.instance.StopSong();
        StopCoroutine(stageLiveCoroutine);
        CommonHandler.instance.FadeInAnimation(stageReport, null, 0.5f);
        yield return new WaitForSeconds(0.5f);
        theStage.SetActive(false);

        // Stage Report Animation
        yield return new WaitForSeconds(timeDelayInStageReport);
        CommonHandler.instance.FadeInAnimation(stageReportAddedMoney.gameObject, null, timeDelayInStageReport * 0.5f);
        foreach (GameObject report in stageReportBalance)
        {
            CommonHandler.instance.FadeInAnimation(report, null, timeDelayInStageReport * 0.5f);
        }

        if (goToStage || isPromotion)
        {
            //CommonHandler.instance.FadeInAnimation(restButton.gameObject, null, timeDelayInStageReport * 0.5f);
            restButton.gameObject.SetActive(false);
            yield return new WaitForSeconds(timeDelayInStageReport * 0.5f);
            nextButton.GetComponent<Image>().sprite = activatedNextButton;
            //restButton.interactable = true;
            nextButton.interactable = true;
        }
        else
        {
            yield return new WaitForSeconds(timeDelayInStageReport * 0.5f);
            nextButton.GetComponent<Image>().sprite = activatedNextButton;
            nextButton.interactable = true;
        }
    }

    IEnumerator StageLiveAnimation()
    {
        List<Sprite> gigPhotos = new List<Sprite>();
        gigPhotos.AddRange(ResetGigPhotos());
        while (true)
        {
            if(gigPhotos.Count <= 0) { gigPhotos.AddRange(ResetGigPhotos()); }
            int randomIndex = UnityEngine.Random.Range(0, gigPhotos.Count);
            theStageLivePics.sprite = gigPhotos[randomIndex];
            gigPhotos.RemoveAt(randomIndex);

            if(theStage.gameObject.activeSelf == false) { break; }
            yield return new WaitForSeconds(2f);
        }
    }

    private List<Sprite> ResetGigPhotos()
    {
        List<Sprite> gigPhotos = new List<Sprite>();
        foreach (int traineeIndex in PlayerSaveData.instance.debutTraineesIndex)
        {
            Trainee trainee = InformationHandler.instance.trainees[traineeIndex];
            gigPhotos.Add(trainee.gigPhoto);
        }
        return gigPhotos;
    }

    void ResetStageReport()
    {
        foreach(GameObject report in stageReportResults)
        {
            report.SetActive(true);
        }
        stageReportAddedMoney.gameObject.SetActive(false);
        foreach (GameObject report in stageReportBalance)
        {
            report.SetActive(false);
        }
        restButton.gameObject.SetActive(false);
        restButton.interactable = false;
        nextButton.interactable = false;
        nextButton.gameObject.SetActive(true);
        nextButton.GetComponent<Image>().sprite = inactivatedNextButton;

        if (goToStage || isPromotion)
        {
            stageReportTotalAudience.text = CommonHandler.instance.IntToString(stageAudience, true);
            stageReportTicketSold.text = CommonHandler.instance.IntToString(stageAudience, true) + "/" + CommonHandler.instance.IntToString(loadedSchedule.promotion.tvPopularity, true);
            int popularity = Mathf.CeilToInt(((float)PlayerGameData.instance.currentSongRate / 10f - 0.6f) * (float)stageAudience);
            stageReportPopularity.text = popularity >= 0 ? "+ " : "- ";
            stageReportPopularity.text += Mathf.Abs(popularity);
            PlayerGameData.instance.AddPopularity(popularity);
            int addedMoney = stageAudience * loadedSchedule.promotion.bookingCostPerDay;
            PlayerGameData.instance.AddProfit(loadedSchedule.timeSchedule, addedMoney);
            stageReportAddedMoney.text = CommonHandler.instance.IntToString(addedMoney);
            stageReportCurrentMoney.text = CommonHandler.instance.IntToString(PlayerGameData.instance.GetCurrency());
            stageReportCurrentGroupStamina.text = PlayerGameData.instance.GetStamina().ToString();
        }
        else
        {
            stageReportTotalAudience.text = CommonHandler.instance.IntToString(stageAudience, true);
            stageReportTicketSold.text = CommonHandler.instance.IntToString(stageAudience, true) + "/" + CommonHandler.instance.IntToString(currentWorldTour.capacity, true);
            int popularity = Mathf.CeilToInt(((float)PlayerGameData.instance.currentSongRate / 10f - 0.6f) * (float)stageAudience); 
            stageReportPopularity.text = popularity >= 0 ? "+ " : "- ";
            stageReportPopularity.text += Mathf.Abs(popularity);
            PlayerGameData.instance.AddPopularity(popularity);
            int addedMoney = stageAudience * currentWorldTour.bookingCostPerDay;
            PlayerGameData.instance.AddProfit(PlayerGameData.instance.GetCurrentDate(), addedMoney);
            stageReportAddedMoney.text = CommonHandler.instance.IntToString(addedMoney);
            stageReportCurrentMoney.text = CommonHandler.instance.IntToString(PlayerGameData.instance.GetCurrency());
            stageReportCurrentGroupStamina.text = PlayerGameData.instance.GetStamina().ToString();
        }
    }

    public void ChooseNextInStageReport()
    {
        CommonHandler.instance.FadeInAnimation(proceedToStageHighlight, null, 0.5f);
    }

    public void ChooseNoToOpenStageHighlight()
    {
        ShowStageEnd();
    }

    private Sprite RandomHighlightPhoto(int highlightIndex)
    {
        List<Sprite> highlightPhotos = new List<Sprite>();
        foreach (int traineeIndex in PlayerSaveData.instance.debutTraineesIndex)
        {
            Trainee trainee = InformationHandler.instance.trainees[traineeIndex];
            highlightPhotos.Add(trainee.highlightPhoto[highlightIndex]);
        }
        Sprite randomHighlightPhoto = highlightPhotos[UnityEngine.Random.Range(0, highlightPhotos.Count)];
        return randomHighlightPhoto;
    }

    public void ChooseYesToOpenStageHighlight()
    {
        isHighlightShowed = true;
        stageHighlight.transform.GetChild(0).gameObject.SetActive(false);
        stageHighlight.transform.GetChild(1).gameObject.SetActive(false);
        highlightImages[0].sprite = RandomHighlightPhoto(0);
        highlightImages[1].sprite = RandomHighlightPhoto(1);

        StartCoroutine(StageHighlightAnimation());
    }

    IEnumerator StageHighlightAnimation()
    {
        CommonHandler.instance.FadeOutAnimation(proceedToStageHighlight, null, 0.5f);
        CommonHandler.instance.FadeInAnimation(stageHighlight.transform.GetChild(0).gameObject, stageHighlight, stageHighlightDelay * 0.5f);
        yield return new WaitForSeconds(stageHighlightDelay);
        CommonHandler.instance.FadeInAnimation(stageHighlight.transform.GetChild(1).gameObject, null, stageHighlightDelay * 0.5f);
        yield return new WaitForSeconds(stageHighlightDelay);
        ShowStageEnd();
    }

    public void SkipStageHighlight()
    {
        if(stages >= maxStages)
        {
            ShowStageEnd();
        }
        else
        {
            OpenProceedToNextStage();
        }
    }

    void OpenProceedToNextStage()
    {
        stageCounterText.text = (stages).ToString() + "/" + maxStages.ToString() + " stages left";
        proceedToNextStage.SetActive(true);
    }

    public void ChooseNoToProceedNextStage()
    {
        proceedToNextStage.SetActive(false);
        stageReport.SetActive(false);
        stageHighlight.SetActive(false);
        detailStage.SetActive(false);
        stagesParent.SetActive(false);
        ResetStage();
    }

    public void ChooseYesToProceedNextStage()
    {
        proceedToNextStage.SetActive(false);
        StartCoroutine(StagesAnimation());
    }

    void ShowStageEnd()
    {
        if (goToStage || isPromotion)
        {
            stageEndTitle.text = promotionStageEndTitle;
            CommonHandler.instance.FadeInAnimation(stageEnd, null, 0.5f);
        }
        else
        {
            string location = currentWorldTour.location.ToString().Replace("_", " ");
            stageEndTitle.text = worldTourStageEndTitle.Replace("[location]", location);
            if (isHighlightShowed)
            {
                CommonHandler.instance.FadeInAnimation(stageEnd, null, 0.5f);
            }
            else
            {
                StartCoroutine(ShowStageEndAnimation());
            }
        }
    }
    IEnumerator ShowStageEndAnimation()
    {
        CommonHandler.instance.FadeOutAnimation(proceedToStageHighlight, null, 0.5f);
        yield return new WaitForSeconds(0.5f);
        CommonHandler.instance.FadeInAnimation(stageEnd, null, 0.5f);
    }

    public void CloseStageEnd()
    {
        if (isHighlightShowed)
            CommonHandler.instance.FadeOutAnimation(stageHighlight, null, 0.5f);
        proceedToStageHighlight.SetActive(false);
        CommonHandler.instance.FadeOutAnimation(stageEnd, null, 0.5f);
        CommonHandler.instance.FadeOutAnimation(stageReport, detailStage, 0.5f);
        CommonHandler.instance.FadeOutAnimation(stagesParent, null, 0.5f);

        if (goToStage)
        {
            // Change Game Dates
            DateTime newGameDate = loadedSchedule.timeSchedule.AddHours(loadedSchedule.promotion.promotionDuration);
            Debug.Log("Promotion ended, now dates become: "+ newGameDate.ToString("dd-MM-yyyy hh:mm:ss"));
            PlayerGameData.instance.ChangeGameDate(newGameDate);
        }
        else
        {
            DateTime newGameDate = PlayerGameData.instance.GetCurrentDate().AddHours(4);
            Debug.Log("World Tour ended, now dates become: " + newGameDate.ToString("dd-MM-yyyy hh:mm:ss"));
            PlayerGameData.instance.ChangeGameDate(newGameDate);
        }
        PlayerGameData.instance.SavePlayerGameData();

        ResetStage();
        GameMenuUI.instance.ChangeMenu(0);
    }

    void ResetStage()
    {
        if (goToStage)
        {
            loadedSchedule = null;
            goToStage = false;
        }
        isHighlightShowed = false;
        stages = 0;
    }
    #endregion
}
