using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class ProduceHandler : MonoBehaviour
{
    [SerializeField] GameObject detailProduce;
    [SerializeField] GameObject proceedToProduce;
    [SerializeField] GameObject composingSong;
    [SerializeField] GameObject songIsFinished;
    [SerializeField] GameObject releaseThisSong;
    [SerializeField] GameObject groupStatusUpdated;
    [SerializeField] TextMeshProUGUI staminaCostText;
    [SerializeField] TextMeshProUGUI moneySpentText;
    [SerializeField] Button previewSongButton;
    [SerializeField] TextMeshProUGUI previewSongText;
    [SerializeField] TextMeshProUGUI previewSongRateText;
    bool isSongPlayed = false;

    [Header("Produce Settings")]
    [SerializeField] int staminaCost;
    [SerializeField] int moneySpent;

    [Header("Pop Up New Song Not Released Yet")]
    [SerializeField] GameObject popUpSongNotReleased;

    [Header("Songs Writing / Proceed to Produce")]
    [SerializeField] TextMeshProUGUI pleaseWaitText;
    [SerializeField] float writeSongDelay;
    [SerializeField] float composingTime;

    [Header("Release This Song")]
    [SerializeField] TextMeshProUGUI songRatingText;
    int songRating;

    void Start()
    {
        CloseAll();
        staminaCostText.text = staminaCost.ToString();
        moneySpentText.text = moneySpent.ToString();
        SetupPreviewSongButton();
    }

    public void SetupPreviewSongButton()
    {
        if (PlayerGameData.instance.currentSong == null)
        {
            previewSongButton.gameObject.SetActive(false);
        }
        else
        {
            previewSongRateText.text = PlayerGameData.instance.currentSongRate + "/10";
            previewSongButton.gameObject.SetActive(true);
        }
    }

    public void OpenProceedToProduce()
    {
        if (PlayerGameData.instance.IsSongWrittenNotReleased)
        {
            ShowPopupNewSongNotReleasedYet();
            return;
        }

        if(PlayerGameData.instance.IsStaminaOverCost(staminaCost) && PlayerGameData.instance.IsCurrencyOverCost(moneySpent))
        {
            PlayerGameData.instance.UseCurrencyandStamina(moneySpent, staminaCost);
            WriteSong();
        }
    }

    private void ShowPopupNewSongNotReleasedYet()
    {
        CommonHandler.instance.FadeInAnimation(popUpSongNotReleased);
    }

    public void ConfirmProceedToProduce()
    {
        PlayerGameData.instance.IsSongWrittenNotReleased = false;
        CommonHandler.instance.FadeOutAnimation(popUpSongNotReleased, null, 0f);
        OpenProceedToProduce();
    }

    public void RestTheBand()
    {
        CommonHandler.instance.FadeOutAnimation(popUpSongNotReleased);
    }

    private void WriteSong()
    {
        CommonHandler.instance.FadeInAnimation(proceedToProduce, detailProduce, 0.25f);
        StartCoroutine(WriteSongAnimation());
    }

    IEnumerator WriteSongAnimation()
    {
        yield return new WaitForSeconds(writeSongDelay / 2f);
        CommonHandler.instance.FadeInAnimation(composingSong, composingSong.transform.parent.gameObject, 0.5f);
        yield return new WaitForSeconds(0.5f);

        MusicPlayer.instance.GetRandomSong();
        MusicPlayer.instance.PlayCurrentSong();

        float timeLeft = composingTime;
        float timeKeeper = 0f;
        float timeCounter = 1f;
        string defaultPleaseWaitText = "Please Wait";
        string addedToPleaseWaitText = "";

        pleaseWaitText.text = defaultPleaseWaitText;
        while (timeLeft >= 0.0f)
        {
            timeLeft -= Time.deltaTime;
            timeKeeper += Time.deltaTime;

            if (timeKeeper >= writeSongDelay * timeCounter)
            {
                if (addedToPleaseWaitText == "...")
                {
                    addedToPleaseWaitText = "";
                }
                addedToPleaseWaitText += ".";
                pleaseWaitText.text = defaultPleaseWaitText + addedToPleaseWaitText;
                timeCounter++;
            }

            yield return null;
        }
        MusicPlayer.instance.StopSong();
        songIsFinished.SetActive(true);
        composingSong.SetActive(false);
    }

    public void OpenReleaseThisSong()
    {
        songIsFinished.SetActive(false);
        songIsFinished.transform.parent.gameObject.SetActive(false);
        songRating = Random.Range(1, 11);
        songRatingText.text = "This song is rated "+ songRating.ToString() +" out of 10.\nRelease this song ? ";
        releaseThisSong.SetActive(true);
    }

    public void WriteAnotherSong()
    {
        if (PlayerGameData.instance.IsStaminaOverCost(staminaCost) && PlayerGameData.instance.IsCurrencyOverCost(moneySpent))
        {
            PlayerGameData.instance.UseCurrencyandStamina(moneySpent, staminaCost);
            releaseThisSong.SetActive(false);
            WriteSong();
        }
    }

    public void OpenGroupStatusUpdated()
    {
        // Setup the current song
        PlayerGameData.instance.currentSongRate = songRating;
        PlayerGameData.instance.totalSongsReleased++;
        PlayerGameData.instance.UpdateTotalSongsReleased(false);
        PlayerGameData.instance.SavePlayerGameData();
        previewSongRateText.text = songRating + "/10";
        SetupPreviewSongButton();
        groupStatusUpdated.SetActive(true);
    }

    public void CloseAll()
    {
        groupStatusUpdated.SetActive(false);
        releaseThisSong.SetActive(false);
        songIsFinished.SetActive(false);
        composingSong.SetActive(false);
        composingSong.transform.parent.gameObject.SetActive(false);
        proceedToProduce.SetActive(false);
        detailProduce.SetActive(false);
    }

    #region PREVIEW SONG  
    public void PreviewSong()
    {
        PlayerGameData playerGameData = PlayerGameData.instance;
        if (isSongPlayed)
        {
            MusicPlayer.instance.StopSong();
            previewSongText.text = "Preview Song";
            isSongPlayed = false;
        }
        else
        {
            MusicPlayer.instance.PlayCurrentSong();
            previewSongText.text = "Stop Song";
            isSongPlayed = true;
        }
    }
    
    #endregion
}
