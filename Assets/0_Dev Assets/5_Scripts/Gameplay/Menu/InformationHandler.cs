using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;

public class InformationHandler : MonoBehaviour
{
    public static InformationHandler instance;

    [Header("Sub Menu")]
    [SerializeField] GameObject groupInformation;
    [SerializeField] GameObject members;
    [SerializeField] GameObject getAStatusSymbol;
    [SerializeField] GameObject detailGroupInformation;

    [Header("Group Information")]
    [SerializeField] TextMeshProUGUI groupNameText;
    [SerializeField] TextMeshProUGUI groupEstablishedSinceText;
    [SerializeField] TextMeshProUGUI totalSongsReleasedText;
    [SerializeField] TextMeshProUGUI popularityText;
    //[SerializeField] TextMeshProUGUI contractDueText;
    //[SerializeField] TextMeshProUGUI fanClubText;
    //[SerializeField] TextMeshProUGUI groupHappinessText;

    [Header("Display Status Symbol")]
    [SerializeField] Color activeSeeStatusSymbolColor;
    [SerializeField] Color inactiveSeeStatusSymbolColor;
    [SerializeField] TextMeshProUGUI seeStatusSymbolText;
    [SerializeField] GameObject displayStatusSymbol;
    [SerializeField] TextMeshProUGUI displayStatusSymbolTitle;
    [HideInInspector] List<bool> groupStatusSymbols = new List<bool>();
    [SerializeField] List<DisplayStatusSymbolUI> displayStatusSymbolUI = new List<DisplayStatusSymbolUI>();

    [Header("Members")]
    public List<Trainee> trainees = new List<Trainee>();
    [SerializeField] List<MemberSetupUI> listMemberSetupUI = new List<MemberSetupUI>();
    [SerializeField] GameObject detailMember;
    [SerializeField] GameObject isiDetailMember;
    [SerializeField] float delayMemberAnimation;
    Transform lastHoveredTraineeParent = null;

    [Header("Get Status Symbol")]
    [SerializeField] List<StatusSymbol> statusSymbols = new List<StatusSymbol>();
    [SerializeField] List<StatusSymbolUI> statusSymbolsUI = new List<StatusSymbolUI>();
    [SerializeField] Sprite defaultPhoto;
    [SerializeField] Sprite filledPhoto;
    [SerializeField] Sprite detailDefaultPhoto;
    [SerializeField] Color soldOutColor;
    [SerializeField] List<bool> productsSoldOutStatus = new List<bool>();

    [Header("Detail Member")]
    [SerializeField] TextMeshProUGUI detailMemberName;
    [SerializeField] TextMeshProUGUI detailMemberAge;
    [SerializeField] TextMeshProUGUI detailMemberPosition;
    [SerializeField] TextMeshProUGUI detailMemberTraits;
    [SerializeField] Image detailMemberBigImage;
    [SerializeField] GameObject detailMemberSticker;

    [Header("Detail Get A Status Symbol")]
    [SerializeField] GameObject detailStatusSymbol;
    [SerializeField] GameObject productDetailNotSoldOut;
    [SerializeField] GameObject confirmProductPurchase;
    [SerializeField] GameObject productDetailSoldOut;
    [SerializeField] GameObject notifNotEnoughMoney;
    [SerializeField] TextMeshProUGUI productDetailName;
    [SerializeField] TextMeshProUGUI productDetailPrice;
    int choosedProductPrice;
    [SerializeField] Image productDetailPhoto;
    [SerializeField] Image productCheckoutPhoto;
    [SerializeField] Sprite productDefaultCheckoutSprite;

    int indexOpenedProduct = -1;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        groupInformation.SetActive(true);
        members.SetActive(false);
        getAStatusSymbol.SetActive(false);
        detailMember.SetActive(false);
        detailStatusSymbol.SetActive(false);
        detailGroupInformation.SetActive(false);

        for (int i = 0; i < statusSymbolsUI.Count; i++)
        {
            productsSoldOutStatus.Add(false);
        }

        SetupGroupInformation();
        SetupDisplayStatusSymbol();
        SetupMember();
        SetupGetStatusSymbol();
    }

    #region SETUP INFORMATION
    private void SetupGroupInformation()
    {
        groupNameText.text = PlayerSaveData.instance.groupName;
        groupEstablishedSinceText.text = PlayerSaveData.instance.establishedDate.ToString("MMMM, d yyyy");
        totalSongsReleasedText.text = PlayerGameData.instance.totalSongsReleased.ToString();
        popularityText.text = PlayerGameData.instance.popularity.ToString();
        displayStatusSymbolTitle.text = PlayerSaveData.instance.groupName + " Status Symbol(s)";
    }

    private void SetupDisplayStatusSymbol()
    {
        for (int i = 0; i < statusSymbols.Count; i++)
        {
            groupStatusSymbols.Add(false);
            displayStatusSymbolUI[i].unObtained.SetActive(true);
            displayStatusSymbolUI[i].obtained.SetActive(false);
        }
    }

    private void SetupMember()
    {
        PlayerSaveData playerData = PlayerSaveData.instance;
        for (int i = 0; i < listMemberSetupUI.Count; i++)
        {
            Trainee thisTrainee = trainees[playerData.debutTraineesIndex[i]];
            listMemberSetupUI[i].memberSmallImage.sprite = thisTrainee.smallPhoto;
            listMemberSetupUI[i].memberName.text = thisTrainee.traineeName;
            listMemberSetupUI[i].memberSmallImageHovered.sprite = thisTrainee.smallPhoto;
            listMemberSetupUI[i].memberNameHovered.text = thisTrainee.traineeName;
            listMemberSetupUI[i].memberTrainee = thisTrainee;

            if (thisTrainee.position == TraineePosition.Vocalist)
            {
                playerData.vocalistSex = thisTrainee.sex;
            }
        }
    }

    private void SetupGetStatusSymbol()
    {
        for (int i = 0; i < statusSymbols.Count; i++)
        {
            statusSymbolsUI[i].productName.text = statusSymbols[i].productName;

            statusSymbolsUI[i].productPrice.text = CommonHandler.instance.IntToString(statusSymbols[i].productPrice);

            statusSymbolsUI[i].productFilledImage.sprite = defaultPhoto;
            if (statusSymbols[i].productFrontPhoto != null)
            {
                statusSymbolsUI[i].productFilledImage.sprite = filledPhoto;
                statusSymbolsUI[i].productFrontImage.sprite = statusSymbols[i].productFrontPhoto;
            }
            statusSymbolsUI[i].statusSymbolItem = statusSymbols[i];
            statusSymbolsUI[i].seeDetails.interactable = true;
            statusSymbolsUI[i].soldOut.SetActive(false);

            // Setup For Matched Obtained Status Symbols
            bool itemMatched = false;
            List<StatusSymbol> obtainedStatusSymbols = PlayerGameData.instance.obtainedStatusSymbols;
            for (int j = 0; j < obtainedStatusSymbols.Count; j++)
            {
                if (obtainedStatusSymbols[j].productIndex == statusSymbols[i].productIndex)
                {
                    itemMatched = true;
                    break;
                }
            }
            if (itemMatched)
            {
                SetupObtainedStatusSymbol(statusSymbols[i]);
            }
        }
    }
    #endregion

    #region HOVER ANIMATION
    public void HoverAnimation(Transform traineeParent)
    {
        if(SystemInfo.deviceType == DeviceType.Desktop)
        {
            if (lastHoveredTraineeParent != null && GameObject.ReferenceEquals(traineeParent.gameObject, lastHoveredTraineeParent.gameObject)) { return; }
            for (int i = 0; i < traineeParent.childCount; i++)
            {
                if (i < 2)
                {
                    traineeParent.GetChild(i).gameObject.SetActive(false);
                }
                else
                {
                    traineeParent.GetChild(i).gameObject.SetActive(true);
                }
            }
            lastHoveredTraineeParent = traineeParent;
        }
    }

    public void UnHoverAnimation(Transform traineeParent)
    {
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            if (lastHoveredTraineeParent == null) { return; }
            for (int i = 0; i < traineeParent.childCount; i++)
            {
                if (i < 2)
                {
                    traineeParent.GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    traineeParent.GetChild(i).gameObject.SetActive(false);
                }
            }
            lastHoveredTraineeParent = null;
        }
    }
    #endregion

    #region DISPLAY SUB MENU
    public void OpenGeneralInformation()
    {
        members.SetActive(false);
        getAStatusSymbol.SetActive(false);
        groupInformation.SetActive(true);
    }

    public void OpenMembers()
    {
        groupInformation.SetActive(false);
        getAStatusSymbol.SetActive(false);
        members.SetActive(true);
    }

    public void OpenGetAStatusSymbol()
    {
        groupInformation.SetActive(false);
        members.SetActive(false);
        getAStatusSymbol.SetActive(true);
    }
    #endregion

    #region DISPLAY DETAIL MENU
    #region INFORMATION
    // Highlight See Status Symbol Text
    public void HighlightText()
    {
        seeStatusSymbolText.color = activeSeeStatusSymbolColor;
    }

    public void ResetHighlightText()
    {
        seeStatusSymbolText.color = inactiveSeeStatusSymbolColor;
    }

    /// <summary>
    /// Detail Display Status Symbol
    /// </summary>
    /// <param name="choosedMember"></param>
    public void OpenDisplayStatusSymbol()
    {
        CommonHandler.instance.FadeInAnimation(displayStatusSymbol, detailGroupInformation);
    }

    public void CloseDisplayStatusSymbol()
    {
        CommonHandler.instance.FadeOutAnimation(displayStatusSymbol, detailGroupInformation);
    }
    #endregion

    #region MEMBER
    /// <summary>
    /// Detail Member
    /// </summary>
    /// <param name="choosedMember"></param>
    public void OpenDetailMember(MemberSetupUI choosedMemberUI)
    {
        detailGroupInformation.SetActive(true);

        // Set the content inside
        Trainee choosedTrainee = choosedMemberUI.memberTrainee;
        detailMemberBigImage.sprite = choosedTrainee.bigPhoto;
        detailMemberName.text = choosedTrainee.traineeName;
        detailMemberAge.text = choosedTrainee.age.ToString();
        detailMemberPosition.text = choosedTrainee.position.ToString();
        string traineeTrait = "";
        foreach (string trait in choosedTrainee.traits)
        {
            if (traineeTrait != "") { traineeTrait += ", "; }
            traineeTrait += trait;
        }
        detailMemberTraits.text = traineeTrait;

        // Set Sticker in Detail Member according to Trainee Position
        // Set all child to inactive
        for (int i = 0; i < detailMemberSticker.transform.childCount; i++)
        {
            var child = detailMemberSticker.transform.GetChild(i).gameObject;
            if (child != null)
                child.SetActive(false);

            if (i == ((int)choosedTrainee.position))
            {
                child.SetActive(true);
            }
        }

        // For this occasion, just open the template detail member
        CommonHandler.instance.FadeInAnimation(isiDetailMember, detailMember);
    }

    public void CloseDetailMember()
    {
        CommonHandler.instance.FadeOutAnimation(isiDetailMember, detailMember, 0.1f);
    }
    #endregion

    #region GET STATUS SYMBOL
    /// <summary>
    /// Detail Status Symbol
    /// </summary>
    /// <param name="choosedStatusSymbol"></param>

    StatusSymbol choosedItem = null;
    public void OpenDetailStatusSymbol(StatusSymbolUI choosedStatusSymbol)
    {
        detailGroupInformation.SetActive(true);
        // Open member with the choosed Index
        int indexOpenedStatusSymbol = choosedStatusSymbol.transform.GetSiblingIndex();
        indexOpenedProduct = indexOpenedStatusSymbol;

        // Set the content inside
        choosedItem = choosedStatusSymbol.statusSymbolItem;
        productDetailName.text = choosedItem.productName;
        productDetailPhoto.sprite = detailDefaultPhoto;
        if (choosedItem.productDetailPhoto != null)
        {
            productDetailPhoto.sprite = choosedItem.productDetailPhoto;
            productDetailPhoto.SetNativeSize();
        }
        //productDetailDescription.text = item.productDescription;
        choosedProductPrice = choosedItem.productPrice;
        productDetailPrice.text = choosedStatusSymbol.productPrice.text;

        // For this occasion, just open the template detail status symbol
        if (productsSoldOutStatus[indexOpenedStatusSymbol] == true)
            CommonHandler.instance.FadeInAnimation(productDetailSoldOut, detailStatusSymbol);
        else CommonHandler.instance.FadeInAnimation(productDetailNotSoldOut, detailStatusSymbol);
    }

    public void CloseDetailStatusSymbol()
    {
        choosedProductPrice = 0;
        choosedItem = null;
        if (productDetailSoldOut.activeSelf)
            CommonHandler.instance.FadeOutAnimation(productDetailSoldOut, detailStatusSymbol);
        else
            CommonHandler.instance.FadeOutAnimation(productDetailNotSoldOut, detailStatusSymbol);
    }

    public void OpenConfirmationPurchaseProduct()
    {
        if (PlayerGameData.instance.IsCurrencyOverCost(choosedProductPrice))
        {
            // Setup Confirmation Checkout Purchase Product
            if (choosedItem.productCheckoutPhoto != null)
            {
                productCheckoutPhoto.sprite = choosedItem.productCheckoutPhoto;
                productCheckoutPhoto.SetNativeSize();
                Vector3 targetPos = choosedItem.checkoutPhotoPosition;
                productCheckoutPhoto.rectTransform.anchoredPosition = new Vector3(targetPos.x, targetPos.y, 0);
            }
            else
            {
                productCheckoutPhoto.sprite = productDefaultCheckoutSprite;
                productCheckoutPhoto.SetNativeSize();
            }
            CommonHandler.instance.FadeInAnimation(confirmProductPurchase);
        }
        else
        {
            CommonHandler.instance.FadeInAnimation(notifNotEnoughMoney);
        }
    }

    public void CancelConfirmationPurchaseProduct()
    {
        CommonHandler.instance.FadeOutAnimation(confirmProductPurchase);
    }

    public void ConfirmNotEnoughMoney()
    {
        CommonHandler.instance.FadeOutAnimation(notifNotEnoughMoney);
    }

    public void ConfirmProductPurchase()
    {
        PlayerGameData.instance.UseCurrency(choosedProductPrice);
        PlayerGameData.instance.ObtainStatusSymbol(choosedItem);
        PlayerGameData.instance.SavePlayerGameData();
        statusSymbolsUI[indexOpenedProduct].seeDetailsText.color = soldOutColor;
        statusSymbolsUI[indexOpenedProduct].seeDetails.interactable = false;
        statusSymbolsUI[indexOpenedProduct].soldOut.SetActive(true);
        groupStatusSymbols[indexOpenedProduct] = true;
        displayStatusSymbolUI[indexOpenedProduct].unObtained.SetActive(false);
        displayStatusSymbolUI[indexOpenedProduct].obtained.SetActive(true);

        indexOpenedProduct = -1;
        CommonHandler.instance.FadeOutAnimation(confirmProductPurchase);
        CloseDetailStatusSymbol();
    }

    private void SetupObtainedStatusSymbol(StatusSymbol choosedItem)
    {
        statusSymbolsUI[choosedItem.productIndex].seeDetailsText.color = soldOutColor;
        statusSymbolsUI[choosedItem.productIndex].seeDetails.interactable = false;
        statusSymbolsUI[choosedItem.productIndex].soldOut.SetActive(true);
        groupStatusSymbols[choosedItem.productIndex] = true;
        displayStatusSymbolUI[choosedItem.productIndex].unObtained.SetActive(false);
        displayStatusSymbolUI[choosedItem.productIndex].obtained.SetActive(true);
        productsSoldOutStatus[choosedItem.productIndex] = true;
    }
    #endregion
    #endregion
}
