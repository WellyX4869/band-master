using System;

public class ProfitPerDay
{
    public DateTime profitDateTime;
    public int profitAmount;

    public ProfitPerDay(DateTime profitDateTime, int profitAmount)
    {
        this.profitDateTime = profitDateTime;
        this.profitAmount = profitAmount;
    }
}