using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using System;
using TMPro;

public class PlayerGameData : MonoBehaviour
{
    public static PlayerGameData instance;

    [Header("Debug Purposes")]
    [SerializeField] int stamina;
    [SerializeField] int currency;
    [SerializeField] DateTime currentDate;

    [Header("Linked GameObject")]
    [SerializeField] Slider staminaSlider;
    [SerializeField] TextMeshProUGUI staminaText;
    [SerializeField] TextMeshProUGUI weeklyCurrentBalance;
    [SerializeField] TextMeshProUGUI monthlyCurrentBalance;
    [SerializeField] TextMeshProUGUI popularityText;
    [SerializeField] TextMeshProUGUI totalSongsReleasedText;
    [SerializeField] Transform inboxMenu = null;
    [SerializeField] Image gameStatusIcon;
    [SerializeField] TextMeshProUGUI gameStatusText;
    [SerializeField] Sprite activeSprite;
    [SerializeField] Sprite inactiveSprite;
    public bool IsSongWrittenNotReleased = true;

    [Header("Maximum Game Data Value")]
    [SerializeField] int maxStamina;

    [Header("Songs")]
    public AudioClip currentSong;
    public int currentSongRate;

    [Header("ProfitPerDay")]
    public List<ProfitPerDay> profitsData = new List<ProfitPerDay>();

    [Header("Informations")]
    public int debutSponsorshipFund = 1500;
    public bool hasReceivedFunds = false;
    public int totalSongsReleased = 0;
    public int popularity = 0;
    public int maxPopularity = 10000;
    public List<StatusSymbol> obtainedStatusSymbols = new List<StatusSymbol>();

    [Header("Stages")]
    public List<BandSchedule> onGoingBandSchedules = new List<BandSchedule>();

    [Header("Lose Settings")]
    [HideInInspector] public bool IsLose = false;
    [HideInInspector] public List<bool> songPromotionChecker = new List<bool>();

    bool todayHasDonePromotionOrTour = false;
    [HideInInspector] public bool isNewGameMailStillNewAfterLoading = true;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        currentDate = DateTime.Now;
        PlayerSaveData.instance.establishedDate = currentDate;
        PlayerSaveData.instance.SavePlayerSetupData();

        // Try to load
        if(LoadPlayerGameData() == true)
        {
            isNewGameMailStillNewAfterLoading = false;
        }
    }

    private void Start()
    {
        ChangeStaminaBar();
        ChangeCurrency();
        ChangeGameDate(currentDate);
        ChangePopularity();
        UpdateTotalSongsReleased();

        if (!IsLose)
        {
            gameStatusIcon.sprite = activeSprite;
            gameStatusText.text = "Active";
            GameMenuUI.instance.UnlockMenu();
        }
        else
        {
            GameOver();
        }
    }

    private void Update()
    {
        if(IsLose) { return; }

        if(songPromotionChecker.Count >= 2)
        {
            if(songPromotionChecker[songPromotionChecker.Count - 1] == false && songPromotionChecker[songPromotionChecker.Count - 2] == false)
            {
                GameOver();
            }
        }

        if (currency < 75 && onGoingBandSchedules.Count <= 0)
        {
            GameOver();
        }

        // UPDATE FOR ON GOING BAND SCHEDULES
        int initialSchedulesCount = onGoingBandSchedules.Count;
        if(initialSchedulesCount <= 0) { return; }
        for(int i = 0; i<onGoingBandSchedules.Count; i++)
        {
            BandSchedule schedule = onGoingBandSchedules[i];
            if (DateTime.Compare(schedule.timeSchedule, currentDate) <= 0)
            {
                onGoingBandSchedules.RemoveAt(i);
                if (onGoingBandSchedules.Count > 0)
                    i--;
                break;
            }
            else
            {
                if (schedule.timeSchedule.Year == currentDate.Year && schedule.timeSchedule.Month == currentDate.Month && schedule.timeSchedule.Day == currentDate.Day)
                {
                    schedule.IsActive = true;
                    HomeHandler.instance.SetupHomeSchedule(true);
                }
            }
        }
    }

    #region SAVE & LOAD GAME DATA
    string filePath = "/GameData";

    [Serializable]
    public class GameDataSave
    {
        public int stamina;
        public int maxStamina;
        public int currency;
        public int currentDay, currentMonth, currentYear;
        public AudioClip currentSong;
        public int currentSongRate;
        public bool IsLose;
        public List<bool> songPromotionChecker = new List<bool>();
        public bool todayHasDonePromotionOrTour;
        public int debutSponsorshipFund;
        public bool hasReceivedFunds;
        public int totalSongsReleased;
        public int popularity;
        public int maxPopularity;
        public List<StatusSymbol> obtainedStatusSymbols = new List<StatusSymbol>();

        // ProfitsData
        public List<int> profitsDataAmount = new List<int>();
        public List<Vector3Int> profitsDataTime = new List<Vector3Int>();

        // OnGoingBandSchedules
        public List<Vector3Int> bandSchedulesTime = new List<Vector3Int>();
        public List<int> bandSchedulesHour = new List<int>();
        public List<Promotion> bandSchedulesPromotion = new List<Promotion>();
        public List<bool> bandSchedulesIsActive = new List<bool>();
        public List<string> bandSchedulesTitle = new List<string>();
    }

    public void SavePlayerGameData()
    {
        GameDataSave saveItem = new GameDataSave();

        saveItem.stamina                    = stamina;
        saveItem.maxStamina                 = maxStamina;
        saveItem.currency                   = currency;
        saveItem.currentDay                 = currentDate.Day;
        saveItem.currentMonth               = currentDate.Month;
        saveItem.currentYear                = currentDate.Year;
        saveItem.currentSong                = currentSong;
        saveItem.currentSongRate            = currentSongRate;
        saveItem.IsLose                     = IsLose;
        saveItem.songPromotionChecker       = songPromotionChecker;
        saveItem.todayHasDonePromotionOrTour = todayHasDonePromotionOrTour;
        saveItem.debutSponsorshipFund       = debutSponsorshipFund;
        saveItem.hasReceivedFunds           = hasReceivedFunds;
        saveItem.totalSongsReleased         = totalSongsReleased;
        saveItem.popularity                 = popularity;
        saveItem.maxPopularity              = maxPopularity;
        saveItem.obtainedStatusSymbols      = obtainedStatusSymbols.Count > 0 ? obtainedStatusSymbols : null;

        // Save profits data
        List<int> profitsDataAmount = new List<int>();
        List<Vector3Int> profitsDataTime = new List<Vector3Int>();
        foreach(ProfitPerDay profitData in profitsData)
        {
            profitsDataAmount.Add(profitData.profitAmount);
            profitsDataTime.Add(new Vector3Int(profitData.profitDateTime.Year, profitData.profitDateTime.Month, profitData.profitDateTime.Day));
        }
        saveItem.profitsDataAmount  = profitsDataAmount;
        saveItem.profitsDataTime    = profitsDataTime;

        // Save OnGoingBandSchedules
        List<Vector3Int> bandSchedulesTime = new List<Vector3Int>();
        List<int> bandSchedulesHour = new List<int>();
        List<Promotion> bandSchedulesPromotion = new List<Promotion>();
        List<bool> bandSchedulesIsActive = new List<bool>();
        List<string> bandSchedulesTitle = new List<string>();
        foreach(BandSchedule schedule in onGoingBandSchedules)
        {
            bandSchedulesTitle.Add(schedule.scheduleTitle);
            bandSchedulesTime.Add(new Vector3Int(schedule.timeSchedule.Year, schedule.timeSchedule.Month, schedule.timeSchedule.Day));
            bandSchedulesHour.Add(schedule.timeSchedule.Hour);
            bandSchedulesPromotion.Add(schedule.promotion);
            bandSchedulesIsActive.Add(schedule.IsActive);
        }
        saveItem.bandSchedulesTitle     = bandSchedulesTitle;
        saveItem.bandSchedulesTime      = bandSchedulesTime;
        saveItem.bandSchedulesHour      = bandSchedulesHour;
        saveItem.bandSchedulesPromotion = bandSchedulesPromotion;
        saveItem.bandSchedulesIsActive  = bandSchedulesIsActive;

        string path = Application.persistentDataPath + filePath;
        string saveJson = JsonUtility.ToJson(saveItem, true);
        File.WriteAllText(path, saveJson);
    }
    
    public bool LoadPlayerGameData()
    {
        try
        {
            string path = Application.persistentDataPath + filePath;

            string loadJson = File.ReadAllText(path);
            GameDataSave loadItem = JsonUtility.FromJson<GameDataSave>(loadJson);

            stamina                     = loadItem.stamina;
            maxStamina                  = loadItem.maxStamina;
            currency                    = loadItem.currency;
            currentDate                 = new DateTime(loadItem.currentYear, loadItem.currentMonth, loadItem.currentDay);
            currentSong                 = loadItem.currentSong;
            currentSongRate             = loadItem.currentSongRate;
            IsLose                      = loadItem.IsLose;
            songPromotionChecker        = loadItem.songPromotionChecker;
            todayHasDonePromotionOrTour = loadItem.todayHasDonePromotionOrTour;
            debutSponsorshipFund        = loadItem.debutSponsorshipFund;
            hasReceivedFunds            = loadItem.hasReceivedFunds;
            totalSongsReleased          = loadItem.totalSongsReleased;
            popularity                  = loadItem.popularity;
            maxPopularity               = loadItem.maxPopularity;
            obtainedStatusSymbols       = loadItem.obtainedStatusSymbols;

            // Load Profits Data
            List<int> profitsDataAmount = loadItem.profitsDataAmount;
            List<Vector3Int> profitsDataTime = loadItem.profitsDataTime;
            profitsData.Clear();
            for(int i = 0; i<profitsDataAmount.Count; i++)
            {
                DateTime profitDateTime = new DateTime(profitsDataTime[i].x, profitsDataTime[i].y, profitsDataTime[i].z);
                profitsData.Add(new ProfitPerDay(profitDateTime, profitsDataAmount[i]));
            }

            // Load OnGoingBandSchedules
            List<Vector3Int> bandSchedulesTime      = loadItem.bandSchedulesTime;
            List<int> bandSchedulesHour             = loadItem.bandSchedulesHour;
            List<Promotion> bandSchedulesPromotion  = loadItem.bandSchedulesPromotion;
            List<bool> bandSchedulesIsActive        = loadItem.bandSchedulesIsActive;
            List<string> bandSchedulesTitle         = loadItem.bandSchedulesTitle;
            onGoingBandSchedules.Clear();
            for (int i = 0; i < bandSchedulesTitle.Count; i++)
            {
                DateTime scheduleTime = new DateTime(bandSchedulesTime[i].x, bandSchedulesTime[i].y, bandSchedulesTime[i].z, bandSchedulesHour[i], 0, 0);
                BandSchedule bandSchedule = new BandSchedule(scheduleTime, bandSchedulesTitle[i], bandSchedulesPromotion[i], bandSchedulesIsActive[i]);
                onGoingBandSchedules.Add(bandSchedule);
            }

            return true;
        }
        catch
        {
            return false;
        }
    }

    public void DeletePlayerGameData()
    {
        string path = Application.persistentDataPath + filePath;
        File.Delete(path);
    }
    #endregion

    #region STAMINA
    public int GetStamina()
    {
        return stamina;
    }

    public void AddStamina(int val)
    {
        int resStamina = stamina + val;
        stamina = resStamina > maxStamina ? maxStamina : resStamina;
        ChangeStaminaBar();
    }

    public bool UseStamina(int staminaCost)
    {
        if (stamina - staminaCost < 0)
        {
            return false;
        }
        else
        {
            stamina -= staminaCost;
            ChangeStaminaBar();
            return true;
        }
    }

    void ChangeStaminaBar()
    {
        staminaSlider.value = (float)stamina / (float)maxStamina;
        staminaText.text = stamina + " / " + maxStamina;
    }

    public bool IsStaminaOverCost(int staminaCost)
    {
        return stamina >= staminaCost;
    }
    #endregion

    #region CURRENCY
    public int GetCurrency()
    {
        return currency;
    }

    public void AddCurrency(int addedCurrency)
    {
        int res = currency + addedCurrency;
        currency = res > Int32.MaxValue ? Int32.MaxValue : res;
        ChangeCurrency();
    }

    public bool IsCurrencyOverCost(int currencyCost)
    {
        return currency >= currencyCost;
    }

    public bool UseCurrency(int currencyCost)
    {
        if (currency - currencyCost < 0)
        {
            return false;
        }
        else
        {
            currency -= currencyCost;
            ChangeCurrency();
            return true;
        }
    }

    void ChangeCurrency()
    {
        weeklyCurrentBalance.text = CommonHandler.instance.IntToString(currency);
        monthlyCurrentBalance.text = CommonHandler.instance.IntToString(currency);
    }
    #endregion

    public void UseCurrencyandStamina(int currencyCost, int staminaCost) 
    {
        UseCurrency(currencyCost);
        UseStamina(staminaCost);
    }

    #region DATE
    public DateTime GetCurrentDate()
    {
        return currentDate;
    }

    public void AddCurrentDate(int days)
    {
        DateTime newCurrentDate = currentDate.AddDays(days);
        newCurrentDate = ChangeTime(newCurrentDate, 6, 0, 0, 0);
        ChangeGameDate(newCurrentDate);

        // For demo purposes
        todayHasDonePromotionOrTour = false;

        // Change calendar
        CalendarHandler.instance.ChangeDate();
    }
    
    public DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
    {
        return new DateTime(
            dateTime.Year,
            dateTime.Month,
            dateTime.Day,
            hours,
            minutes,
            seconds,
            milliseconds,
            dateTime.Kind);
    }

    public void ChangeGameDate(DateTime gameDate)
    {
        DateTime lastDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day);
        int comparisonResult = lastDate.Date.CompareTo(gameDate.Date);

        currentDate = gameDate;
        CalendarHandler.instance.ChangeDate();
        if (comparisonResult < 0)
        {
            FinanceHandler.instance.RefreshGraph();
        }
    }

    #endregion

    #region POPULARITY
    public void UpdateTotalSongsReleased(bool fromLoading = true)
    {
        if (!fromLoading) { IsSongWrittenNotReleased = true; }
        else { IsSongWrittenNotReleased = false; }
        StagesHandler.instance.SetupPromotion();
        StagesHandler.instance.SetupWorldTour();
        totalSongsReleasedText.text = totalSongsReleased.ToString();
    }

    public float GetRatioOfPopularity()
    {
        float clampedPopularity = Mathf.Clamp(popularity, 0, 2 * maxPopularity);
        return clampedPopularity / (float)maxPopularity;
    }

    public void AddPopularity(int addedPopularity)
    {
        popularity += addedPopularity;
        ChangePopularity();
    }

    public void ReducePopularity(int reducedPopularity)
    {
        popularity -= reducedPopularity;
        ChangePopularity();
    }

    void ChangePopularity()
    {
        popularityText.text = CommonHandler.instance.IntToString(popularity, true);
    }
    #endregion

    #region STATUS SYMBOLS
    public void ObtainStatusSymbol(StatusSymbol obtainedItem)
    {
        obtainedStatusSymbols.Add(obtainedItem);
    }
    #endregion

    #region BAND SCHEDULES
    public void AddNewPromotionSchedule(List<BandSchedule> newSchedules)
    {
        onGoingBandSchedules.AddRange(newSchedules);
        onGoingBandSchedules.Sort((x, y) => DateTime.Compare(x.timeSchedule, y.timeSchedule));
    }

    public void PromoteNow()
    {
        if(onGoingBandSchedules.Count > 0)
        {
            onGoingBandSchedules.RemoveAt(0);
        }
    }

    public bool CompareDateToOngoingSchedule(DateTime dateTime)
    {
        bool match = false;
        for(int i = 0; i<onGoingBandSchedules.Count; i++)
        {
            int comparisonResult = DateTime.Compare(dateTime, onGoingBandSchedules[i].timeSchedule);
            if (comparisonResult == 0)
            {
                match = true;
                break;
            }
            else if(comparisonResult < 0)
            {
                break;
            }
        }
        return match;
    }
    #endregion

    #region PROFITS DATA
    public void TestAddProfit()
    {
        AddProfit(currentDate, 100);
    }

    public void AddProfit(DateTime profitTime, int profitAmount)
    {
        AddCurrency(profitAmount);
        profitsData.Add(new ProfitPerDay(profitTime, profitAmount));
        profitsData.Sort((x, y) => DateTime.Compare(x.profitDateTime, y.profitDateTime));

        List<ProfitPerDay> newProfitsData = new List<ProfitPerDay>();
        DateTime lastTime = profitsData[0].profitDateTime;
        foreach(ProfitPerDay profitData in profitsData)
        {
            if (lastTime.Date.CompareTo(profitData.profitDateTime.Date) == 0)
            {
                if(newProfitsData.Count <= 0) { newProfitsData.Add(new ProfitPerDay(lastTime, profitData.profitAmount)); continue; }
                newProfitsData[newProfitsData.Count - 1].profitAmount += profitData.profitAmount;
            }
            else
            {
                lastTime = profitData.profitDateTime;
                newProfitsData.Add(new ProfitPerDay(lastTime, profitData.profitAmount));
            }
        }
        profitsData.Clear();
        profitsData.AddRange(newProfitsData);
        FinanceHandler.instance.UpdateGraph();
        SavePlayerGameData();
        //DebugLogProfitsData();
    }

    private void DebugLogProfitsData()
    {
        string logs = "";
        foreach(ProfitPerDay profitData in profitsData)
        {
            logs += "- " + profitData.profitDateTime.ToString("dd-MM-yyyy") + " = " + profitData.profitAmount.ToString() + "\n";
        }
        Debug.Log(logs);
    }

    public int GetThisDayProfit()
    {
        int profitAmount = 0;
        if(profitsData.Count > 0 && currentDate.Date.CompareTo(profitsData[profitsData.Count - 1].profitDateTime.Date) == 0)
        {
            profitAmount = profitsData[profitsData.Count - 1].profitAmount;
        }
        return profitAmount;
    }

    public int GetThisWeekProfit()
    {
        int profitAmount = 0;
        List<int> weekProfits = GetThisWeekProfitsData(CommonHandler.GetFirstDateOfThisWeek(currentDate));
        for(int i = 0; i<weekProfits.Count; i++)
        {
            profitAmount += weekProfits[i];
        }
        return profitAmount;
    }

    public DateTime GetFirstDayOfThisWeek()
    {
        int currentDateInWeek = (int)(currentDate.DayOfWeek + 6) % 7;
        DateTime mondayDate = currentDate.AddDays(-currentDateInWeek);
        return mondayDate;
    }

    public DateTime GetLastDayOfThisWeek()
    {
        int currentDateInWeek = (int)(currentDate.DayOfWeek + 6) % 7;
        DateTime sundayDate = currentDate.AddDays(6-currentDateInWeek);
        return sundayDate;
    }

    public List<int> GetThisWeekProfitsData(DateTime firstDayOfWeek)
    {
        List<int> profitAmounts = new List<int>();
        List<ProfitPerDay> weekProfitsData = new List<ProfitPerDay>();
        for (int  i = 0; i<7; i++)
        {
            weekProfitsData.Add(new ProfitPerDay(firstDayOfWeek.AddDays(i), 0));
        }
      
        if(profitsData.Count > 0)
        {
            int dayIndex = 0;
            int profitDataIndex = 0;
            while (dayIndex < weekProfitsData.Count)
            {
                if (profitDataIndex >= profitsData.Count) { break; }
                int maxComparisonResultDate = profitsData[profitDataIndex].profitDateTime.Date.CompareTo(weekProfitsData[weekProfitsData.Count - 1].profitDateTime.Date);
                if (maxComparisonResultDate > 0) { break; }

                for (int i = profitDataIndex; i < profitsData.Count; i++)
                {
                    int comparisonResultDate = profitsData[i].profitDateTime.Date.CompareTo(weekProfitsData[dayIndex].profitDateTime.Date);
                    if (comparisonResultDate < 0)
                    {
                        profitDataIndex++;
                        continue;
                    }
                    else if (comparisonResultDate == 0)
                    {
                        weekProfitsData[dayIndex].profitAmount = profitsData[i].profitAmount;
                        profitDataIndex = i;
                        break;
                    }
                    else if(comparisonResultDate > 0)
                    {
                        break;
                    }
                }
                dayIndex++;
            }
        }

        for(int i = 0; i<weekProfitsData.Count; i++)
        {
            profitAmounts.Add(weekProfitsData[i].profitAmount);
        }
        return profitAmounts;
    }

    public DateTime GetFirstDayOfThisMonth()
    {
        DateTime firstDayOfMonthDate = new DateTime(currentDate.Year, currentDate.Month, 1);
        return firstDayOfMonthDate;
    }

    public DateTime GetLastDayOfThisMonth()
    {
        DateTime lastDayOfThisMonthDate = new DateTime(currentDate.Year, currentDate.Month, 1);
        lastDayOfThisMonthDate = lastDayOfThisMonthDate.AddMonths(1);
        lastDayOfThisMonthDate = lastDayOfThisMonthDate.AddDays(-1);
        return lastDayOfThisMonthDate;
    }

    public List<int> GetThisMonthProfitsData(DateTime firstDayOfMonth)
    {
        List<int> profitAmounts = new List<int>();
        DateTime firstMonthDate = firstDayOfMonth;
        List<ProfitPerDay> monthProfitsData = new List<ProfitPerDay>();
        int daysInMonth = DateTime.DaysInMonth(firstDayOfMonth.Year, firstDayOfMonth.Month);
        for (int i = 0; i < daysInMonth; i++)
        {
            monthProfitsData.Add(new ProfitPerDay(firstMonthDate.AddDays(i), 0));
        }

        if (profitsData.Count > 0)
        {
            int dayIndex = 0;
            int profitDataIndex = 0;
            while (dayIndex < monthProfitsData.Count)
            {
                if (profitDataIndex >= profitsData.Count) { break; }
                int maxComparisonResultDate = profitsData[profitDataIndex].profitDateTime.Date.CompareTo(monthProfitsData[monthProfitsData.Count - 1].profitDateTime.Date);
                if (maxComparisonResultDate > 0) { break; }

                for (int i = profitDataIndex; i < profitsData.Count; i++)
                {
                    int comparisonResultDate = profitsData[i].profitDateTime.Date.CompareTo(monthProfitsData[dayIndex].profitDateTime.Date);
                    if (comparisonResultDate < 0)
                    {
                        profitDataIndex++;
                        continue;
                    }
                    else if (comparisonResultDate == 0)
                    {
                        monthProfitsData[dayIndex].profitAmount = profitsData[i].profitAmount;
                        profitDataIndex = i;
                        break;
                    }
                    else if (comparisonResultDate > 0)
                    {
                        break;
                    }
                }
                dayIndex++;
            }
        }
       
        int lastWeekProfits = 0;
        for (int i = 0; i < monthProfitsData.Count; i++)
        {
            lastWeekProfits += monthProfitsData[i].profitAmount;
            if (monthProfitsData[i].profitDateTime.DayOfWeek == 0)
            {
                profitAmounts.Add(lastWeekProfits);
                lastWeekProfits = 0;
            }

            if(i == monthProfitsData.Count - 1 && monthProfitsData[i].profitDateTime.DayOfWeek > 0)
            {
                profitAmounts.Add(lastWeekProfits);
                lastWeekProfits = 0;
            }
        }
        return profitAmounts;
    }

    public bool IsPreviousDateHaveProfitData(DateTime firstDate)
    {
        bool haveProfitData = false;
        if(profitsData.Count > 0 && profitsData[0].profitDateTime.Date.CompareTo(firstDate.Date) < 0)
        {
            haveProfitData = true;
        }
        return haveProfitData;
    }

    public bool IsNextDateHaveProfitData(DateTime lastDate)
    {
        bool haveProfitData = false;
        if (profitsData.Count > 0 && profitsData[profitsData.Count-1].profitDateTime.Date.CompareTo(lastDate.Date) > 0)
        {
            haveProfitData = true;
        }
        return haveProfitData;
    }
    #endregion

    #region DEMO PURPOSES
    // Just for this demo purposes
    public void JustDonePromotionOrTourToday()
    {
        todayHasDonePromotionOrTour = true;
    }

    public bool IsTodayOkayToPromoteOrTour()
    {
        return !todayHasDonePromotionOrTour;
    }
    #endregion

    #region CHECK GAME OVER / LOSE
    public void CheckPromotion()
    {
        if(currentSongRate < 6)
        {
            if(songPromotionChecker.Count < totalSongsReleased)
                songPromotionChecker.Add(false);
        }

        if(currentSongRate >= 6)
        {
            if (songPromotionChecker.Count < totalSongsReleased)
                songPromotionChecker.Add(true);
        }
    }

    void GameOver()
    {
        IsLose = true;
        gameStatusIcon.sprite = inactiveSprite;
        gameStatusText.text = "Inactive";
        if (inboxMenu != null)
            GameMenuUI.instance.ChangeMenu(inboxMenu);

        InboxHandler.instance.ShowNewGameMail();
        GameMenuUI.instance.LockMenu();
        SavePlayerGameData();
    }

    public void NewGame()
    {
        DeletePlayerGameData();
        PlayerSaveData.instance.DeletePlayerSetupData();
        GameManager.instance.LoadingToFirstSetupScene();
    }
    #endregion
}
