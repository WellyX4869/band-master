using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public static MusicPlayer instance;
    [HideInInspector] public AudioSource audioSource;

    public List<AudioClip> maleSongs = new List<AudioClip>();
    public List<AudioClip> femaleSongs = new List<AudioClip>();

    [HideInInspector] public List<AudioClip> songs = new List<AudioClip>();
    public AudioClip bgmSong;
    [SerializeField] [Range(0,1)] float bgmVolume;
    [SerializeField] [Range(0, 1)] float songVolume;
    bool firstTime = true;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            audioSource = GetComponent<AudioSource>();
        }
    }

    private void Start()
    {
        SetupSongs();
    }

    public void SetupSongs()
    {
        songs.Clear();
        // Set player vocalist sex data for randomized things
        if (PlayerSaveData.instance.vocalistSex == Sex.male)
        {
            songs.AddRange(maleSongs);
        }
        else
        {
            songs.AddRange(femaleSongs);
        }

        bgmSong = songs[0];
        songs.RemoveAt(0);

        if (audioSource.isPlaying) { return; }

        audioSource.clip = bgmSong;
        audioSource.volume = bgmVolume;
        audioSource.Play();
    }
    
    public void GetRandomSong()
    {
        if(songs.Count <= 3)
        {
            SetupSongs();
        }

        if (firstTime)
        {
            PlayerGameData.instance.currentSong = bgmSong;
            firstTime = false;
        }
        else
        {
            int randomSongIndex = Random.Range(0, songs.Count);
            PlayerGameData.instance.currentSong = songs[randomSongIndex];
            songs.RemoveAt(randomSongIndex);
        }
    }

    public void PlayShortClipSong(AudioClip song)
    {
        audioSource.clip = song;
        audioSource.volume = songVolume;
        audioSource.Play();
    }

    public void PlayFullClipSong(AudioClip song)
    {
        audioSource.clip = song;
        audioSource.volume = songVolume;
        audioSource.Play();
    }

    public void PlayCurrentSong()
    {
        audioSource.clip = PlayerGameData.instance.currentSong;
        audioSource.volume = songVolume;
        audioSource.Play();
    }

    public void StopSong()
    {
        audioSource.Stop();
        audioSource.clip = bgmSong;
        audioSource.volume = bgmVolume;
        audioSource.Play();
    }
}
