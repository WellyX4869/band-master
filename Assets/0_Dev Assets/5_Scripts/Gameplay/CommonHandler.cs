using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using TMPro;
using System;

public class CommonHandler : MonoBehaviour
{
    public static CommonHandler instance;

    [Header("Animation Settings")]
    public float fadeInDuration;
    public float fadeOutDuration;

    public Color inactiveColor;
    public Color activeColor;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    #region ANIMATION
    public void FadeInAnimation(GameObject panel, GameObject parent = null, float fadeDuration = -1f)
    {
        StartCoroutine(FadeIn(panel, parent, fadeDuration));
    }

    public void FadeOutAnimation(GameObject panel, GameObject parent = null, float fadeDuration = -1f)
    {
        StartCoroutine(FadeOut(panel, parent, fadeDuration));
    }

    IEnumerator FadeIn(GameObject panel, GameObject parent = null, float fadeDuration = -1f)
    {
        float FadeDuration = fadeDuration < 0 ? fadeInDuration : fadeDuration;
        if (parent != null) { parent.SetActive(true); }
        var panelImages = panel.GetComponentsInChildren<Image>();
        var panelTexts = panel.GetComponentsInChildren<TextMeshProUGUI>();
        foreach (Image panelImage in panelImages)
        {
            // if the gameobject of image is not active then don't add them
            Color defaultColor = new Color(panelImage.color.r, panelImage.color.g, panelImage.color.b, panelImage.color.a);
            panelImage.color = new Color(0, 0, 0, 0);
            panelImage.DOColor(defaultColor, FadeDuration);
        }
        foreach (TextMeshProUGUI panelText in panelTexts)
        {
            Color defaultColor = new Color(panelText.color.r, panelText.color.g, panelText.color.b, panelText.color.a);
            panelText.color = new Color(0, 0, 0, 0);
            panelText.DOColor(defaultColor, FadeDuration);
        }
        panel.SetActive(true);
        yield return new WaitForSeconds(FadeDuration);
    }

    IEnumerator FadeOut(GameObject panel, GameObject parent = null, float fadeDuration = -1f)
    {
        float FadeDuration = fadeDuration < 0 ? fadeOutDuration : fadeDuration;
        var panelImages = panel.GetComponentsInChildren<Image>();
        var panelTexts = panel.GetComponentsInChildren<TextMeshProUGUI>();
        List<Color> defaultPanelImagesColors = new List<Color>();
        List<Color> defaultPanelTextsColors = new List<Color>();
        foreach (Image panelImage in panelImages)
        {
            defaultPanelImagesColors.Add(panelImage.color);
            panelImage.DOColor(new Color(0, 0, 0, 0), FadeDuration);
        }
        foreach (TextMeshProUGUI panelText in panelTexts)
        {
            defaultPanelTextsColors.Add(panelText.color);
            panelText.DOColor(new Color(0, 0, 0, 0), FadeDuration);
        }

        yield return new WaitForSeconds(FadeDuration);
        panel.SetActive(false); int counterIndex = 0;
        foreach (Image panelImage in panelImages)
        {
            panelImage.DOColor(defaultPanelImagesColors[counterIndex], 0);
            counterIndex++;
        }
        counterIndex = 0;
        foreach (TextMeshProUGUI panelText in panelTexts)
        {
            panelText.DOColor(defaultPanelTextsColors[counterIndex], 0);
            counterIndex++;
        }

        if(parent != null)
        {
            parent.SetActive(false);
        }
    }
    #endregion

    #region OTHERS
    public string IntToString(int currency, bool withoutCommaDash = false)
    {
        string resCurrency = "";
        string reversedCurrencyStr = "";
        string currencyStr = currency.ToString();
        for(int i = currencyStr.Length - 1; i>= 0; i--)
        {
            reversedCurrencyStr += currencyStr[i];
        }

        for(int i = 0; i <reversedCurrencyStr.Length; i++)
        {
            if (i > 0 && i % 3 == 0)
            {
                resCurrency += ".";
            }
            resCurrency += reversedCurrencyStr[i];
        }

        if (withoutCommaDash)
        {
            resCurrency = Reverse(resCurrency);
        }
        else
        {
            resCurrency = Reverse(resCurrency) + ",-";
        }
        return resCurrency;
    }

    string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }

    public static int GetWeekNumberOfMonth(DateTime date)
    {
        int dayIndex = 0;
        int weekIndex = 0;
        DateTime dayCounter = new DateTime(date.Year, date.Month, 1);
        while (dayCounter.Date.CompareTo(date.Date) <= 0)
        {
            dayIndex++;
            if (dayCounter.DayOfWeek == 0)
            {
                dayIndex = 0;
                weekIndex++;
            }
            dayCounter = dayCounter.AddDays(1);
        }
        if(dayIndex > 0) { weekIndex++; }

        return weekIndex;
    }

    public static DateTime GetFirstDateOfThisWeek(DateTime date)
    {
        int currentDateInWeek = (int)(date.DayOfWeek + 6) % 7;
        DateTime firstDateOfThisWeek = date.AddDays(-currentDateInWeek);
        return firstDateOfThisWeek;
    }

    public static DateTime GetLastDateOfThisWeek(DateTime date)
    {
        int currentDateInWeek = (int)(date.DayOfWeek + 6) % 7;
        DateTime lastDateOfThisWeek = date.AddDays(6-currentDateInWeek);
        return lastDateOfThisWeek;
    }

    public static DateTime GetFirstDateOfThisMonth(DateTime date)
    {
        DateTime firstDayOfMonthDate = new DateTime(date.Year, date.Month, 1);
        return firstDayOfMonthDate;
    }

    public static DateTime GetLastDateOfThisMonth(DateTime date)
    {
        DateTime lastDayOfThisMonthDate = new DateTime(date.Year, date.Month, 1);
        lastDayOfThisMonthDate = lastDayOfThisMonthDate.AddMonths(1);
        lastDayOfThisMonthDate = lastDayOfThisMonthDate.AddDays(-1);
        return lastDayOfThisMonthDate;
    }
    #endregion
}
