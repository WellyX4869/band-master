using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public enum SceneIndexes
{
    MANAGER = 0,
    FIRST_SETUP = 1,
    GAME = 2
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] Camera gameManagerCamera;
    public GameObject loadingScreen;
    public float fastLoadTimeTreshold = 5f;

    [Header("Loading To Game Scene")]
    public GameObject loadingToGame;
    public TextMeshProUGUI loadingToGameTitleText;
    public TextMeshProUGUI loadingToGameDescriptionText;
    public TextMeshProUGUI loadingToGameText;
    public Slider loadingToGameProgressBar;
    public float loadingTextDelayTime;

    [Header("Loading To Menu Scene")]
    public GameObject loadingToFirstSetup;
    public TextMeshProUGUI loadingToFirstSetupTitleText;
    public Slider loadingToFirstSetupProgressBar;

    private void Awake()
    {
        instance = this;
        loadingToFirstSetup.SetActive(false);
        loadingToGame.SetActive(false);
        loadingScreen.gameObject.SetActive(false);
        gameManagerCamera.gameObject.SetActive(false);
        SceneManager.LoadSceneAsync((int)SceneIndexes.FIRST_SETUP, LoadSceneMode.Additive);
    }

    List<AsyncOperation> scenesLoading = new List<AsyncOperation>();
    public void LoadingToGameScene()
    {
        scenesLoading.Clear();
        loadingToGameTitleText.text = "Preparation complete!";
        loadingToGameDescriptionText.text = "Documents for your group will be available on your desk soon.";
        loadingScreen.gameObject.SetActive(true);
        loadingToGame.SetActive(true);
        gameManagerCamera.gameObject.SetActive(true);
        PlayerSaveData.instance.IsPlayerAlreadySetup = true;
        PlayerSaveData.instance.SavePlayerSetupData();
        scenesLoading.Add(SceneManager.UnloadSceneAsync((int)SceneIndexes.FIRST_SETUP));
        scenesLoading.Add(SceneManager.LoadSceneAsync((int)SceneIndexes.GAME, LoadSceneMode.Additive));

        StartCoroutine(GetSceneLoadToGameSceneProgress());
    }

    float totalSceneProgress;
    public IEnumerator GetSceneLoadToGameSceneProgress()
    {
        float elapsedTime = 0f;
        float counterTime = loadingTextDelayTime;
        string pleaseWait = "Please wait";
        string dot = "";

        while (true)
        {
            // Loading Text Animation
            if (elapsedTime >= counterTime)
            {
                if (dot == "...") { dot = ""; }
                dot += ".";
                loadingToGameText.text = pleaseWait + dot;
                counterTime += loadingTextDelayTime;
            }

            elapsedTime += Time.deltaTime;
            totalSceneProgress = 0;

            foreach (AsyncOperation operation in scenesLoading)
            {
                totalSceneProgress += operation.progress;
            }

            totalSceneProgress = (totalSceneProgress / scenesLoading.Count);
            loadingToGameProgressBar.value = totalSceneProgress;

            if (totalSceneProgress >= 1 && elapsedTime > fastLoadTimeTreshold)
            {
                break;
            }
            yield return null;
        }

        loadingToGame.SetActive(false);
        loadingScreen.gameObject.SetActive(false);
        gameManagerCamera.gameObject.SetActive(false);
    }

    public void LoadingToFirstSetupScene()
    {
        scenesLoading.Clear();
        loadingToFirstSetupTitleText.text = "Signing Out";
        loadingScreen.gameObject.SetActive(true);
        loadingToFirstSetup.SetActive(true);
        gameManagerCamera.gameObject.SetActive(true);
        scenesLoading.Add(SceneManager.UnloadSceneAsync((int)SceneIndexes.GAME));
        scenesLoading.Add(SceneManager.LoadSceneAsync((int)SceneIndexes.FIRST_SETUP, LoadSceneMode.Additive));

        StartCoroutine(GetSceneLoadToFirstSetupProgress());
    }

    public IEnumerator GetSceneLoadToFirstSetupProgress()
    {
        float elapsedTime = 0f;
        float counterTime = loadingTextDelayTime;
        string signOut = "Signing Out";
        string dot = "";

        while (true)
        {
            // Loading Text Animation
            if (elapsedTime >= counterTime)
            {
                if (dot == "...") { dot = ""; }
                dot += ".";
                loadingToFirstSetupTitleText.text = signOut + dot;
                counterTime += loadingTextDelayTime;
            }

            elapsedTime += Time.deltaTime;

            totalSceneProgress = 0;

            foreach (AsyncOperation operation in scenesLoading)
            {
                totalSceneProgress += operation.progress;
            }

            totalSceneProgress = (totalSceneProgress / scenesLoading.Count);
            loadingToFirstSetupProgressBar.value = totalSceneProgress;

            if (totalSceneProgress >= 1f && elapsedTime > fastLoadTimeTreshold)
            {
                break;
            }
            yield return null;
        }

        loadingToFirstSetup.SetActive(false);
        loadingScreen.gameObject.SetActive(false);
        gameManagerCamera.gameObject.SetActive(false);
    }
}
